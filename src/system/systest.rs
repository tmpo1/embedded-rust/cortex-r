#![allow(non_snake_case)]

use crate::registers::{reg_efc::*, reg_esm::*, reg_flash::*, reg_stc::*, reg_system::*,
                       reg_vim::*, reg_pbist::*, reg_tcram::*, reg_het::*};
use super::sys_core::*;

use volatile_register::{RW, RO};
use raw_pointer::Pointer;

/**** START CONSTANTS ****/

pub const OUTPUT_ENABLE: u32 = 0x0003C000u32;
pub const OUTPUT_DISABLE: u32 = 0x0u32;

pub const EFC_AUTOLOAD_ERROR_EN: u32 = 0x00040000u32;
pub const EFC_INSTRUCTION_ERROR_EN: u32 = 0x00080000u32;
pub const EFC_INSTRUCTION_INFO_EN: u32 = 0x00100000u32;
pub const EFC_SELF_TEST_ERROR_EN: u32 = 0x00200000u32;

pub const EFC_AUTOLOAD_ERROR_DIS: u32 = 0x0u32;
pub const EFC_INSTRUCTION_ERROR_DIS: u32 = 0x0u32;
pub const EFC_INSTRUCTION_INFO_DIS: u32 = 0x0u32;
pub const EFC_SELF_TEST_ERROR_DIS: u32 = 0x0u32;

pub const EFC_SELF_TEST_DONE: u32 = 0x00008000u32;
pub const EFC_SELF_TEST_ERROR: u32 = 0x00004000u32;

/***** END CONSTANTS *****/

/**** START FAIL CODES ****/

pub enum FailCode {
    EFC(u32),
    Flash(u32),
    STC(u32),
    CCM(u32),
    PBIST(u32),
    RAMECC,
    FlashECC,
}

/***** END FAIL CODES *****/

pub fn selfTestFailNotify(_err: FailCode) {
    // add some sort of way to notify of failure based on the failcode
}

pub fn memInit(ram: u32) {
    unsafe { SYSFRAME1.MINITGCR.write(0xAu32); }

    unsafe { SYSFRAME1.MSINENA.write(ram); }

    loop {
        if (SYSFRAME1.MSTCGSTAT.read() & 0x100u32) == 0x100u32 { break; }
    }

    unsafe { SYSFRAME1.MINITGCR.write(0x5u32); }
}

pub fn efcStuckZeroTest() -> bool {
    let ESM_ESTATUS4: u32;
    let ESM_ESTATUS1: u32;

    let mut result: bool = false;
    let error_checks = EFC_INSTRUCTION_INFO_EN
                     | EFC_INSTRUCTION_ERROR_EN
                     | EFC_AUTOLOAD_ERROR_EN
                     | EFC_SELF_TEST_ERROR_EN;

    unsafe { efcREG.BOUNDARY.write(OUTPUT_ENABLE | error_checks); }
    
    if (efcREG.PINS.read() & 0x5C00u32) == 0x5C00u32 {
        ESM_ESTATUS4 = esmREG.ESMSR4.read();
        ESM_ESTATUS1 = esmREG.ESMSR3.read();
        
        if ((ESM_ESTATUS4 & 0x200u32) == 0x200u32) && ((ESM_ESTATUS1 & 0x2u32) == 0x2u32) {
            result = true;
        }
    }

    unsafe {
        efcREG.BOUNDARY.write(OUTPUT_ENABLE);
        
        esmREG.ESMSR4.write(0x200u32);
        esmREG.ESMSR3.write(0x2u32);
        esmREG.ESMEKR.write(0x5u32);
    }

    result
}

pub unsafe fn efcSelfTest() {
    efcREG.SELF_TEST_CYCLES.write(0x258u32);

    efcREG.SELF_TEST_SIGN.write(0x5362F97Fu32);

    efcREG.BOUNDARY.write(0x200Fu32);
}

pub fn efcCheck() -> u32 {
    let (status, efc_status): (u32, u32);
    
    efc_status = efcREG.ERROR.read();

    if efc_status == 0u32 {
        if efcStuckZeroTest() {
            unsafe { efcSelfTest(); }
            status = 0u32;
        } else {
            selfTestFailNotify(FailCode::EFC(2));
            status = 1u32;
        }
    } else {
        if efc_status == 0x15u32 {
            unsafe { efcSelfTest(); }
            status = 2u32;
        } else {
            selfTestFailNotify(FailCode::EFC(2));
            status = 3u32;
        }
    }

    status
}

pub fn efcSelfTestCheck() -> bool {
    let mut result: bool = false;

    let (EFC_PINS, EFC_ERROR): (u32, u32);
    let (ch40_stat, ch41_stat): (u32, u32);

    loop {
        if (efcREG.PINS.read() & EFC_SELF_TEST_DONE) == 0u32 { break; }
    }

    EFC_PINS = efcREG.PINS.read();
    EFC_ERROR = efcREG.ERROR.read();

    if ((EFC_PINS & EFC_SELF_TEST_ERROR) == 0u32) && ((EFC_ERROR & 0x1Fu32) == 0u32) {
        ch40_stat = esmREG.ESMSR4.read() & 0x100u32;
        ch41_stat = esmREG.ESMSR4.read() & 0x200u32;
        if (ch40_stat == 0u32) && (ch41_stat == 0u32) {
            result = true;
        }
    }

    result
}

pub fn fmcECCcheck() {
    let mut _otpread: u32 = (*singleBitErr).read();
    let _temp: u32;

    if (esmREG.ESMSR1.read() & 0x40u32) == 0x40u32 {
        unsafe { flashREG.FEDACSTATUS.write(0x00010006u32); }

        unsafe { esmREG.ESMSR1.write(0x40u32); }

        _otpread = (*doubleBitErr).read();
        if (esmREG.ESMSR3.read() & 0x80u32) == 0x80u32 {
            _temp = flashREG.FUNC_ERR_ADD.read();
            unsafe { flashREG.FEDACSTATUS.write(0x00020100u32); }

            unsafe {
                esmREG.ESMSR3.write(0x80u32);
                esmREG.ESMEKR.write(0x5u32);
            }
        } else {
            selfTestFailNotify(FailCode::Flash(2));
        }
    } else {
        selfTestFailNotify(FailCode::Flash(1));
    }
}

pub fn flashErrSelfTest() {
    unsafe { flashREG.FEDACTRL1.write(0x000A060Au32); }

    if (esmREG.ESMSR1.read() & 0x40u32) == 0x40u32 {
        unsafe { esmREG.ESMSR1.write(0x40u32); }
        fmcECCcheck();
    } else {
        fmcECCcheck();
    }
}

pub unsafe fn stcSelfCheck() {
    SYSFRAME1.SYSESR.write(0u32);   // clear SYSESR

    SYSFRAME2.STCCLKDIV.write(0x01000000u32);

    stcREG.STCGCR0.write(0x00010001u32);

    stcREG.STCSCSCR.write(0x1Au32);

    stcREG.STCTPR.write(-1i32 as u32);

    for _ in 0..16 {
        // wait 16 clock cycles for some ungodly reason
    }

    stcREG.STCGCR1.write(0xAu32);

    _gotoCPUIdle_();
}

pub fn stcSelfCheckTest() -> bool {
    let result: bool = {
        if (stcREG.STCGSTAT.read() & 1u32) == 1u32 {
            if (stcREG.STCGSTAT.read() & 3u32) != 1u32 {
                if (stcREG.STCFSTAT.read() & 1u32) == 1u32 {
                    // cpu 1 fail
                    selfTestFailNotify(FailCode::STC(0));
                }
                if (stcREG.STCFSTAT.read() & 2u32) == 2u32 {
                    // cpu 2 fail
                    selfTestFailNotify(FailCode::STC(1));
                }
                if (stcREG.STCFSTAT.read() & 4u32) == 4u32 {
                    // timeout
                    selfTestFailNotify(FailCode::STC(2));
                }
            }
            true
        } else {
            unsafe { stcSelfCheck(); }

            false
        }
    };

    result
}

pub const CCMSR: Pointer<RO<u32>> = Pointer::<RO<u32>> { ptr: 0xFFFFF600u32 as *mut RO<u32> };
pub const CCMKEYR: Pointer<RW<u32>> = Pointer::<RW<u32>> { ptr: 0xFFFFF604u32 as *mut RW<u32> };

pub fn ccmSelfCheck() {
    unsafe { (*CCMKEYR).write(6u32); }

    loop { if ((*CCMSR).read() & 0x100u32) == 0x100u32 { break; } }

    if ((*CCMSR).read() & 1u32) == 1u32 {
        selfTestFailNotify(FailCode::CCM(1));   // STE is set
    } else {
        if (esmREG.ESMSR1.read() & 0x80000000u32) == 0x80000000u32 {
            selfTestFailNotify(FailCode::CCM(2));   // ESM flag is not set on self-test
        } else {
            unsafe { (*CCMKEYR).write(9u32); }

            loop { if (*CCMKEYR).read() == 0u32 { break; } }

            if (esmREG.ESMSR2.read() & 4u32) != 4u32 {
                selfTestFailNotify(FailCode::CCM(3));   // ESM flag is not set on compare error test
            } else {
                if (vimREG.FIQINDEX.read() & 0xFFu32) != 1u32 {
                    selfTestFailNotify(FailCode::CCM(4));   // ESM high interrupt flag is not set in VIM
                }

                unsafe {
                    esmREG.ESMSR2.write(4u32);
                    esmREG.ESMSSR2.write(4u32);
                    esmREG.ESMSR1.write(0x80000000u32);
                    esmREG.ESMEKR.write(5u32);
                    
                    (*CCMKEYR).write(0xFu32);
                }

                loop { if (*CCMKEYR).read() == 0u32 { break; } }

                if (esmREG.ESMSR1.read() & 0x80000000u32) != 0x80000000u32 {
                    selfTestFailNotify(FailCode::CCM(2));   // ESM flag not set
                } else {
                    unsafe { esmREG.ESMSR1.write(0x80000000u32); }
                }
            }
        }
    }
}

pub unsafe fn pbistSelfCheck() {
    SYSFRAME1.MSTGCR.write(SYSFRAME1.MSTGCR.read() | 0x100u32);
    SYSFRAME1.MSINENA.write(1u32);
    SYSFRAME1.MSTGCR.write(SYSFRAME1.MSTGCR.read() & !(0xFu32));
    SYSFRAME1.MSTGCR.write(SYSFRAME1.MSTGCR.read() | 0xAu32);

    for _ in 0..32 {
        // wait 32 VBUS clock cycles
    }

    pbistREG.PACT.write(3u32);
    pbistREG.ALGO.write(pbistAlgo::March13N_DP.value());
    pbistREG.RINFOL.write(1u32);
    pbistREG.OVER.write(0u32);
    pbistREG.ROM.write(3u32);
    pbistREG.DLR.write(0x14u32);

    loop { if SYSFRAME1.MSTCGSTAT.read() & 1u32 == 1u32 { break; } }

    // check for failure
    if (pbistREG.FSRF0.read() & 1u32) != 1u32 {
        selfTestFailNotify(FailCode::PBIST(0)); // could not detect failure
    }

    pbistStopTest();
}

pub unsafe fn pbistRunTest(algo: u32, rinfo: u32) {
    SYSFRAME1.MSTGCR.write(5u32);
    SYSFRAME1.MINITGCR.write(5u32);
    SYSFRAME1.MSINENA.write(1u32);
    SYSFRAME1.MSTGCR.write(0xAu32);
    
    for _ in 0..32 {
        // wait 32 VBUS clock cycles
    }

    pbistREG.PACT.write(3u32);
    pbistREG.ALGO.write(algo);
    pbistREG.RINFOL.write(rinfo);
    pbistREG.RINFOU.write(0u32);
    pbistREG.OVER.write(0u32);
    pbistREG.ROM.write(3u32);
    pbistREG.DLR.write(0x14u32);
}

pub fn pbistTestCompleted() -> bool {
    SYSFRAME1.MSTCGSTAT.read() & 1u32 != 0u32
}

pub unsafe fn pbistStopTest() {
    pbistREG.PACT.write(0u32);
    SYSFRAME1.MSTGCR.write(SYSFRAME1.MSTGCR.read() & 0xFFFFFFF0u32);
    SYSFRAME1.MSTGCR.write(SYSFRAME1.MSTGCR.read() | 5u32);
}

pub fn pbistTestPassed() -> bool {
    pbistREG.FSRF0.read() == 0u32
}

pub fn checkRAMECC() {
    let mut tcramA_1_bit_error = Pointer::<u32>::from(0x08400000);
    let mut tcramA_2_bit_error = Pointer::<u32>::from(0x08400010);
    
    let mut tcramB_1_bit_error = Pointer::<u32>::from(0x08400008);
    let mut tcramB_2_bit_error = Pointer::<u32>::from(0x08400018);

    let mut tcramA_1_bit = Pointer::<u64>::from(0x08000000);
    let mut tcramA_2_bit = Pointer::<u64>::from(0x08000010);

    let mut tcramB_1_bit = Pointer::<u64>::from(0x08000008);
    let mut tcramB_2_bit = Pointer::<u64>::from(0x08000018);

    let tcramA_1_bk: u64 = *tcramA_1_bit;
    let tcramB_1_bk: u64 = *tcramB_1_bit;
    let tcramA_2_bk: u64 = *tcramA_2_bit;
    let tcramB_2_bk: u64 = *tcramB_2_bit;

    unsafe {
        tcram1REG.RAMOCCUR.write(0u32);
        tcram2REG.RAMOCCUR.write(0u32);

        tcram1REG.RAMTHRESHOLD.write(1u32);
        tcram2REG.RAMTHRESHOLD.write(1u32);

        tcram1REG.RAMCTRL.write(0x5010Au32);
        tcram2REG.RAMCTRL.write(0x5010Au32);
    }

    unsafe { _coreDisableRamEcc_(); }

    *tcramA_1_bit_error = *tcramA_1_bit_error ^ 1u32;
    *tcramB_1_bit_error = *tcramB_1_bit_error ^ 1u32;

    unsafe { _coreEnableRamEcc_(); }

    let mut _ramread: u64 = *tcramA_1_bit;
    _ramread = *tcramB_1_bit;

    let tcram_1_err_stat: u32 = tcram1REG.RAMERRSTATUS.read() & 1u32;
    let tcram_2_err_stat: u32 = tcram2REG.RAMERRSTATUS.read() & 1u32;

    if (tcram_1_err_stat == 0u32) || (tcram_2_err_stat == 0u32) {
        selfTestFailNotify(FailCode::RAMECC);
    } else {
        if (esmREG.ESMSR1.read() & 0x14000000u32) != 0x14000000u32 {
            unsafe {
                tcram1REG.RAMERRSTATUS.write(1u32);
                tcram2REG.RAMERRSTATUS.write(1u32);

                esmREG.ESMSR1.write(0x14000000u32);
            }
        }
    }

    unsafe { _coreDisableRamEcc_(); }

    *tcramA_2_bit_error = *tcramA_2_bit_error ^ 3u32;
    *tcramB_2_bit_error = *tcramB_2_bit_error ^ 3u32;

    unsafe { _coreEnableRamEcc_(); }

    _ramread = *tcramA_2_bit;
    _ramread = *tcramB_2_bit;

    let mut _regread = tcram1REG.RAMUERRADDR.read();
    _regread = tcram2REG.RAMUERRADDR.read();

    unsafe {
        tcram1REG.RAMCTRL.write(0x5000Au32);
        tcram2REG.RAMCTRL.write(0x5000Au32);
    }

    *tcramA_1_bit = tcramA_1_bk;
    *tcramA_2_bit = tcramA_2_bk;
    *tcramB_1_bit = tcramB_1_bk;
    *tcramB_2_bit = tcramB_2_bk;
}

pub fn checkFlashECC() {
    let mut _flashread: u32;
    
    unsafe {
        flashREG.FEDACTRL1.write(0xA060Au32);
        flashREG.FDIAGCTRL.write(0x50007u32);
        flashREG.FPAR_OVR.write(0x5A01u32);
        flashREG.FDIAGCTRL.write(flashREG.FDIAGCTRL.read() | 0x01000000u32);
    }

    let flash_bad_ECC_1 = Pointer::<u32>::from(0x20000000);
    let flash_bad_ECC_2 = Pointer::<u32>::from(0x20000010);

    _flashread = *flash_bad_ECC_1;

    unsafe { flashREG.FDIAGCTRL.write(0xA0007u32); }

    if (flashREG.FEDACSTATUS.read() & 2u32) == 0u32 {
        selfTestFailNotify(FailCode::FlashECC);
    } else {
        unsafe {
            flashREG.FEDACSTATUS.write(2u32);
            
            esmREG.ESMSR1.write(0x40u32);

            flashREG.FDIAGCTRL.write(0x50007u32);
            flashREG.FPAR_OVR.write(0x5A03u32);
            flashREG.FDIAGCTRL.write(flashREG.FDIAGCTRL.read() | 0x01000000u32);
        
            _flashread = *flash_bad_ECC_2;

            flashREG.FDIAGCTRL.write(0xA0007u32);
        }
    }
}

pub unsafe fn enableParity() {
    (*VIM_PARCTL).write(0xAu32);
    // canREG1.CTL.write(0xAu32 << 10);
    // canREG2.CTL.write(0xAu32 << 10);
    // adcREG1.PARCR.write(0xAu32);
    hetREG1.PCR.write(0xAu32);
    // htuREG1.PCR.write(0xAu32);
}

pub unsafe fn disableParity() {
    (*VIM_PARCTL).write(5u32);
    // canREG1.CTL.write(5u32 << 10);
    // canREG2.CTL.write(5u32 << 10);
    // adcREG1.PARCR.write(5u32);
    hetREG1.PCR.write(5u32);
    // htuREG1.PCR.write(5u32);
}
