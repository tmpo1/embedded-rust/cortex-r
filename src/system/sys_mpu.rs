#![allow(non_snake_case, non_camel_case_types, non_upper_case_globals)]

use core::asm;
/* -------------------------------------------------------------------------------
; sys_mpu.asm
;
; Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com
;
;
;  Redistribution and use in source and binary forms, with or without
;  modification, are permitted provided that the following conditions
;  are met:
;
;    Redistributions of source code must retain the above copyright
;    notice, this list of conditions and the following disclaimer.
;
;    Redistributions in binary form must reproduce the above copyright
;    notice, this list of conditions and the following disclaimer in the
;    documentation and/or other materials provided with the
;    distribution.
;
;    Neither the name of Texas Instruments Incorporated nor the names of
;    its contributors may be used to endorse or promote products derived
;    from this software without specific prior written permission.
;
;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
;
;
;

  
*/

/* Author's Note: sys_mpu.h */
/* @def mpuREGION1
*   @brief Mpu region 1
*
*   Alias for Mpu region 1
*/
pub const mpuREGION1: u32 = 0u32;

/* @def mpuREGION2
*   @brief Mpu region 2
*
*   Alias for Mpu region 1
*/
pub const mpuREGION2: u32 = 1u32;

/* @def mpuREGION3
*   @brief Mpu region 3
*
*   Alias for Mpu region 3
*/
pub const mpuREGION3: u32 = 2u32;

/* @def mpuREGION4
*   @brief Mpu region 4
*
*   Alias for Mpu region 4
*/
pub const mpuREGION4: u32 = 3u32;

/* @def mpuREGION5
*   @brief Mpu region 5
*
*   Alias for Mpu region 5
*/
pub const mpuREGION5: u32 = 4u32;

/* @def mpuREGION6
*   @brief Mpu region 6
*
*   Alias for Mpu region 6
*/
pub const mpuREGION6: u32 = 5u32;

/* @def mpuREGION7
*   @brief Mpu region 7
*
*   Alias for Mpu region 7
*/
pub const mpuREGION7: u32 = 6u32;

/* @def mpuREGION8
*   @brief Mpu region 8
*
*   Alias for Mpu region 8
*/
pub const mpuREGION8: u32 = 7u32;

/* @def mpuREGION_ENABLE
*   @brief Enable MPU Region
*
*   Alias for MPU region enable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuREGION_ENABLE: u32 = 1u32;

/* @def mpuREGION_DISABLE
*   @brief Disable MPU Region
*
*   Alias for MPU region disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuREGION_DISABLE: u32 = 0u32;

/* @def mpuSUBREGION0_DISABLE
*   @brief Disable MPU Sub Region0
*
*   Alias for MPU subregion0 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION0_DISABLE: u32 = 0x100u32;

/* @def mpuSUBREGION1_DISABLE
*   @brief Disable MPU Sub Region1
*
*   Alias for MPU subregion1 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION1_DISABLE: u32 = 0x200u32;

/* @def mpuSUBREGION2_DISABLE
*   @brief Disable MPU Sub Region2
*
*   Alias for MPU subregion2 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION2_DISABLE: u32 = 0x400u32;

/* @def mpuSUBREGION3_DISABLE
*   @brief Disable MPU Sub Region3
*
*   Alias for MPU subregion3 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION3_DISABLE: u32 = 0x800u32;

/* @def mpuSUBREGION4_DISABLE
*   @brief Disable MPU Sub Region4
*
*   Alias for MPU subregion4 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION4_DISABLE: u32 = 0x1000u32;

/* @def mpuSUBREGION5_DISABLE
*   @brief Disable MPU Sub Region5
*
*   Alias for MPU subregion5 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION5_DISABLE: u32 = 0x2000u32;

/* @def mpuSUBREGION6_DISABLE
*   @brief Disable MPU Sub Region6
*
*   Alias for MPU subregion6 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION6_DISABLE: u32 = 0x4000u32;

/* @def mpuSUBREGION7_DISABLE
*   @brief Disable MPU Sub Region7
*
*   Alias for MPU subregion7 disable.
*
*   @note This should be used as the parameter of the API _mpuSetRegionSizeRegister_
*/
pub const mpuSUBREGION7_DISABLE: u32 = 0x8000u32;

/* @enum mpuRegionAccessPermission
*   @brief Alias names for mpu region access permissions
*
*   This enumeration is used to provide alias names for the mpu region access permission:
*     - MPU_PRIV_NA_USER_NA_EXEC no access in privileged mode, no access in user mode and execute
*     - MPU_PRIV_RW_USER_NA_EXEC read/write in privileged mode, no access in user mode and execute
*     - MPU_PRIV_RW_USER_RO_EXEC read/write in privileged mode, read only in user mode and execute
*     - MPU_PRIV_RW_USER_RW_EXEC read/write in privileged mode, read/write in user mode and execute
*     - MPU_PRIV_RO_USER_NA_EXEC read only in privileged mode, no access in user mode and execute
*     - MPU_PRIV_RO_USER_RO_EXEC read only in privileged mode, read only in user mode and execute
*     - MPU_PRIV_NA_USER_NA_NOEXEC no access in privileged mode, no access in user mode and no execution
*     - MPU_PRIV_RW_USER_NA_NOEXEC read/write in privileged mode, no access in user mode and no execution
*     - MPU_PRIV_RW_USER_RO_NOEXEC read/write in privileged mode, read only in user mode and no execution
*     - MPU_PRIV_RW_USER_RW_NOEXEC read/write in privileged mode, read/write in user mode and no execution
*     - MPU_PRIV_RO_USER_NA_NOEXEC read only in privileged mode, no access in user mode and no execution
*     - MPU_PRIV_RO_USER_RO_NOEXEC read only in privileged mode, read only in user mode and no execution
*
*/
pub enum mpuRegionAccessPermission
{
    MPU_PRIV_NA_USER_NA_EXEC   = 0x0000, /*< Alias no access in privileged mode, no access in user mode and execute */
    MPU_PRIV_RW_USER_NA_EXEC   = 0x0100, /*< Alias no read/write in privileged mode, no access in user mode and execute */
    MPU_PRIV_RW_USER_RO_EXEC   = 0x0200, /*< Alias no read/write in privileged mode, read only in user mode and execute */
    MPU_PRIV_RW_USER_RW_EXEC   = 0x0300, /*< Alias no read/write in privileged mode, read/write in user mode and execute */
    MPU_PRIV_RO_USER_NA_EXEC   = 0x0500, /*< Alias no read only in privileged mode, no access in user mode and execute */
    MPU_PRIV_RO_USER_RO_EXEC   = 0x0600, /*< Alias no read only in privileged mode, read only in user mode and execute */
    MPU_PRIV_NA_USER_NA_NOEXEC = 0x1000, /*< Alias no access in privileged mode, no access in user mode and no execution */
    MPU_PRIV_RW_USER_NA_NOEXEC = 0x1100, /*< Alias no read/write in privileged mode, no access in user mode and no execution */
    MPU_PRIV_RW_USER_RO_NOEXEC = 0x1200, /*< Alias no read/write in privileged mode, read only in user mode and no execution */
    MPU_PRIV_RW_USER_RW_NOEXEC = 0x1300, /*< Alias no read/write in privileged mode, read/write in user mode and no execution */
    MPU_PRIV_RO_USER_NA_NOEXEC = 0x1500, /*< Alias no read only in privileged mode, no access in user mode and no execution */
    MPU_PRIV_RO_USER_RO_NOEXEC = 0x1600  /*< Alias no read only in privileged mode, read only in user mode and no execution */
}

/* @enum mpuRegionType
*   @brief Alias names for mpu region type
*
*   This enumeration is used to provide alias names for the mpu region type:
*     - MPU_STRONGLYORDERED_SHAREABLE Memory type strongly ordered and sharable
*     - MPU_DEVICE_SHAREABLE          Memory type device and sharable
*     - MPU_NORMAL_OIWTNOWA_NONSHARED Memory type normal outer and inner write-through, no write allocate and non shared
*     - MPU_NORMAL_OIWTNOWA_SHARED    Memory type normal outer and inner write-through, no write allocate and shared
*     - MPU_NORMAL_OIWBNOWA_NONSHARED Memory type normal outer and inner write-back, no write allocate and non shared
*     - MPU_NORMAL_OIWBNOWA_SHARED    Memory type normal outer and inner write-back, no write allocate and shared
*     - MPU_NORMAL_OINC_NONSHARED     Memory type normal outer and inner non-cachable and non shared
*     - MPU_NORMAL_OINC_SHARED        Memory type normal outer and inner non-cachable and shared
*     - MPU_NORMAL_OIWBWA_NONSHARED   Memory type normal outer and inner write-back, write allocate and non shared
*     - MPU_NORMAL_OIWBWA_SHARED      Memory type normal outer and inner write-back, write allocate and shared
*     - MPU_DEVICE_NONSHAREABLE       Memory type device and non sharable
*/
pub enum mpuRegionType
{
    MPU_STRONGLYORDERED_SHAREABLE = 0x0000, /*< Memory type strongly ordered and sharable */
    MPU_DEVICE_SHAREABLE          = 0x0001, /*< Memory type device and sharable */
    MPU_NORMAL_OIWTNOWA_NONSHARED = 0x0002, /*< Memory type normal outer and inner write-through, no write allocate and non shared */
    MPU_NORMAL_OIWBNOWA_NONSHARED = 0x0003, /*< Memory type normal outer and inner write-back, no write allocate and non shared */
    MPU_NORMAL_OIWTNOWA_SHARED    = 0x0006, /*< Memory type normal outer and inner write-through, no write allocate and shared */
    MPU_NORMAL_OIWBNOWA_SHARED    = 0x0007, /*< Memory type normal outer and inner write-back, no write allocate and shared */
    MPU_NORMAL_OINC_NONSHARED     = 0x0008, /*< Memory type normal outer and inner non-cachable and non shared */
    MPU_NORMAL_OIWBWA_NONSHARED   = 0x000B, /*< Memory type normal outer and inner write-back, write allocate and non shared */
    MPU_NORMAL_OINC_SHARED        = 0x000C, /*< Memory type normal outer and inner non-cachable and shared */
    MPU_NORMAL_OIWBWA_SHARED      = 0x000F, /*< Memory type normal outer and inner write-back, write allocate and shared */
    MPU_DEVICE_NONSHAREABLE       = 0x0010  /*< Memory type device and non sharable */
}

/* @enum mpuRegionSize
*   @brief Alias names for mpu region type
*
*   This enumeration is used to provide alias names for the mpu region type:
*     - MPU_STRONGLYORDERED_SHAREABLE Memory type strongly ordered and sharable
*     - MPU_32_BYTES Memory size in bytes
*     - MPU_64_BYTES Memory size in bytes
*     - MPU_128_BYTES Memory size in bytes
*     - MPU_256_BYTES Memory size in bytes
*     - MPU_512_BYTES Memory size in bytes
*     - MPU_1_KB Memory size in kB
*     - MPU_2_KB Memory size in kB
*     - MPU_4_KB Memory size in kB
*     - MPU_8_KB Memory size in kB
*     - MPU_16_KB Memory size in kB
*     - MPU_32_KB Memory size in kB
*     - MPU_64_KB Memory size in kB
*     - MPU_128_KB Memory size in kB
*     - MPU_256_KB  Memory size in kB
*     - MPU_512_KB Memory size in kB
*     - MPU_1_MB Memory size in MB
*     - MPU_2_MB Memory size in MB
*     - MPU_4_MB Memory size in MB
*     - MPU_8_MBv Memory size in MB
*     - MPU_16_MB Memory size in MB
*     - MPU_32_MB Memory size in MB
*     - MPU_64_MB Memory size in MB
*     - MPU_128_MB Memory size in MB
*     - MPU_256_MB Memory size in MB
*     - MPU_512_MB Memory size in MB
*     - MPU_1_GB Memory size in GB
*     - MPU_2_GB Memory size in GB
*     - MPU_4_GB Memory size in GB
*/
pub enum mpuRegionSize
{
    MPU_32_BYTES  = 0x04 << 1, /*< Memory size in bytes */
    MPU_64_BYTES  = 0x05 << 1, /*< Memory size in bytes */
    MPU_128_BYTES = 0x06 << 1, /*< Memory size in bytes */
    MPU_256_BYTES = 0x07 << 1, /*< Memory size in bytes */
    MPU_512_BYTES = 0x08 << 1, /*< Memory size in bytes */
    MPU_1_KB      = 0x09 << 1, /*< Memory size in kB */
    MPU_2_KB      = 0x0A << 1, /*< Memory size in kB */
    MPU_4_KB      = 0x0B << 1, /*< Memory size in kB */
    MPU_8_KB      = 0x0C << 1, /*< Memory size in kB */
    MPU_16_KB     = 0x0D << 1, /*< Memory size in kB */
    MPU_32_KB     = 0x0E << 1, /*< Memory size in kB */
    MPU_64_KB     = 0x0F << 1, /*< Memory size in kB */
    MPU_128_KB    = 0x10 << 1, /*< Memory size in kB */
    MPU_256_KB    = 0x11 << 1, /*< Memory size in kB */
    MPU_512_KB    = 0x12 << 1, /*< Memory size in kB */
    MPU_1_MB      = 0x13 << 1, /*< Memory size in MB */
    MPU_2_MB      = 0x14 << 1, /*< Memory size in MB */
    MPU_4_MB      = 0x15 << 1, /*< Memory size in MB */
    MPU_8_MB      = 0x16 << 1, /*< Memory size in MB */
    MPU_16_MB     = 0x17 << 1, /*< Memory size in MB */
    MPU_32_MB     = 0x18 << 1, /*< Memory size in MB */
    MPU_64_MB     = 0x19 << 1, /*< Memory size in MB */
    MPU_128_MB    = 0x1A << 1, /*< Memory size in MB */
    MPU_256_MB    = 0x1B << 1, /*< Memory size in MB */
    MPU_512_MB    = 0x1C << 1, /*< Memory size in MB */
    MPU_1_GB      = 0x1D << 1, /*< Memory size in GB */
    MPU_2_GB      = 0x1E << 1, /*< Memory size in GB */
    MPU_4_GB      = 0x1F << 1  /*< Memory size in GB */
}



/* Author's Note: sys_mpu.asm */
/* Author's Note: Return values are assumed to be r0, inputs are placed left to right in r0,r1...rN */

//  .text
//  .arm

//-------------------------------------------------------------------------------
// Initalize Mpu
// SourceId : MPU_SourceId_001
// DesignId : MPU_DesignId_001
// Requirements : HL_SR487
    //.def     _mpuInit_
pub unsafe fn _mpuInit_() { 
	asm!("
        @ Disable mpu
        mrc   p15, #0, r0, c1, c0, #0
        bic   r0,  r0, #1
        dsb
        mcr   p15, #0, r0, c1, c0, #0
        isb
        @ Disable background region
        mrc   p15, #0, r0,      c1, c0, #0
        bic   r0,  r0, #0x20000
        mcr   p15, #0, r0,      c1, c0, #0
        @ Setup region 1
        mov   r0,  #0
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r1Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0008
        orr   r0,  r0,    #0x1000
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((1 << 15) + (1 << 14) + (1 << 13) + (1 << 12) + (1 << 11) + (1 << 10) + (1 <<  9) + (1 <<  8) + (0x1F << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region 2
        mov   r0,  #1
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r2Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0008
        orr   r0,  r0,    #0x0600
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((0 << 15) + (0 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x15 << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region
        mov   r0,  #2
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r3Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0008
        orr   r0,  r0,    #0x0300
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((0 << 15) + (0 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x11 << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region 4
        mov   r0,  #3
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r4Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0008
        orr   r0,  r0,    #0x0300
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((0 << 15) + (0 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x11 << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region 5
        mov   r0,  #4
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r5Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0000
        orr   r0,  r0,    #0x0300
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((1 << 15) + (1 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x19 << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region 6
        mov   r0,  #5
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r6Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0000
        orr   r0,  r0,    #0x0300
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((0 << 15) + (0 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x1A << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region 7
        mov   r0,  #6
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r7Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0008
        orr   r0,  r0,    #0x1200
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((0 << 15) + (0 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x16 << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2
        @ Setup region 8
        mov   r0,  #7
        mcr   p15, #0,    r0, c6, c2, #0
        ldr   r0,  r8Base
        mcr   p15, #0,    r0, c6, c1, #0
        mov   r0,  #0x0010
        orr   r0,  r0,    #0x1300
        mcr   p15, #0,    r0, c6, c1, #4
        movw  r0,  #((0 << 15) + (0 << 14) + (0 << 13) + (0 << 12) + (0 << 11) + (0 << 10) + (0 <<  9) + (0 <<  8) + (0x17 << 1) + (1))
        mcr   p15, #0,    r0, c6, c1, #2

        @ Enable mpu background region
        mrc   p15, #0, r0,      c1, c0, #0
        orr   r0,  r0, #0x20000
        mcr   p15, #0, r0,      c1, c0, #0
        @ Enable mpu
        mrc   p15, #0, r0, c1, c0, #0
        orr   r0,  r0, #1
        dsb
        mcr   p15, #0, r0, c1, c0, #0
        isb
        bx    lr

        r1Base:  .word 0x00000000
        r2Base:  .word 0x00000000
        r3Base:  .word 0x08000000
        r4Base:  .word 0x08400000
        r5Base:  .word 0x60000000
        r6Base:  .word 0x80000000
        r7Base:  .word 0xF0000000
        r8Base:  .word 0xFC000000


        "
    );
}


//-------------------------------------------------------------------------------
// Enable Mpu
// SourceId : MPU_SourceId_002
// DesignId : MPU_DesignId_002
// Requirements : HL_SR488

    //.def     _mpuEnable_
pub unsafe fn _mpuEnable_() { 
	asm!("

        mrc   p15, #0, r0, c1, c0, #0
        orr   r0,  r0, #1
        dsb
        mcr   p15, #0, r0, c1, c0, #0
        isb
        bx    lr

        "
    );
}


//-------------------------------------------------------------------------------
// Disable Mpu
// SourceId : MPU_SourceId_003
// DesignId : MPU_DesignId_003
// Requirements : HL_SR488

    //.def     _mpuDisable_
pub unsafe fn _mpuDisable_() { 
	asm!("

        mrc   p15, #0, r0, c1, c0, #0
        bic   r0,  r0, #1
        dsb
        mcr   p15, #0, r0, c1, c0, #0
        isb
        bx    lr

        "
    );
}


//-------------------------------------------------------------------------------
// Enable Mpu background region
// SourceId : MPU_SourceId_004
// DesignId : MPU_DesignId_004
// Requirements : HL_SR488

    //.def     _mpuEnableBackgroundRegion_
pub unsafe fn _mpuEnableBackgroundRegion_() { 
	asm!("

        mrc   p15, #0, r0,      c1, c0, #0
        orr   r0,  r0, #0x20000
        mcr   p15, #0, r0,      c1, c0, #0
        bx    lr

        "
    );
}


//-------------------------------------------------------------------------------
// Disable Mpu background region
// SourceId : MPU_SourceId_005
// DesignId : MPU_DesignId_005
// Requirements : HL_SR488

    //.def     _mpuDisableBackgroundRegion_
pub unsafe fn _mpuDisableBackgroundRegion_() { 
	asm!("

        mrc   p15, #0, r0,      c1, c0, #0
        bic   r0,  r0, #0x20000
        mcr   p15, #0, r0,      c1, c0, #0
        bx    lr

        "
    );
}


//-------------------------------------------------------------------------------
// Returns number of implemented Mpu regions
// SourceId : MPU_SourceId_006
// DesignId : MPU_DesignId_006
// Requirements : HL_SR490

    //.def     _mpuGetNumberOfRegions_
pub unsafe fn _mpuGetNumberOfRegions_() -> u32 { 
	let retval: u32;
    asm!("

        mrc   p15, #0, r0,      c0, c0, #4
        uxtb  {0},  r0, ROR #8

        ", 
        out(reg) retval
    );
     retval
}


//-------------------------------------------------------------------------------
// Returns the type of the implemented mpu regions
// SourceId : MPU_SourceId_007
// DesignId : MPU_DesignId_007
// Requirements : HL_SR490

    //.def     _mpuAreRegionsSeparate_
pub unsafe fn _mpuAreRegionsSeparate_() -> u32 { 
	let retval: u32;
    asm!("

        mrc   p15, #0, r0,      c0, c0, #4
        uxtb  {0},  r0

        ", 
        out(reg) retval
    );
    retval
}


//-------------------------------------------------------------------------------
// Set mpu region number
// SourceId : MPU_SourceId_008
// DesignId : MPU_DesignId_008
// Requirements : HL_SR489

    //.def     _mpuSetRegion_
pub unsafe fn _mpuSetRegion_(region: u32)  { 
	asm!("

        mcr   p15, #0, r0, c6, c2, #0
        bx    lr

        ", 
        in("r0") region
    );
}


//-------------------------------------------------------------------------------
// Get mpu region number
// SourceId : MPU_SourceId_009
// DesignId : MPU_DesignId_009
// Requirements : HL_SR490

    //.def     _mpuGetRegion_
pub unsafe fn _mpuGetRegion_() -> u32 { 
	let retval: u32;
    asm!("

        mrc   p15, #0, r0, c6, c2, #0

        ",
        out("r0") retval
    );
    retval
}


//-------------------------------------------------------------------------------
// Set base address
// SourceId : MPU_SourceId_010
// DesignId : MPU_DesignId_010
// Requirements : HL_SR489

    //.def     _mpuSetRegionBaseAddress_
pub unsafe fn _mpuSetRegionBaseAddress_(address: u32) { 
	asm!("

        mcr   p15, #0, r0, c6, c1, #0
        bx    lr

        ", 
        in("r0") address
    );
}


//-------------------------------------------------------------------------------
// Get base address
// SourceId : MPU_SourceId_011
// DesignId : MPU_DesignId_011
// Requirements : HL_SR490

    //.def     _mpuGetRegionBaseAddress_
pub unsafe fn _mpuGetRegionBaseAddress_() -> u32 { 
	let retval: u32;
    asm!("

        mrc   p15, #0, r0, c6, c1, #0

        ", 
        out("r0") retval
    );
    retval
}



//-------------------------------------------------------------------------------
// Set type and permission
// SourceId : MPU_SourceId_012
// DesignId : MPU_DesignId_012
// Requirements : HL_SR489

    //.def     _mpuSetRegionTypeAndPermission_
pub unsafe fn _mpuSetRegionTypeAndPermission_(_type: u32, permission: u32) { 
	asm!("

        orr   r0,  {0}, {1}
        mcr   p15, #0, r0, c6, c1, #4
        bx    lr
        ",
        in(reg) _type,
        in(reg) permission
    );
}


//-------------------------------------------------------------------------------
// Get type
// SourceId : MPU_SourceId_013
// DesignId : MPU_DesignId_013
// Requirements : HL_SR490

    //.def     _mpuGetRegionType_
pub unsafe fn _mpuGetRegionType_() -> u32 { 
	let retval: u32;
    asm!("

        mrc   p15, #0, r0, c6, c1, #4
        bic   {0},  r0, #0xFF00

        ", 
        out(reg) retval
    );
    retval
}


//-------------------------------------------------------------------------------
// Get permission
// SourceId : MPU_SourceId_014
// DesignId : MPU_DesignId_014
// Requirements : HL_SR490

    //.def     _mpuGetRegionPermission_
pub unsafe fn _mpuGetRegionPermission_() -> u32 { 
	let retval: u32;
    asm!("

        mrc   p15, #0, r0, c6, c1, #4
        bic   {0},  r0, #0xFF

        ", 
        out(reg) retval
    );
    retval
}


//-------------------------------------------------------------------------------
// Set region size register value
// SourceId : MPU_SourceId_015
// DesignId : MPU_DesignId_015
// Requirements : HL_SR489

    //.def     _mpuSetRegionSizeRegister_
pub unsafe fn _mpuSetRegionSizeRegister_(value: u32) { 
	asm!("


        mcr   p15, #0, r0, c6, c1, #2
        bx    lr

        ", 
        in("r0") value
    );
}

    

//-------------------------------------------------------------------------------
