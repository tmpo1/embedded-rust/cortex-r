#![allow(unused_variables, dead_code, non_snake_case)]
use raw_pointer::*;
use crate::registers::reg_dcc::*;
use crate::registers::reg_gio::*;
use crate::registers::reg_spi::*;
use crate::registers::reg_het::*;
use crate::registers::reg_crc::*;

/** @file notification.c 
*   @brief User Notification Definition File
*   @date 11-Dec-2018
*   @version 04.07.01
*
*   This file  defines  empty  notification  routines to afn
*   linker errors, Driver expects user to define the notification. 
*   The user needs to either remove this file and use their custom 
*   notification function or place their code sequence in this file 
*   between the provided USER CODE BEGIN and USER CODE END.
*
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* USER CODE BEGIN (0) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(esmGroup1Notification)
pub fn esmGroup1Notification(channel: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (1) */
/* USER CODE END */
}

/* USER CODE BEGIN (2) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(esmGroup2Notification)
pub fn esmGroup2Notification(channel: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (3) */
/* USER CODE END */
}

/* USER CODE BEGIN (4) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(memoryPort0TestFailNotification)
pub fn memoryPort0TestFailNotification(groupSelect: u32, dataSelect: u32, address: u32, data: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (5) */
/* USER CODE END */
}

/* USER CODE BEGIN (6) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(memoryPort1TestFailNotification)
pub fn memoryPort1TestFailNotification(groupSelect: u32, dataSelect: u32, address: u32, data: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (7) */
/* USER CODE END */
}

/* USER CODE BEGIN (8) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(rtiNotification)
pub fn rtiNotification(notification: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (9) */
/* USER CODE END */
}

/* BEGIN COMMENT OUT NOT DEFINED MODULES
/* USER CODE BEGIN (10) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(adcNotification)
pub fn adcNotification(adc: Pointer<adcBASE_t>, group: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (11) */
/* USER CODE END */
}


/* USER CODE BEGIN (12) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(canErrorNotification)
pub fn canErrorNotification(node: Pointer<canBASE_t>, notification: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (13) */
/* USER CODE END */
}

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(canStatusChangeNotification)
pub fn canStatusChangeNotification(node: Pointer<canBASE_t>, notification: u32)  
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (14) */
/* USER CODE END */
}

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(canMessageNotification)
pub fn canMessageNotification(node: Pointer<canBASE_t>, messageBox: u32)  
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (15) */
/* USER CODE END */
}

END COMMENT OUT NOT DEFINED MODULES */

/* USER CODE BEGIN (16) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(dccNotification)
pub fn dccNotification(dcc: Pointer<dccBASE_t>,flags: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (17) */
/* USER CODE END */
}

/* USER CODE BEGIN (18) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(gioNotification)
pub fn gioNotification(port: Pointer<gioPORT_t>, bit: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (19) */
/* USER CODE END */
}

/* BEGIN COMMENT OUT NOT DEFINED MODULES
/* USER CODE BEGIN (20) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(linNotification)
pub fn linNotification(lin: Pointer<linBASE_t>, flags: u32)      
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (23) */
/* USER CODE END */
}

/* USER CODE BEGIN (24) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(mibspiNotification)
pub fn mibspiNotification(mibspi: Pointer<mibspiBASE_t>, flags: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (25) */
/* USER CODE END */
}

/* USER CODE BEGIN (26) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(mibspiGroupNotification)
pub fn mibspiGroupNotification(mibspi: Pointer<mibspiBASE_t>, group: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (27) */
/* USER CODE END */
}
/* USER CODE BEGIN (28) */
/* USER CODE END */

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(sciNotification)
pub fn sciNotification(sci: Pointer<sciBASE_t>, flags: u32)     
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (29) */
/* USER CODE END */
}

END COMMENT OUT NOT DEFINED MODULES */
/* USER CODE BEGIN (30) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(spiNotification)
pub fn spiNotification(spi: Pointer<spiBASE_t>, flags: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (31) */
/* USER CODE END */
}

/* USER CODE BEGIN (32) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(spiEndNotification)
pub fn spiEndNotification(spi: Pointer<spiBASE_t>)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (33) */
/* USER CODE END */
}

/* USER CODE BEGIN (34) */
/* USER CODE END */

/* BEGIN COMMENT OUT NOT DEFINED MODULES
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(pwmNotification)
pub fn pwmNotification(hetREG: Pointer<hetBASE_t>, pwm: u32, notification: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (35) */
/* USER CODE END */
}

/* USER CODE BEGIN (36) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(edgeNotification)
pub fn edgeNotification(hetREG: Pointer<hetBASE_t>, edge: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (37) */
/* USER CODE END */
}

END COMMENT OUT NOT DEFINED MODULES */

/* USER CODE BEGIN (38) */
/* USER CODE END */
/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(hetNotification)
pub fn hetNotification(het: Pointer<hetBASE_t>, offset: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (39) */
/* USER CODE END */
}

/* USER CODE BEGIN (40) */
/* USER CODE END */

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(crcNotification)
pub fn crcNotification(crc: Pointer<crcBASE_t>, flags: u32)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (41) */
/* USER CODE END */
}
/* USER CODE BEGIN (42) */
/* USER CODE END */

/* USER CODE BEGIN (43) */
/* USER CODE END */


/* USER CODE BEGIN (47) */
/* USER CODE END */
/* BEGIN COMMENT OUT NOT DEFINED MODULES

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma WEAK(eqepNotification)
pub fn eqepNotification(eqep: Pointer<eqepBASE_t>,flags: u16)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (48) */
/* USER CODE END */
}

END COMMENT OUT NOT DEFINED MODULES */
/* USER CODE BEGIN (49) */
/* USER CODE END */

/* USER CODE BEGIN (50) */
/* USER CODE END */


/* USER CODE BEGIN (53) */
/* USER CODE END */


/* USER CODE BEGIN (56) */
/* USER CODE END */

/* USER CODE BEGIN (58) */
/* USER CODE END */

/* USER CODE BEGIN (60) */
/* USER CODE END */
