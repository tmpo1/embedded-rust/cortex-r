#![allow(non_snake_case)]

use core::asm;


/* Author's Note: Return values are assumed to be r0, inputs are placed left to right in r0,r1...rN */
pub unsafe fn _coreInitRegisters_() {
    asm!(
        // we begin in supervisor mode (19)
        "cps {mode0}",
        "
            mov r0, lr
            mov r1, #0
            mov r2, #0
            mov r3, #0
            mov r4, #0
            mov r5, #0
            mov r6, #0
            mov r7, #0
            mov r8, #0
            mov r9, #0
            mov r10, #0
            mov r11, #0
            mov r12, #0
            mov r13, #0
            mrs r1, cpsr
            msr spsr_cxsf, r1
        ",
        // switch to FIQ mode (17)
        "cps {mode1}",
        "
            mov lr, r0
            mrs r1, cpsr
            msr spsr_cxsf, r1
        ",
        // switch to IRQ mode (18)
        "cps {mode2}",
        "
            mov lr, r0
            mrs r1, cpsr
            msr spsr_cxsf, r1
        ",
        // switch to abort mode (23)
        "cps {mode3}",
        "
            mov lr, r0
            mrs r1, cpsr
            msr spsr_cxsf, r1
        ",
        // switch to undefined instruction mode (27)
        "cps {mode4}",
        "
            mov lr, r0
            mrs r1, cpsr
            msr spsr_cxsf, r1
        ",
        // switch to system mode (31)
        "cps {mode5}",
        "
            mov lr, r0
            mrs r1, cpsr
            msr spsr_cxsf, r1
        ",
        // switch back to supervisor mode (19)
        "cps {mode0}",
        "bl next1",
        "next1: bl next2",
        "next2: bl next3",
        "next3: bl next4",
        "next4: bx r0",
        mode0 = const 19,
        mode1 = const 17,
        mode2 = const 18,
        mode3 = const 23,
        mode4 = const 27,
        mode5 = const 31
    );
}

pub unsafe fn _coreInitStackPointer_() {
    asm!(
        "cps {fiq}",
        "ldr sp, fiqSp",
        "cps {irq}",
        "ldr sp, irqSp",
        "cps {svc}",
        "ldr sp, svcSp",
        "cps {abort}",
        "ldr sp, abortSp",
        "cps {undef}",
        "ldr sp, undefSp",
        "cps {user}",
        "ldr sp, userSp",
        "bx lr",
        "
            userSp:     .word 0x08000000+0x00001000
            svcSp:      .word 0x08000000+0x00001000+0x00000100
            fiqSp:      .word 0x08000000+0x00001000+0x00000100+0x00000100
            irqSp:      .word 0x08000000+0x00001000+0x00000100+0x00000100+0x00000100
            abortSp:    .word 0x08000000+0x00001000+0x00000100+0x00000100+0x00000100+0x00000100
            undefSp:    .word 0x08000000+0x00001000+0x00000100+0x00000100+0x00000100+0x00000100+0x00000100
        ",
        fiq = const 17,
        irq = const 18,
        svc = const 19,
        abort = const 23,
        undef = const 27,
        user = const 31
    );
}

pub unsafe fn _getCPSRValue_() -> u32 {
    let retval: u32;
    asm!(
        "
        mrs r0, cpsr

        ", out("r0") retval
    );
    retval
}

pub unsafe fn _gotoCPUIdle_() {
    asm!(
        "
        WFI
        nop
        nop
        nop
        nop
        "
    );
}

pub unsafe fn _coreEnableEventBusExport_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0x00, r0, c9, c12, #0x00
        orr r0, r0, #0x10
        mcr p15, #0x00, r0, c9, c12, #0x00
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreDisableEventBusExport_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0x00, r0, c9, c12, #0x00
        bic r0, r0, #0x10
        mcr p15, #0x00, r0, c9, c12, #0x00
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreEnableRamEcc_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0x00, r0, c1, c0, #0x01
        orr r0, r0, #0x0C000000
        mcr p15, #0x00, r0, c1, c0, #0x01
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreDisableRamEcc_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0x00, r0, c1, c0, #0x01
        bic r0, r0, #0x0C000000
        mcr p15, #0x00, r0, c1, c0, #0x01
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreEnableFlashEcc_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0x00, r0, c1, c0, #0x01
        orr r0, r0, #0x02000000
        dmb
        mcr p15, #0x00, r0, c1, c0, #0x01
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreDisableFlashEcc_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0x00, r0, c1, c0, #0x01
        bic r0, r0, #0x02000000
        mcr p15, #0x00, r0, c1, c0, #0x01
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreEnableIrqVicOffset_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mrc p15, #0, r0, c1, c0, #0
        orr r0, r0, #0x01000000
        mcr p15, #0, r0, c1, c0, #0
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreGetDataFault_() -> u32 {
    let retval: u32;
    asm!(
        "mrc p15, #0, r0, c5, c0, #0",
        out("r0") retval
    );
    retval
}

pub unsafe fn _coreClearDataFault_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mov r0, #0
        mcr p15, #0, r0, c5, c0, #0
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreGetInstructionFault_() -> u32 {
    let retval: u32;
    asm!(
        "mrc p15, #0, r0, c5, c0, #1",
        out("r0") retval
    );
    retval
}

pub unsafe fn _coreClearInstructionFault_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mov r0, #0
        mcr p15, #0, r0, c5, c0, #1
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreGetDataFaultAddress_() -> u32 {
    let retval: u32;
    asm!(
        "mrc p15, #0, r0, c6, c0, #0",
        out("r0") retval
    );
    retval
}

pub unsafe fn _coreClearDataFaultAddress_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mov r0, #0
        mcr p15, #0, r0, c6, c0, #0
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreGetInstructionFaultAddress_() -> u32 {
    let retval: u32;
    asm!(
        "mrc p15, #0, r0, c6, c0, #2",
        out("r0") retval
    );
    retval
}

pub unsafe fn _coreClearInstructionFaultAddress_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mov r0, #0
        mcr p15, #0, r0, c6, c0, #2
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreGetAuxiliaryDataFault_() -> u32 {
    let retval: u32;
    asm!(
        "mrc p15, #0, r0, c5, c1, #0",
        out("r0") retval
    );
    retval
}

pub unsafe fn _coreClearAuxiliaryDataFault_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mov r0, #0
        mcr p15, #0, r0, c5, c1, #0
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _coreGetAuxiliaryInstructionFault_() -> u32 {
    let retval: u32;
    asm!(
        "mrc p15, #0, r0, c5, c1, #1",
        out("r0") retval
    );
    retval
}

pub unsafe fn _coreClearAuxiliaryInstructionFault_() {
    asm!(
        "
        stmfd sp!, {{r0}}
        mov r0, #0
        mcr p15, #0, r0, c5, c1, #1     @ typo on original source?
        ldmfd sp!, {{r0}}
        bx lr
        "
    );
}

pub unsafe fn _disable_interrupt_() {
    asm!(
        "
        cpsid if
        bx lr
        "
    );
}

pub unsafe fn _disable_FIQ_interrupt_() {
    asm!(
        "
        cpsid f
        bx lr
        "
    );
}

pub unsafe fn _disable_IRQ_interrupt_() {
    asm!(
        "
        cpsid i
        bx lr
        "
    );
}

pub unsafe fn _enable_interrupt_() {
    asm!(
        "
        cpsie if
        bx lr
        "
    );
}

pub unsafe fn _esmCcmErrorsClear_() {
    asm!(
        "
        stmfd sp!, {{r0-r2}}
        ldr r0, ESMSR1_REG      @ load the ESMSR1 status register address
        ldr r2, ESMSR1_ERR_CLR
        str r2, [r0]            @ clear the ESMSR1 register

        ldr r0, ESMSR2_REG      @ load the ESMSR2 status register address
        ldr r2, ESMSR2_ERR_CLR
        str r2, [r0]            @ clear the ESMSR2 register

        ldr r0, ESMSSR2_REG     @ load the ESMSSR2 status register address
        ldr r2, ESMSSR2_ERR_CLR
        str r2, [r0]            @ clear the ESMSSR2 register

        ldr r0, ESMKEY_REG      @ load the ESMKEY register address
        mov r2, #0x5            @ load R2 with 0x5
        str r2, [r0]            @ clear the ESMKEY register
        
        ldr r0, VIM_INTREQ      @ load the INTREQ register address
        ldr r2, VIM_INT_CLR
        str r2, [r0]            @ clear the INTREQ register

        ldr r0, CCMR4_STAT_REG  @ load the CCMR4 status register address
        ldr r2, CCMR4_ERR_CLR
        str r2, [r0]            @ clear the CCMR4 status register
        ldmfd sp!, {{r0-r2}}
        bx lr

        @ why are some of these not used?

        ESMSR1_REG:        .word 0xFFFFF518
        ESMSR2_REG:        .word 0xFFFFF51C
        ESMSR3_REG:        .word 0xFFFFF520
        ESMKEY_REG:        .word 0xFFFFF538
        ESMSSR2_REG:       .word 0xFFFFF53C
        CCMR4_STAT_REG:    .word 0xFFFFF600
        ERR_CLR_WRD:       .word 0xFFFFFFFF
        CCMR4_ERR_CLR:     .word 0x00010000
        ESMSR1_ERR_CLR:    .word 0x80000000
        ESMSR2_ERR_CLR:    .word 0x00000004
        ESMSSR2_ERR_CLR:   .word 0x00000004
        VIM_INT_CLR:       .word 0x00000001
        VIM_INTREQ:        .word 0xFFFFFE20
        "
    );
}

// TODO
// figure out how to best handle the base plus limit registers (which seem to be memory mapped)
