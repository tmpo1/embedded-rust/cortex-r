#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(dead_code)] //i was tired of the warnings

#[non_exhaustive]
pub struct ClockSource;

use crate::registers::{reg_system::*, reg_flash::*, reg_pcr::*};
use crate::peripherals::{pinmux};
use super::systest::*;
use super::sys_core::*;

// aliases
impl ClockSource {
    pub const OSCIN: u32 = 0;
    pub const FMPLL1: u32 = 1;
    pub const EXTCLKIN: u32 = 3;
    pub const LF_LPO: u32 = 4;
    pub const HF_LPO: u32 = 5;
    pub const VCLK: u32 = 9;
}

fn setupPLL() {
    // disable PLLs
    unsafe { SYSFRAME1.CSDISSET.write(2u32); }
    
    // wait for disable
    let mut disVal: u32 = SYSFRAME1.CSDIS.read();
    while disVal & 2u32 != 2u32 {
        disVal = SYSFRAME1.CSDIS.read();
    }

    // clear global status register
    unsafe { SYSFRAME1.GLBSTAT.write(0x301u32); }

    /* configure PLL control registers */
    
    let config_ctrl1: u32 = {
        0u32
            | 0u32              /* ROS = 0 */
            | 1u32 << 29        /* MASK_SLIP = 0x01 */
            | 0x1Fu32 << 24     /* PLLDIV = 0x1F */
            | 0u32              /* ROF = 0 */
            | 5u32 << 16        /* REFCLKDIV = 5, NR = 6 */
            | 0x9500u32         /* PLLMUL = 0x9500, NF = 150 */
    };
    unsafe { SYSFRAME1.PLLCTL1.write(config_ctrl1); }

    let config_ctrl2: u32 = {
        0u32
            | 0u32              /* FMENA = 0 */
            | 255u32 << 22      /* SPREADINGRATE = 255, NS = 256 */
            | 7u32 << 12        /* MULMOD = 7 */
            | 1u32 << 9         /* ODPLL = 1, OD = 2 */
            | 61u32             /* SPR_AMOUNT = 61 */
    };
    unsafe { SYSFRAME1.PLLCTL2.write(config_ctrl2); }

    // enable PLL(s) to start up or lock
    disVal = {
        0u32
            | 0x00u32
            | 0x08u32
            | 0x80u32
            | 0x00u32
            | 0x40u32
            | 0x00u32
    };
    unsafe { SYSFRAME1.CSDIS.write(disVal); }
}

fn trimLPO() {
    let clock_test: u32 = SYSFRAME1
        .CLKTEST
        .read();

    unsafe {
        SYSFRAME1
            .CLKTEST
            .write(
                clock_test
                    | 1u32 << 24
                    & (!(1u32 << 25) as u32)
            );
    }

    let trim_value: u32 = (*LPO_TRIM_VALUE & 0xFFFF0000u32) >> 16;

    let trim: u32;
    if trim_value != 0xFFFFu32 {
        trim = {
            0u32
                | 1u32 << 24    /* enable bias */
                | trim_value    /* trim value */
        };
    } else {
        trim = {
            0u32
                | 1u32 << 24    /* enable bias */
                | 16u32 << 16   /* HFTRIM = 0x10 */
                | 16u32         /* LFTRIM = 0x10 */
        };
    }

    unsafe { SYSFRAME1.LPOMONCTL.write(trim); }

    // restore clock test configuration
    unsafe {
        SYSFRAME1
            .CLKTEST
            .write(clock_test);
    }
}

fn setupFlash() {
    // set up flash read mode, address wait states and data wait states
    let opt_ctrl: u32 = {
        0u32
            | 1u32 << 8         /* RWAIT = 1 */
            | 0u32 << 4         /* ASWSTEN = 0 */
            | 1u32              /* ENPIPE = 1 */
    };
    unsafe { flashREG.FRDCNTL.write(opt_ctrl); }
    
    // set up flash access wait states for bank 7
    unsafe { flashREG.FSM_WR_ENA.write(0x5u32); }
    let ee_config: u32 = {
        2u32
            | 4u32 << 16        /* wait states for bank 7 */
    };
    unsafe { flashREG.EEPROM_CONFIG.write(ee_config); }

    // disable write access to flash state machine registers
    unsafe { flashREG.FSM_WR_ENA.write(0xAu32); }

    // set up flash bank power modes
    let fallback: u32 = {
        0u32
            | (FlashPowerMode::ACTIVE as u32) << 14      /* BANK 7 */
            | (FlashPowerMode::ACTIVE as u32) << 0       /* BANK 0 */
    };
    unsafe { flashREG.FBFALLBACK.write(fallback); }
}

fn periphInit() {
    // disable peripherals before powerup
    let mut clk_control: u32 = SYSFRAME1
        .CLKCNTL
        .read();
    unsafe { SYSFRAME1.CLKCNTL.write(clk_control & 0xFFFFFEFFu32); }

    let clear_val: u32 = -1i32 as u32;
    unsafe {
        PCRREG
            .PSPWRDWNCLR0
            .write(clear_val);

        PCRREG
            .PSPWRDWNCLR1
            .write(clear_val);

        PCRREG
            .PSPWRDWNCLR2
            .write(clear_val);

        PCRREG
            .PSPWRDWNCLR3
            .write(clear_val);
    }

    // enable peripherals
    clk_control = SYSFRAME1
        .CLKCNTL
        .read();
    unsafe { SYSFRAME1.CLKCNTL.write(clk_control | 0x0100u32); }
}

fn mapClocks() {
    let domain_config = {
        0u32
            | 0u32 << 4     /* AVCLK1, 1 - OFF, 0 - ON */
            | 1u32 << 5     /* AVCLK2, 1 - OFF, 0 - ON */
            | 0u32 << 8     /* AVCLK3, 1 - OFF, 0 - ON */
            | 0u32 << 10    /* AVCLK4, 1 - OFF, 0 - ON */
            | 0u32 << 11    /* AVCLK5, 1 - OFF, 0 - ON */
    };
    unsafe { SYSFRAME1.CDDIS.write(domain_config); }

    let mut status: u32 = SYSFRAME1.CSVSTAT.read();
    let mut source: u32 = SYSFRAME1.CSDIS.read();
    while (status & ((source ^ 0xFFu32) & 0xFFu32)) != ((source ^ 0xFFu32) & 0xFFu32) {
        status = SYSFRAME1.CSVSTAT.read();
        source = SYSFRAME1.CSDIS.read();
    }

    unsafe {
        SYSFRAME1.GHVSRC.write(
            0u32
            | ClockSource::OSCIN << 24
            | ClockSource::OSCIN << 16
            | ClockSource::FMPLL1
        );

        SYSFRAME1.RCLKSRC.write(
            0u32
            | 1u32 << 24
            | ClockSource::VCLK << 16
            | 1u32 << 8
            | ClockSource::VCLK
        );

        SYSFRAME1.VCLKASRC.write(
            0u32
            | ClockSource::VCLK << 8
            | ClockSource::VCLK
        );
    }

    let mut clk_control: u32 = SYSFRAME1.CLKCNTL.read();
    clk_control &= 0xF0F0FFFFu32;
    unsafe { SYSFRAME1.CLKCNTL.write(clk_control); }
}

pub fn sys_init() {

    /* beginning with step 7 of bootloader */
    let efc_status: u32;
    
    // configure PLL control registers and enable PLLs
    setupPLL();

    // run the eFuse controller start-up checks and
    // start the self-test on the eFuse controller SECDED logic
    efc_status = efcCheck();

    // enable clocks to peripherals and release peripheral reset
    periphInit();

    // set up the device-level multiplexing
    unsafe { pinmux::muxInit(); }

    // wait for the eFuse controller ECC logic self-test to complete
    if efc_status == 0u32 {
        if !efcSelfTestCheck() {
            selfTestFailNotify(FailCode::EFC(1));
        }
    }
    
    if efc_status == 2u32 {
        if !efcSelfTestCheck() {
            selfTestFailNotify(FailCode::EFC(2));
        } else {
            selfTestFailNotify(FailCode::EFC(3));
        }
    }

    // set up Flash
    setupFlash();

    // trim the LPO
    trimLPO();

    // run a self test on the SECDED logic embedded in the Flash module
    flashErrSelfTest();

    // map the clock domains to the desired clock sources
    mapClocks();

    // reduce the values of the R-dividers in steps
    let pll_ctl1: u32 = SYSFRAME1.PLLCTL1.read();
    let pll_ctl1 = (pll_ctl1 & 0xE0FFFFFFu32) | (1u32 << 24);
    unsafe { SYSFRAME1.PLLCTL1.write(pll_ctl1); }

    /* ending with step 19 of the bootloader */

    unsafe {

        SYSFRAME1.SYSPC1.write(0u32);   // set ECLK pins functional mode

        SYSFRAME1.SYSPC4.write(0u32);   // set ECLK pins default output value

        SYSFRAME1.SYSPC2.write(1u32);   // set ECLK pins output direction

        SYSFRAME1.SYSPC7.write(0u32);   // set ECLK pins open drain enable

        SYSFRAME1.SYSPC8.write(0u32);   // set ECLK pins pullup/pulldown enable

        SYSFRAME1.SYSPC9.write(1u32);   // set ECLK pins pullup/pulldown select

        // set up ECLK
        SYSFRAME1.ECPCNTL.write(7u32 & 0xFFFFu32);

    }

}

pub unsafe fn sys_shutdown(mode: u32) {
    SYSFRAME1.CSDISSET.write(mode & 0xFFu32);

    SYSFRAME1.CDDIS.write((mode >> 8) & 0xFFFu32);

    _gotoCPUIdle_();
}
