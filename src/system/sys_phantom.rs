#![allow(dead_code, non_snake_case)]
/** @file sys_phantom.c 
*   @brief Phantom Interrupt Source File
*   @date 11-Dec-2018
*   @version 04.07.01
*
*   This file contains:
*   - Phantom Interrupt Handler
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* Phantom Interrupt Handler */

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma CODE_STATE(phantomInterrupt, 32)
// #pragma INTERRUPT(phantomInterrupt, IRQ)
// #pragma WEAK(phantomInterrupt)

fn phantomInterrupt() {

}

