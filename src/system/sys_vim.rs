#![allow(non_camel_case_types, non_snake_case, non_upper_case_globals, )]

use crate::registers::reg_vim::*;
use crate::registers::reg_esm::*;
use crate::system::notification::*;
use raw_pointer::*;
use crate::registers::sys_common::*;


/** @file sys_vim.c 
*   @brief VIM Driver Implementation File
*   @date 11-Dec-2018
*   @version 04.07.01
*
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/



/* Vim Ram Definition */
/** @struct vimRam
*   @brief Vim Ram Definition
*
*   This type is used to access the Vim Ram.
*/
/** @typedef vimRAM_t
*   @brief Vim Ram Type Definition
*
*   This type is used to access the Vim Ram.
*/
// typedef volatile struct vimRam
// {
//     t_isrFuncPTR ISR[VIM_CHANNELS];
// } vimRAM_t;

pub struct vimRAM_t {
    pub ISR: [t_isrFuncPTR; VIM_CHANNELS as usize],
} 

// #define vimRAM ((vimRAM_t *)0xFFF82000U)
pub static mut vimRAM: Pointer<vimRAM_t> = Pointer::<vimRAM_t> {
	ptr: 0xFFF82000u32 as *mut vimRAM_t
};

/* Author's Note: phantomInterrupt and esmHighInterrupt definitions are placeholders and should be removed once sys_phantom.c and esm.c are ported to this project. */
pub fn phantomInterrupt() {
    return;
}

pub fn esmHighInterrupt() {
    return;
}

pub const s_vim_init: [t_isrFuncPTR; 96] =
[
    phantomInterrupt,
    esmHighInterrupt,            /* Channel 0 */
    phantomInterrupt,            /* Channel 1 */
    phantomInterrupt,            /* Channel 2 */
    phantomInterrupt,            /* Channel 3 */
    phantomInterrupt,            /* Channel 4 */
    phantomInterrupt,            /* Channel 5 */
    phantomInterrupt,            /* Channel 6 */
    phantomInterrupt,            /* Channel 7 */
    phantomInterrupt,            /* Channel 8 */
    phantomInterrupt,            /* Channel 9 */
    phantomInterrupt,            /* Channel 10 */
    phantomInterrupt,            /* Channel 11 */
    phantomInterrupt,            /* Channel 12 */
    phantomInterrupt,            /* Channel 13 */
    phantomInterrupt,            /* Channel 14 */
    phantomInterrupt,            /* Channel 15 */
    phantomInterrupt,            /* Channel 16 */
    phantomInterrupt,            /* Channel 17 */
    phantomInterrupt,            /* Channel 18 */
    phantomInterrupt,            /* Channel 19 */
    phantomInterrupt,            /* Channel 20 */
    phantomInterrupt,            /* Channel 21 */
    phantomInterrupt,            /* Channel 22 */
    phantomInterrupt,            /* Channel 23 */
    phantomInterrupt,            /* Channel 24 */
    phantomInterrupt,            /* Channel 25 */
    phantomInterrupt,            /* Channel 26 */
    phantomInterrupt,            /* Channel 27 */
    phantomInterrupt,            /* Channel 28 */
    phantomInterrupt,            /* Channel 29 */
    phantomInterrupt,            /* Channel 30 */
    phantomInterrupt,            /* Channel 31 */
    phantomInterrupt,            /* Channel 32 */
    phantomInterrupt,            /* Channel 33 */
    phantomInterrupt,            /* Channel 34 */
    phantomInterrupt,            /* Channel 35 */
    phantomInterrupt,            /* Channel 36 */
    phantomInterrupt,            /* Channel 37 */
    phantomInterrupt,            /* Channel 38 */
    phantomInterrupt,            /* Channel 39 */
    phantomInterrupt,            /* Channel 40 */
    phantomInterrupt,            /* Channel 41 */
    phantomInterrupt,            /* Channel 42 */
    phantomInterrupt,            /* Channel 43 */
    phantomInterrupt,            /* Channel 44 */
    phantomInterrupt,            /* Channel 45 */
    phantomInterrupt,            /* Channel 46 */
    phantomInterrupt,            /* Channel 47 */
    phantomInterrupt,            /* Channel 48 */
    phantomInterrupt,            /* Channel 49 */
    phantomInterrupt,            /* Channel 50 */
    phantomInterrupt,            /* Channel 51 */
    phantomInterrupt,            /* Channel 52 */
    phantomInterrupt,            /* Channel 53 */
    phantomInterrupt,            /* Channel 54 */
    phantomInterrupt,            /* Channel 55 */
    phantomInterrupt,            /* Channel 56 */
    phantomInterrupt,            /* Channel 57 */
    phantomInterrupt,            /* Channel 58 */
    phantomInterrupt,            /* Channel 59 */
    phantomInterrupt,            /* Channel 60 */
    phantomInterrupt,            /* Channel 61 */
    phantomInterrupt,            /* Channel 62 */
    phantomInterrupt,            /* Channel 63 */
    phantomInterrupt,            /* Channel 64 */
    phantomInterrupt,            /* Channel 65 */
    phantomInterrupt,            /* Channel 66 */
    phantomInterrupt,            /* Channel 67 */
    phantomInterrupt,            /* Channel 68 */
    phantomInterrupt,            /* Channel 69 */
    phantomInterrupt,            /* Channel 70 */
    phantomInterrupt,            /* Channel 71 */
    phantomInterrupt,            /* Channel 72 */
    phantomInterrupt,            /* Channel 73 */
    phantomInterrupt,            /* Channel 74 */
    phantomInterrupt,            /* Channel 75 */
    phantomInterrupt,            /* Channel 76 */
    phantomInterrupt,            /* Channel 77 */
    phantomInterrupt,            /* Channel 78 */
    phantomInterrupt,            /* Channel 79 */
    phantomInterrupt,            /* Channel 80 */
    phantomInterrupt,            /* Channel 81 */
    phantomInterrupt,            /* Channel 82 */
    phantomInterrupt,            /* Channel 83 */
    phantomInterrupt,            /* Channel 84 */
    phantomInterrupt,            /* Channel 85 */
    phantomInterrupt,            /* Channel 86 */
    phantomInterrupt,            /* Channel 87 */
    phantomInterrupt,            /* Channel 88 */
    phantomInterrupt,            /* Channel 89 */
    phantomInterrupt,            /* Channel 90 */
    phantomInterrupt,            /* Channel 91 */
    phantomInterrupt,            /* Channel 92 */
    phantomInterrupt,            /* Channel 93 */
    phantomInterrupt,            /* Channel 94 */
];

// pub fn vimParityErrorHandler();

/** @fn void vimInit(void)
*   @brief Initializes VIM module
*
*   This function initializes VIM RAM and registers
*/
/* SourceId : VIM_SourceId_001 */
/* DesignId : VIM_DesignId_001 */
/* Requirements : HL_SR100 */
pub unsafe fn vimInit()
{
	/* VIM RAM Parity Enable */
	// VIM_PARCTL = 0xAU;
    /* Author's Note: Not sure if this is translated correctly */
    VIM_PARCTL.write(0xAu32);

	/* Initialize VIM table */
    {
        // for (i = 0U; i < VIM_CHANNELS; i++)
        for i in 0..VIM_CHANNELS
        {
            vimRAM.ISR[i as usize] = s_vim_init[i as usize];
        }
    }
    
    /* Set Fall-Back Address Parity Error Register */
	/*SAFETYMCUSW 439 S MR:11.3 <APPROVED> " Need to store the address of a function in a 32 bit register - Advisory as per MISRA" */
    
    /* Author's Note: UNCOMMENT REMEMBER TO UNCOMMENT THIS!! */
    // VIM_FBPARERR = &vimParityErrorHandler as u32;

    /* set IRQ/FIQ priorities */
vimREG.FIRQPR0.write(((systemInterrupt_t::SYS_FIQ as u32) << 0u32)
| ((systemInterrupt_t::SYS_FIQ as u32) << 1u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 2u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 3u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 4u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 5u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 6u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 7u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 8u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 9u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 10u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 11u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 12u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 13u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 14u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 15u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 16u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 17u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 18u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 19u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 20u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 21u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 22u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 23u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 24u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 25u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 26u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 27u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 28u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 29u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 30u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 31u32));

vimREG.FIRQPR1.write(((systemInterrupt_t::SYS_IRQ as u32) << 0u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 1u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 2u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 3u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 4u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 5u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 6u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 7u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 8u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 9u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 10u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 11u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 12u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 13u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 14u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 15u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 16u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 17u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 18u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 19u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 20u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 21u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 22u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 23u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 24u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 25u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 26u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 27u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 28u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 29u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 30u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 31u32));


vimREG.FIRQPR2.write(((systemInterrupt_t::SYS_IRQ as u32) << 0u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 1u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 2u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 3u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 4u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 5u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 6u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 7u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 8u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 9u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 10u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 11u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 12u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 13u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 14u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 15u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 16u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 17u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 18u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 19u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 20u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 21u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 22u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 23u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 24u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 25u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 26u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 27u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 28u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 29u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 30u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 31u32));

vimREG.FIRQPR3.write(((systemInterrupt_t::SYS_IRQ as u32) << 0u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 1u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 2u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 3u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 4u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 5u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 6u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 7u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 8u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 9u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 10u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 11u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 12u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 13u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 14u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 15u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 16u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 17u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 18u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 19u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 20u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 21u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 22u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 23u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 24u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 25u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 26u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 27u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 28u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 29u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 30u32)
| ((systemInterrupt_t::SYS_IRQ as u32) << 31u32));


/* enable interrupts */
vimREG.REQMASKSET0.write((1u32 << 0u32)
    | (1u32 << 1u32)
    | (0u32 << 2u32)
    | (0u32 << 3u32)
    | (0u32 << 4u32)
    | (0u32 << 5u32)
    | (0u32 << 6u32)
    | (0u32 << 7u32)
    | (0u32 << 8u32)
    | (0u32 << 9u32)
    | (0u32 << 10u32)
    | (0u32 << 11u32)
    | (0u32 << 12u32)
    | (0u32 << 13u32)
    | (0u32 << 14u32)
    | (0u32 << 15u32)
    | (0u32 << 16u32)
    | (0u32 << 17u32)
    | (0u32 << 18u32)
    | (0u32 << 19u32)
    | (0u32 << 20u32)
    | (0u32 << 21u32)
    | (0u32 << 22u32)
    | (0u32 << 23u32)
    | (0u32 << 24u32)
    | (0u32 << 25u32)
    | (0u32 << 26u32)
    | (0u32 << 27u32)
    | (0u32 << 28u32)
    | (0u32 << 29u32)
    | (0u32 << 30u32)
    | (0u32 << 31u32));

vimREG.REQMASKSET1.write((0u32 << 0u32)
    | (0u32 << 1u32)
    | (0u32 << 2u32)
    | (0u32 << 3u32)
    | (0u32 << 4u32)
    | (0u32 << 5u32)
    | (0u32 << 6u32)
    | (0u32 << 7u32)
    | (0u32 << 8u32)
    | (0u32 << 9u32)
    | (0u32 << 10u32)
    | (0u32 << 11u32)
    | (0u32 << 12u32)
    | (0u32 << 13u32)
    | (0u32 << 14u32)
    | (0u32 << 15u32)
    | (0u32 << 16u32)
    | (0u32 << 17u32)
    | (0u32 << 18u32)
    | (0u32 << 19u32)
    | (0u32 << 20u32)
    | (0u32 << 21u32)
    | (0u32 << 22u32)
    | (0u32 << 23u32)
    | (0u32 << 24u32)
    | (0u32 << 25u32)
    | (0u32 << 26u32)
    | (0u32 << 27u32)
    | (0u32 << 28u32)
    | (0u32 << 29u32)
    | (0u32 << 30u32)
    | (0u32 << 31u32));

vimREG.REQMASKSET2.write((0u32 << 0u32)
    | (0u32 << 1u32)
    | (0u32 << 2u32)
    | (0u32 << 3u32)
    | (0u32 << 4u32)
    | (0u32 << 5u32)
    | (0u32 << 6u32)
    | (0u32 << 7u32)
    | (0u32 << 8u32)
    | (0u32 << 9u32)
    | (0u32 << 10u32)
    | (0u32 << 11u32)
    | (0u32 << 12u32)
    | (0u32 << 13u32)
    | (0u32 << 14u32)
    | (0u32 << 15u32)
    | (0u32 << 16u32)
    | (0u32 << 17u32)
    | (0u32 << 18u32)
    | (0u32 << 19u32)
    | (0u32 << 20u32)
    | (0u32 << 21u32)
    | (0u32 << 22u32)
    | (0u32 << 23u32)
    | (0u32 << 24u32)
    | (0u32 << 25u32)
    | (0u32 << 26u32)
    | (0u32 << 27u32)
    | (0u32 << 28u32)
    | (0u32 << 29u32)
    | (0u32 << 30u32)
    | (0u32 << 31u32));
    
vimREG.REQMASKSET3.write((0u32 << 0u32)
    | (0u32 << 1u32)
    | (0u32 << 2u32)
    | (0u32 << 3u32)
    | (0u32 << 4u32)
    | (0u32 << 5u32)
    | (0u32 << 6u32)
    | (0u32 << 7u32)
    | (0u32 << 8u32)
    | (0u32 << 9u32)
    | (0u32 << 10u32)
    | (0u32 << 11u32)
    | (0u32 << 12u32)
    | (0u32 << 13u32)
    | (0u32 << 14u32)
    | (0u32 << 15u32)
    | (0u32 << 16u32)
    | (0u32 << 17u32)
    | (0u32 << 18u32)
    | (0u32 << 19u32)
    | (0u32 << 20u32)
    | (0u32 << 21u32)
    | (0u32 << 22u32)
    | (0u32 << 23u32)
    | (0u32 << 24u32)
    | (0u32 << 25u32)
    | (0u32 << 26u32)
    | (0u32 << 27u32)
    | (0u32 << 28u32)
    | (0u32 << 29u32)
    | (0u32 << 30u32)
    | (0u32 << 31u32));    

/* Set Capture event sources */
vimREG.CAPEVT.write((0u32 << 0u32)
 | (0u32 << 16u32));
}


/** @fn void vimChannelMap(request: u32, channel: u32, handler: t_isrFuncPTR)
*   @brief Map selected interrupt request to the selected channel
*
*    @param[in] request: Interrupt request number 2..95
*    @param[in] channel: VIM Channel number 2..95
*    @param[in] handler: Address of the interrupt handler
*
*   This function will map selected interrupt request to the selected channel.
*
*/
/* SourceId : VIM_SourceId_002 */
/* DesignId : VIM_DesignId_002 */
/* Requirements : HL_SR101 */
pub fn vimChannelMap(request: u32, channel: u32, handler: t_isrFuncPTR)
{
    let i: usize;
    let mut j: usize;
    i = (channel as usize) >> 2usize;              /* Find the register to configure */
    j = (channel as usize) - (i << 2usize);        /* Find the offset of the type    */
    j = 3usize - j;                     /* reverse the byte order         */
    j = j << 3usize;                    /* find the bit location          */

    /*Mapping the required interrupt request to the required channel*/
    unsafe {
        vimREG.CHANCTRL[i].write(vimREG.CHANCTRL[i].read() & !(0xFFu32 << j));
        vimREG.CHANCTRL[i].write(vimREG.CHANCTRL[i].read() | (request << j));
        /*Updating VIMRAM*/
        vimRAM.ISR[(channel as usize) + 1] = handler;
    }
}


/** @fn void vimEnableInterrupt(channel: u32, boolean inttype)
*   @brief Enable interrupt for the the selected channel
*
*    @param[in] channel: VIM Channel number 2..95
*    @param[in] inttype: Interrupt type
*                        - SYS_IRQ: Selected channel will be enabled as IRQ
*                        - SYS_FIQ: Selected channel will be enabled as FIQ
*
*   This function will enable interrupt for the selected channel.
*
*/
/* SourceId : VIM_SourceId_003 */
/* DesignId : VIM_DesignId_003 */
/* Requirements : HL_SR102 */
pub unsafe fn vimEnableInterrupt(channel: u32, inttype: systemInterrupt_t)
{
    if channel >= 64u32
    {
        if (inttype as u32) == (systemInterrupt_t::SYS_IRQ as u32)
        {
            vimREG.FIRQPR2.write(vimREG.FIRQPR2.read() & !(1u32 << (channel-64u32)));
        }
        else
        {
            vimREG.FIRQPR2.write(vimREG.FIRQPR2.read() | (1u32 << (channel-64u32)));
        }
        vimREG.REQMASKSET2.write(1u32 << (channel-64u32));
    }
    else if channel >= 32u32
    {
        if (inttype as u32) == (systemInterrupt_t::SYS_IRQ as u32)
        {
            vimREG.FIRQPR1.write(vimREG.FIRQPR1.read() & !(1u32 << (channel-32u32)));
        }
        else
        {
            vimREG.FIRQPR1.write(vimREG.FIRQPR1.read() | (1u32 << (channel-32u32)));
        }
        vimREG.REQMASKSET1.write(1u32 << (channel-32u32));
    }
    else if channel >= 2u32
    {
        if (inttype as u32) == (systemInterrupt_t::SYS_IRQ as u32)
        {
            vimREG.FIRQPR0.write(vimREG.FIRQPR0.read() & !(1u32 << channel));
        }
        else
        {
            vimREG.FIRQPR0.write(vimREG.FIRQPR0.read() | (1u32 << channel));
        }
        vimREG.REQMASKSET0.write(1u32 << channel);
    }
    else
    {
		/* Empty */
    }
}

/** @fn void vimDisableInterrupt(channel: u32)
*   @brief Disable interrupt for the the selected channel
*
*    @param[in] channel: VIM Channel number 2..95
*
*   This function will disable interrupt for the selected channel.
*
*/
/* SourceId : VIM_SourceId_004 */
/* DesignId : VIM_DesignId_004 */
/* Requirements : HL_SR103 */
pub unsafe fn vimDisableInterrupt(channel: u32)
{
    if channel >= 64u32
    {
        vimREG.REQMASKCLR2.write(1u32 << (channel-64u32));
    }
    else if channel >=32u32
    {
        vimREG.REQMASKCLR1.write(1u32 << (channel-32u32));
    }
    else if channel >= 2u32
    {
        vimREG.REQMASKCLR0.write(1u32 << channel);
    }
    else
    {
		/* Empty */
    }
}

/* USER CODE BEGIN (1) */
/* USER CODE END */

/** @fn void vimGetConfigValue(vim_config_reg_t *config_reg, config_value_type_t type)
*   @brief Get the initial or current values of the configuration registers
*
*    @param[in] *config_reg: pointer to the struct to which the initial or current value of the configuration registers need to be stored
*    @param[in] type:     whether initial or current value of the configuration registers need to be stored
*                        - InitialValue: initial value of the configuration registers will be stored in the struct pointed by config_reg
*                        - CurrentValue: initial value of the configuration registers will be stored in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type') of the configuration 
*   registers to the struct pointed by config_reg
*
*/
/* SourceId : VIM_SourceId_005 */
/* DesignId : VIM_DesignId_005 */
/* Requirements : HL_SR104 */
pub fn vimGetConfigValue(config_reg: &mut vim_config_reg_t, _type: config_value_type_t )
{  
    if let config_value_type_t::InitialValue = _type {
        config_reg.CONFIG_FIRQPR0 = VIM_FIRQPR0_CONFIGVALUE;
        config_reg.CONFIG_FIRQPR1 = VIM_FIRQPR1_CONFIGVALUE;
        config_reg.CONFIG_FIRQPR2 = VIM_FIRQPR2_CONFIGVALUE;
        config_reg.CONFIG_FIRQPR3 = VIM_FIRQPR3_CONFIGVALUE;
        config_reg.CONFIG_REQMASKSET0 = VIM_REQMASKSET0_CONFIGVALUE;
        config_reg.CONFIG_REQMASKSET1 = VIM_REQMASKSET1_CONFIGVALUE;
        config_reg.CONFIG_REQMASKSET2 = VIM_REQMASKSET2_CONFIGVALUE;
        config_reg.CONFIG_REQMASKSET3 = VIM_REQMASKSET3_CONFIGVALUE;
        config_reg.CONFIG_WAKEMASKSET0 = VIM_WAKEMASKSET0_CONFIGVALUE;
        config_reg.CONFIG_WAKEMASKSET1 = VIM_WAKEMASKSET1_CONFIGVALUE;
        config_reg.CONFIG_WAKEMASKSET2 = VIM_WAKEMASKSET2_CONFIGVALUE;
        config_reg.CONFIG_WAKEMASKSET3 = VIM_WAKEMASKSET3_CONFIGVALUE;
        config_reg.CONFIG_CAPEVT = VIM_CAPEVT_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[0] = VIM_CHANCTRL0_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[1] = VIM_CHANCTRL1_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[2] = VIM_CHANCTRL2_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[3] = VIM_CHANCTRL3_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[4] = VIM_CHANCTRL4_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[5] = VIM_CHANCTRL5_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[6] = VIM_CHANCTRL6_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[7] = VIM_CHANCTRL7_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[8] = VIM_CHANCTRL8_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[9] = VIM_CHANCTRL9_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[10] = VIM_CHANCTRL10_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[11] = VIM_CHANCTRL11_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[12] = VIM_CHANCTRL12_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[13] = VIM_CHANCTRL13_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[14] = VIM_CHANCTRL14_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[15] = VIM_CHANCTRL15_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[16] = VIM_CHANCTRL16_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[17] = VIM_CHANCTRL17_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[18] = VIM_CHANCTRL18_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[19] = VIM_CHANCTRL19_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[20] = VIM_CHANCTRL20_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[21] = VIM_CHANCTRL21_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[22] = VIM_CHANCTRL22_CONFIGVALUE;
		config_reg.CONFIG_CHANCTRL[23] = VIM_CHANCTRL23_CONFIGVALUE;
    }
    else
    {
	/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        config_reg.CONFIG_FIRQPR0 = vimREG.FIRQPR0.read();
        config_reg.CONFIG_FIRQPR1 = vimREG.FIRQPR1.read();
        config_reg.CONFIG_FIRQPR2 = vimREG.FIRQPR2.read();
        config_reg.CONFIG_FIRQPR3 = vimREG.FIRQPR3.read();
        config_reg.CONFIG_REQMASKSET0 = vimREG.REQMASKSET0.read();
        config_reg.CONFIG_REQMASKSET1 = vimREG.REQMASKSET1.read();
        config_reg.CONFIG_REQMASKSET2 = vimREG.REQMASKSET2.read();
        config_reg.CONFIG_REQMASKSET3 = vimREG.REQMASKSET3.read();
        config_reg.CONFIG_WAKEMASKSET0 = vimREG.WAKEMASKSET0.read();
        config_reg.CONFIG_WAKEMASKSET1 = vimREG.WAKEMASKSET1.read();
        config_reg.CONFIG_WAKEMASKSET2 = vimREG.WAKEMASKSET2.read();
        config_reg.CONFIG_WAKEMASKSET3 = vimREG.WAKEMASKSET3.read();
        config_reg.CONFIG_CAPEVT = vimREG.CAPEVT.read();
		config_reg.CONFIG_CHANCTRL[0] = vimREG.CHANCTRL[0].read();
		config_reg.CONFIG_CHANCTRL[1] = vimREG.CHANCTRL[1].read();
		config_reg.CONFIG_CHANCTRL[2] = vimREG.CHANCTRL[2].read();
		config_reg.CONFIG_CHANCTRL[3] = vimREG.CHANCTRL[3].read();
		config_reg.CONFIG_CHANCTRL[4] = vimREG.CHANCTRL[4].read();
		config_reg.CONFIG_CHANCTRL[5] = vimREG.CHANCTRL[5].read();
		config_reg.CONFIG_CHANCTRL[6] = vimREG.CHANCTRL[6].read();
		config_reg.CONFIG_CHANCTRL[7] = vimREG.CHANCTRL[7].read();
		config_reg.CONFIG_CHANCTRL[8] = vimREG.CHANCTRL[8].read();
		config_reg.CONFIG_CHANCTRL[9] = vimREG.CHANCTRL[9].read();
		config_reg.CONFIG_CHANCTRL[10] = vimREG.CHANCTRL[10].read();
		config_reg.CONFIG_CHANCTRL[11] = vimREG.CHANCTRL[11].read();
		config_reg.CONFIG_CHANCTRL[12] = vimREG.CHANCTRL[12].read();
		config_reg.CONFIG_CHANCTRL[13] = vimREG.CHANCTRL[13].read();
		config_reg.CONFIG_CHANCTRL[14] = vimREG.CHANCTRL[14].read();
		config_reg.CONFIG_CHANCTRL[15] = vimREG.CHANCTRL[15].read();
		config_reg.CONFIG_CHANCTRL[16] = vimREG.CHANCTRL[16].read();
		config_reg.CONFIG_CHANCTRL[17] = vimREG.CHANCTRL[17].read();
		config_reg.CONFIG_CHANCTRL[18] = vimREG.CHANCTRL[18].read();
		config_reg.CONFIG_CHANCTRL[19] = vimREG.CHANCTRL[19].read();
		config_reg.CONFIG_CHANCTRL[20] = vimREG.CHANCTRL[20].read();
		config_reg.CONFIG_CHANCTRL[21] = vimREG.CHANCTRL[21].read();
		config_reg.CONFIG_CHANCTRL[22] = vimREG.CHANCTRL[22].read();
		config_reg.CONFIG_CHANCTRL[23] = vimREG.CHANCTRL[23].read();


        
    }
}


/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAU132/the-weak-pragma-stdz0556591.html
*/
// #pragma CODE_STATE(vimParityErrorHandler, 32)
// #pragma INTERRUPT(vimParityErrorHandler, IRQ)
// #pragma WEAK(vimParityErrorHandler)

/* SourceId : VIM_SourceId_006 */
/* DesignId : VIM_DesignId_006 */
/* Requirements : HL_SR105 */
pub fn vimParityErrorHandler()
{
    let mut vec: u32;
    
    /* Identify the corrupted address */
    /*Author's Note: Intention is to get ptr addr */
    let error_addr: u32 = VIM_ADDERR.ptr as u32;

    /* Identify the channel number */
    let error_channel:u32 = (error_addr & 0x1FFu32) >> 2u32;

	if error_channel >= VIM_CHANNELS
	{
        /* Index is out of bonds */
        /* This condition should never be true since the HW only implements VIM_CHANNELS (96) channels */
        /* However, it was added due to defensive programming */
/* USER CODE BEGIN (2) */
/* USER CODE END */
	}
	else
	{
		unsafe {
            /* Correct the corrupted location */
		    vimRAM.ISR[error_channel as usize] = s_vim_init[error_channel as usize];

		    /* Clear Parity Error Flag */
            VIM_PARFLG.write(1u32);
        }
		/* Disable and enable the highest priority pending channel */
		if vimREG.FIQINDEX.read() != 0u32
		{
			vec = vimREG.FIQINDEX.read() - 1u32;
		}
		else 
		{
		   /*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "Read 32 bit volatile register" */
			vec = vimREG.IRQINDEX.read() - 1u32;
		}
		if vec == 0u32
		{
            unsafe {
                vimREG.INTREQ0.write(1u32);
                vec = esmREG.ESMIOFFHR.read() - 1u32;
                
                if vec < 32u32
                {
                    esmREG.ESMSR1.write(1u32 << vec);
                    esmGroup1Notification(vec);
                }
                else if vec < 64u32
                {
                    esmREG.ESMSR2.write(1u32 << (vec-32u32));
                    esmGroup2Notification(vec-32u32);
                }
                else if vec < 96u32
                {
                    esmREG.ESMSR4.write(1u32 << (vec-64u32));
                    esmGroup1Notification(vec-32u32);
                }
                /* no mention of there being several ESMSR4 registers in the technical ref */
              //  else
              //  {
              //      esmREG.ESMSR4[1].write(1u32 << (vec-96u32));
              //      esmGroup2Notification(vec-64u32);
              //  }
            }
        
		}
		else if vec < 32u32
		{
			unsafe {
                vimREG.REQMASKCLR0.write(1u32 << vec);
			    vimREG.REQMASKSET0.write(1u32 << vec);
            }
		}
		else if vec < 64u32
		{
			unsafe {
                vimREG.REQMASKCLR1.write(1u32 << (vec-32u32));
			    vimREG.REQMASKSET1.write(1u32 << (vec-32u32));
            }		
        }
		else
		{
            unsafe {
                vimREG.REQMASKCLR2.write(1u32 << (vec-64u32));
                vimREG.REQMASKSET2.write(1u32 << (vec-64u32));
            }
		}
	}
}
