//! Low level access to Cortex-R processors
//!
//! # Uses nightly

#![no_std]
#![feature(asm)]

pub mod system;
pub mod registers;
pub mod peripherals;
pub mod bootloader;
