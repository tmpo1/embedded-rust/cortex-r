use crate::system::{system, sys_core::*, systest::*};
use crate::registers::{reg_flash::*, reg_esm::*, reg_system::*, reg_pbist::pbistAlgo};

pub fn init() {
    unsafe {
        _coreInitRegisters_();

        _coreInitStackPointer_();

        flashREG
            .FEDACTRL1
            .write(0xAu32);

        _coreEnableEventBusExport_();

        _coreEnableFlashEcc_();
    }

    let mut stc_done: bool = false;

    // TODO: handle any potential causes of reset
    if (SYSFRAME1.SYSESR.read() & (1u32 << 5)) == 1u32 {
        // a CPU reset was triggered
        stc_done = stcSelfCheckTest();
    }
    
    // Check if any ESM group3 error was indicated during power-up
    if esmREG.ESMSR3.read() != 0 {
        loop {}
    }
    
    system::sys_init();

    // step 20/21: not in the HalCoGen code.. so this is my assumption for how this is handled
    if !stc_done {
        unsafe { stcSelfCheck(); }
    }

    ccmSelfCheck();

    // step 23 has been changed a bit (runs march13 on ROM rather than a custom algorithm on
    // reserved memory
    unsafe { pbistSelfCheck(); }
    
    unsafe {
        pbistStopTest(); // just in case a test is currently running

        pbistRunTest(pbistAlgo::TripleReadSlow | pbistAlgo::TripleReadFast, 2u32);
    }

    loop { if pbistTestCompleted() { break; } }

    if !pbistTestPassed() {
        selfTestFailNotify(FailCode::PBIST(2));
    }

    unsafe {
        pbistStopTest();

        pbistRunTest(pbistAlgo::TripleReadSlow | pbistAlgo::TripleReadFast, 1u32);
    }

    loop { if pbistTestCompleted() { break; } }

    if !pbistTestPassed() {
        selfTestFailNotify(FailCode::PBIST(1));
    }

    unsafe {
        pbistStopTest();

        pbistRunTest(pbistAlgo::March13N_SP.value(), 0x20u32);
    }

    loop { if pbistTestCompleted() { break; } }

    if !pbistTestPassed() {
        selfTestFailNotify(FailCode::PBIST(0x20u32));
    }

    unsafe { pbistStopTest(); }

    unsafe { _coreEnableRamEcc_(); }

    memInit(1u32);

    unsafe {
        pbistRunTest(pbistAlgo::March13N_DP.value(), 0x364Cu32); // dual port
    }

    checkRAMECC();

    checkFlashECC();

    loop { if pbistTestCompleted() { break; } }

    if !pbistTestPassed() {
        selfTestFailNotify(FailCode::PBIST(0x364Cu32));
    }

    unsafe { pbistStopTest(); }

    // NOTE: need help figuring out how to not check the CPU RAM
    
    // TODO: step 33-34
}
