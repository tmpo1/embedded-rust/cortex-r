#![allow(unused_parens, unused_variables, unused_assignments, non_snake_case, unused_must_use)]

use crate::registers::reg_crc::*;
use crate::registers::sys_common::*;
use raw_pointer::Pointer;
use volatile_register::*;


/* @fn fn crcInit(fn)
*   @brief Initializes the crc Driver
*
*   This function initializes the crc module.
*/
/* SourceId : CRC_SourceId_001 */
/* DesignId : CRC_DesignId_001 */
/* Requirements: HL_SR107 */
pub unsafe fn crcInit()
{
    /* @b initialize @b CRC */

    /* - Reset  PSA*/
	crcREG.CTRL0.write((1u32 << 0u32)
                    | (1u32 << 8u32));

    /* - Pulling PSA out of reset */
	crcREG.CTRL0.write(0x00000000u32);
	
   /* - Setup the Data trace for channel1 */
	crcREG.CTRL2.write(crcREG.CTRL2.read() | (0u32 << 4u32));

   /* - Setup the Channel mode */
   crcREG.CTRL2.write(crcREG.CTRL2.read() | ((CRC_FULL_CPU)  | ((CRC_FULL_CPU) << 8u32)));
}


/* @fn fn crcSendPowerDown(crc: Pointer<crcBASE_t>)
*   @brief Send crc power down
*   @param[in] crc - crc module base address
*
*   Send crc power down signal to enter into sleep mode
*/
/* SourceId : CRC_SourceId_002 */
/* DesignId : CRC_DesignId_002 */
/* Requirements: HL_SR108 */
pub unsafe fn crcSendPowerDown(crc: Pointer<crcBASE_t>)
{
/* USER CODE BEGIN (4) */
/* USER CODE END */

    crc.CTRL1.write(crc.CTRL1.read() | 0x00000001u32);

/* USER CODE BEGIN (5) */
/* USER CODE END */
}

/* @fn fn crcSignGen(crc: Pointer<crcBASE_t>,param: &crcModConfig_t)
*   @brief set the mode specific parameters for signature generation
*   @param[in] crc - crc module base address
*   @param[in] param - structure holding mode specific parameters
*   Generate CRC signature. This device supports only FULL CPU mode of operation.
*/
/* SourceId : CRC_SourceId_003 */
/* DesignId : CRC_DesignId_003 */
/* Requirements: HL_SR109 */
pub fn crcSignGen(crc: Pointer<crcBASE_t>,param: &crcModConfig_t)
{
/* USER CODE BEGIN (6) */
/* USER CODE END */
    /* Author's Note: these pointer definitions are a bit tricky, could be incorrect from port */
    let mut ptr64: Pointer<RW<u64>> = Pointer::<RW<u64>> {
        ptr: param.src_data_pat.ptr as *mut RW<u64>
    };
	
	/*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
	/*SAFETYMCUSW 439 S MR:11.3 <APPROVED> "Pointer Manupulation required to find offset - Advisory as per MISRA" */
	
	
    // psaSigx = (uint32)(&crc->PSA_SIGREGL1) + ((uint32)(param->crc_channel) * 0x40U);
    
    /* Author's Note: We need format to get the pointer address */
    // let psaSigx: u32 = u32::from_str_radix(format!("{:p}", &crc.PSA_SIGREGL1), 16).unwrap() as u32 + ((param.crc_channel as u32) * 0x40u32);      
    let psaSigx: u32 = 0u32;
    
    // psaSigx_ptr64 = (uint64 *) (psaSigx);
    let psaSigx_ptr64: Pointer<RW<u64>> = Pointer::<RW<u64>> {
        ptr: psaSigx as *mut RW<u64>
    };
    

	if param.mode==CRC_FULL_CPU
    {
        for _ in 0..param.data_length
        {
		    /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
            unsafe {
                psaSigx_ptr64.write(ptr64.read());
                /*SAFETYMCUSW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
                /*SAFETYMCUSW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
                ptr64.ptr = ptr64.ptr.add(1);
            }		
        }
    }
    else
	{
        /* Empty */
    }
}


/* @fn fn crcChannelReset(crc: Pointer<crcBASE_t>,channel: u32)
*   @brief Reset the channel configurations
*   @param[in] crc - crc module base address
*   @param[in] channel-crc channel
*                      CRC_CH1 - channel1
*                      CRC_CH2 - channel2
*                      CRC_CH3 - channel3
*                      CRC_CH4 - channel4
*
*   Reset configurations of the selected channels.
*/
/* SourceId : CRC_SourceId_008 */
/* DesignId : CRC_DesignId_009 */
/* Requirements: HL_SR114 */
pub unsafe fn crcChannelReset(crc: Pointer<crcBASE_t>,channel: u32)
{

    if channel == 0u32
    {
        crc.CTRL0.write(crc.CTRL0.read() | (1u32 << 0u32)); /* Reset the CRC channel  */
        crc.CTRL0.write(crc.CTRL0.read() & !(1u32 << 0u32)); /* Exit the reset  */
     }
     else if channel == 1u32
     {
        crc.CTRL0.write(crc.CTRL0.read() | (1u32 << 8u32)); /* Reset the CRC channel  */
        crc.CTRL0.write(crc.CTRL0.read() & !(1u32 << 8u32)); /* Exit the reset  */
     }
     else
	 {
      /* Empty  */
	 }
 }
 
 
/* @fn uint32 crcGetPSASig(crc: Pointer<crcBASE_t>,channel: u32)
*   @brief get genearted PSA signature used for FULL CPU mode
*   @param[in] crc - crc module base address
*   @param[in] channel - crc channel
*                      CRC_CH1 - channel1
*                      CRC_CH2 - channel2
*                      CRC_CH3 - channel3
*                      CRC_CH4 - channel4
*
*   Get PSA signature used for FULL CPU mode of selected channel
*/
/* SourceId : CRC_SourceId_011 */
/* DesignId : CRC_DesignId_005 */
/* Requirements: HL_SR525 */
pub fn crcGetPSASig(crc: Pointer<crcBASE_t>,channel: u32) -> u64
{
    let mut status:u64 = 0u64;
	let CRC_PSA_SIGREGH1:u32 = crcREG.PSA_SIGREGH1.read();
	let CRC_PSA_SIGREGL1:u32 = crcREG.PSA_SIGREGL1.read();
	let CRC_PSA_SIGREGH2:u32 = crcREG.PSA_SIGREGH2.read();
	let CRC_PSA_SIGREGL2:u32 = crcREG.PSA_SIGREGL2.read();

    match channel
      {
    0 =>
        /*SAFETYMCUSW 51 S MR:12.3 <APPROVED> "Needs shifting for 64-bit value" */
		status = ((CRC_PSA_SIGREGL1 as u64) << 32u32) | (CRC_PSA_SIGREGH1 as u64),
    1 =>
        /*SAFETYMCUSW 51 S MR:12.3 <APPROVED> "Needs shifting for 64-bit value" */
		status = (((CRC_PSA_SIGREGL2 as u64) << 32u32) | (CRC_PSA_SIGREGH2 as u64)),
    _ => status = 0,
    }
    status
}


/* @fn fn crcGetConfigValue(crc_config_reg_t *config_reg, config_value_type_t type)
*   @brief Get the initial or current values of the CRC configuration registers
*
*	@param[in] *config_reg: pointer to the struct to which the initial or current 
*                           value of the configuration registers need to be stored
*	@param[in] type: 	whether initial or current value of the configuration registers need to be stored
*						- InitialValue: initial value of the configuration registers will be stored 
*                                       in the struct pointed by config_reg
*						- CurrentValue: initial value of the configuration registers will be stored 
*                                       in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type') 
*   of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : CRC_SourceId_012 */
/* DesignId : CRC_DesignId_012 */
/* Requirements: HL_SR119 */
pub fn crcGetConfigValue(config_reg: &mut crc_config_reg_t, _type: config_value_type_t)
{
	if let config_value_type_t::InitialValue = _type {
		config_reg.CONFIG_CTRL0        = CRC_CTRL0_CONFIGVALUE;
		config_reg.CONFIG_CTRL1        = CRC_CTRL1_CONFIGVALUE;
		config_reg.CONFIG_CTRL2        = CRC_CTRL2_CONFIGVALUE;
	}
	else
	{
	/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
		config_reg.CONFIG_CTRL0        = crcREG.CTRL0.read();
		config_reg.CONFIG_CTRL1        = crcREG.CTRL1.read();
		config_reg.CONFIG_CTRL2        = crcREG.CTRL2.read();
	}
}

