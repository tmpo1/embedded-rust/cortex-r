#![allow(unused_parens, dead_code, non_upper_case_globals, non_snake_case)]

use crate::registers::reg_het::*;
use crate::registers::sys_common::*;

/* Global variables                                                           */

const s_het1pwmPolarity: [u32; 8] = [3u32, 3u32, 3u32, 3u32, 3u32, 3u32, 3u32, 3u32];

/*----------------------------------------------------------------------------*/
/* Default Program                                                            */

/* @var static const hetINSTRUCTION_t het1PROGRAM[58]
*   @brief Default Program
*
*   Het program running after initialization.
*/

const het1PROGRAM: [hetINSTRUCTION_t; 58] =
[
    /* CNT: Timebase
    *       - Instruction                  = 0
    *       - Next instruction             = 1
    *       - Conditional next instruction = na
    *       - Interrupt                    = na
    *       - Pin                          = na
    *       - Reg                          = T
    */
    hetINSTRUCTION_t {
        Program: 
        0x00002C80u32,
        Control: 
        0x01FFFFFFu32,
        Data: 
        0xFFFFFF80u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 0 -> Duty Cycle
    *         - Instruction                  = 1
    *         - Next instruction             = 2
    *         - Conditional next instruction = 2
    *         - Interrupt                    = 1
    *         - Pin                          = 8
    */
    hetINSTRUCTION_t {
        Program: 
        0x000055C0u32,
        Control: 
        (0x00004006u32 | (8u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 0 -> Period
    *         - Instruction                  = 2
    *         - Next instruction             = 3
    *         - Conditional next instruction = 41
    *         - Interrupt                    = 2
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00007480u32,
        Control: 
        0x00052006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 1 -> Duty Cycle
    *         - Instruction                  = 3
    *         - Next instruction             = 4
    *         - Conditional next instruction = 4
    *         - Interrupt                    = 3
    *         - Pin                          = 10
    */
    hetINSTRUCTION_t {
        Program: 
        0x000095C0u32,
        Control: 
        (0x00008006u32 | (10u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 1 -> Period
    *         - Instruction                  = 4
    *         - Next instruction             = 5
    *         - Conditional next instruction = 43
    *         - Interrupt                    = 4
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0000B480u32,
        Control: 
        0x00056006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 2 -> Duty Cycle
    *         - Instruction                  = 5
    *         - Next instruction             = 6
    *         - Conditional next instruction = 6
    *         - Interrupt                    = 5
    *         - Pin                          = 12
    */
    hetINSTRUCTION_t {
        Program: 
        0x0000D5C0u32,
        Control: 
        (0x0000C006u32 | (12u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 2 -> Period
    *         - Instruction                  = 6
    *         - Next instruction             = 7
    *         - Conditional next instruction = 45
    *         - Interrupt                    = 6
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0000f64480u32,
        Control: 
        0x0005A006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 3 -> Duty Cycle
    *         - Instruction                  = 7
    *         - Next instruction             = 8
    *         - Conditional next instruction = 8
    *         - Interrupt                    = 7
    *         - Pin                          = 14
    */
    hetINSTRUCTION_t {
        Program: 
        0x000115C0u32,
        Control: 
        (0x00010006u32 | (14u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 3 -> Period
    *         - Instruction                  = 8
    *         - Next instruction             = 9
    *         - Conditional next instruction = 47
    *         - Interrupt                    = 8
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00013480u32,
        Control: 
        0x0005E006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 4 -> Duty Cycle
    *         - Instruction                  = 9
    *         - Next instruction             = 10
    *         - Conditional next instruction = 10
    *         - Interrupt                    = 9
    *         - Pin                          = 16
    */
    hetINSTRUCTION_t {
        Program: 
        0x000155C0u32,
        Control: 
        (0x00014006u32 | (16u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 4 -> Period
    *         - Instruction                  = 10
    *         - Next instruction             = 11
    *         - Conditional next instruction = 49
    *         - Interrupt                    = 10
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00017480u32,
        Control: 
        0x00062006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 5 -> Duty Cycle
    *         - Instruction                  = 11
    *         - Next instruction             = 12
    *         - Conditional next instruction = 12
    *         - Interrupt                    = 11
    *         - Pin                          = 17
    */
    hetINSTRUCTION_t {
        Program: 
        0x000195C0u32,
        Control: 
        (0x00018006u32 | (17u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 5 -> Period
    *         - Instruction                  = 12
    *         - Next instruction             = 13
    *         - Conditional next instruction = 51
    *         - Interrupt                    = 12
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0001B480u32,
        Control: 
        0x00066006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 6 -> Duty Cycle
    *         - Instruction                  = 13
    *         - Next instruction             = 14
    *         - Conditional next instruction = 14
    *         - Interrupt                    = 13
    *         - Pin                          = 18
    */
    hetINSTRUCTION_t {
        Program: 
        0x0001D5C0u32,
        Control: 
        (0x0001C006u32 | (18u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 6 -> Period
    *         - Instruction                  = 14
    *         - Next instruction             = 15
    *         - Conditional next instruction = 53
    *         - Interrupt                    = 14
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0001f64480u32,
        Control: 
        0x0006A006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PWCNT: PWM 7 -> Duty Cycle
    *         - Instruction                  = 15
    *         - Next instruction             = 16
    *         - Conditional next instruction = 16
    *         - Interrupt                    = 15
    *         - Pin                          = 19
    */
    hetINSTRUCTION_t {
        Program: 
        0x000215C0u32,
        Control: 
        (0x00020006u32 | (19u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* DJZ: PWM 7 -> Period
    *         - Instruction                  = 16
    *         - Next instruction             = 17
    *         - Conditional next instruction = 55
    *         - Interrupt                    = 16
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00023480u32,
        Control: 
        0x0006E006u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 0
    *         - Instruction                  = 17
    *         - Next instruction             = 18
    *         - Conditional next instruction = 18
    *         - Interrupt                    = 17
    *         - Pin                          = 9
    */
    hetINSTRUCTION_t {
        Program: 
        0x00025440u32,
        Control: 
        (0x00024007u32 | (9u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 1
    *         - Instruction                  = 18
    *         - Next instruction             = 19
    *         - Conditional next instruction = 19
    *         - Interrupt                    = 18
    *         - Pin                          = 11
    */
    hetINSTRUCTION_t {
        Program: 
        0x00027440u32,
        Control: 
        (0x00026007u32 | (11u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 2
    *         - Instruction                  = 19
    *         - Next instruction             = 20
    *         - Conditional next instruction = 20
    *         - Interrupt                    = 19
    *         - Pin                          = 13
    */
    hetINSTRUCTION_t {
        Program: 
        0x00029440u32,
        Control: 
        (0x00028007u32 | (13u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 3
    *         - Instruction                  = 20
    *         - Next instruction             = 21
    *         - Conditional next instruction = 21
    *         - Interrupt                    = 20
    *         - Pin                          = 15
    */
    hetINSTRUCTION_t {
        Program: 
        0x0002B440u32,
        Control: 
        (0x0002A007u32 | (15u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 4
    *         - Instruction                  = 21
    *         - Next instruction             = 22
    *         - Conditional next instruction = 22
    *         - Interrupt                    = 21
    *         - Pin                          = 20
    */
    hetINSTRUCTION_t {
        Program: 
        0x0002D440u32,
        Control: 
        (0x0002C007u32 | (20u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 5
    *         - Instruction                  = 22
    *         - Next instruction             = 23
    *         - Conditional next instruction = 23
    *         - Interrupt                    = 22
    *         - Pin                          = 21
    */
    hetINSTRUCTION_t {
        Program: 
        0x0002f64440u32,
        Control: 
        (0x0002E007u32 | (21u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 6
    *         - Instruction                  = 23
    *         - Next instruction             = 24
    *         - Conditional next instruction = 24
    *         - Interrupt                    = 23
    *         - Pin                          = 22
    */
    hetINSTRUCTION_t {
        Program: 
        0x00031440u32,
        Control: 
        (0x00030007u32 | (22u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* ECNT: CCU Edge 7
    *         - Instruction                  = 24
    *         - Next instruction             = 25
    *         - Conditional next instruction = 25
    *         - Interrupt                    = 24
    *         - Pin                          = 23
    */
    hetINSTRUCTION_t {
        Program: 
        0x00033440u32,
        Control: 
        (0x00032007u32 | (23u32 << 8u32) as u32 | (1u32 << 4u32) as u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 0
    *         - Instruction                  = 25
    *         - Next instruction             = 26
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 0
    */
    hetINSTRUCTION_t {
        Program: 
        0x00034E00u32 | (0u32 << 6u32) as u32  | (0u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 0
    *         - Instruction                  = 26
    *         - Next instruction             = 27
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 0  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x00036E80u32 | (0u32 << 6u32) as u32  | ((0u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 1
    *         - Instruction                  = 27
    *         - Next instruction             = 28
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 2
    */
    hetINSTRUCTION_t {
        Program: 
        0x00038E00u32 | (0u32 << 6u32) as u32  | (2u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 1
    *         - Instruction                  = 28
    *         - Next instruction             = 29
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 2  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x0003AE80u32 | (0u32 << 6u32) as u32  | ((2u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 2
    *         - Instruction                  = 29
    *         - Next instruction             = 30
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 4
    */
    hetINSTRUCTION_t {
        Program: 
        0x0003CE00u32 | (0u32 << 6u32) as u32  | (4u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 2
    *         - Instruction                  = 30
    *         - Next instruction             = 31
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 4  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x0003EE80u32 | (0u32 << 6u32) as u32  | ((4u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 3
    *         - Instruction                  = 31
    *         - Next instruction             = 32
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 6
    */
    hetINSTRUCTION_t {
        Program: 
        0x00040E00u32 | (0u32 << 6u32) as u32  | (6u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 3
    *         - Instruction                  = 32
    *         - Next instruction             = 33
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 6  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x00042E80u32 | (0u32 << 6u32) as u32  | ((6u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 4
    *         - Instruction                  = 33
    *         - Next instruction             = 34
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 24
    */
    hetINSTRUCTION_t {
        Program: 
        0x00044E00u32 | (0u32 << 6u32) as u32  | (24u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 4
    *         - Instruction                  = 34
    *         - Next instruction             = 35
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 24  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x00046E80u32 | (0u32 << 6u32) as u32  | ((24u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 5
    *         - Instruction                  = 35
    *         - Next instruction             = 36
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 26
    */
    hetINSTRUCTION_t {
        Program: 
        0x00048E00u32 | (0u32 << 6u32) as u32  | (26u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 5
    *         - Instruction                  = 36
    *         - Next instruction             = 37
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 26  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x0004AE80u32 | (0u32 << 6u32) as u32  | ((26u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 6
    *         - Instruction                  = 37
    *         - Next instruction             = 38
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 28
    */
    hetINSTRUCTION_t {
        Program: 
        0x0004CE00u32 | (0u32 << 6u32) as u32  | (28u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 6
    *         - Instruction                  = 38
    *         - Next instruction             = 39
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 28  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x0004EE80u32 | (0u32 << 6u32) as u32  | ((28u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Duty 7
    *         - Instruction                  = 39
    *         - Next instruction             = 40
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 30
    */
    hetINSTRUCTION_t {
        Program: 
        0x00050E00u32 | (0u32 << 6u32) as u32  | (30u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* PCNT: Capture Period 7
    *         - Instruction                  = 40
    *         - Next instruction             = 57
    *         - Conditional next instruction = na
    *         - Interrupt                    = na
    *         - Pin                          = 30  + 1
    */
    hetINSTRUCTION_t {
        Program: 
        0x00072E80u32 | (0u32 << 6u32) as u32  | ((30u32) + 1u32) as u32,
        Control: 
        0x00000000u32,
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 0 -> Duty Cycle Update
    *         - Instruction                  = 41
    *         - Next instruction             = 42
    *         - Conditional next instruction = 2
    *         - Interrupt                    = 1
    *         - Pin                          = 8
    */
    hetINSTRUCTION_t {
        Program: 
        0x00054201u32,
        Control: 
        (0x00004007u32 | (0u32 << 22u32) as u32 | (8u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 0 -> Period Update
    *         - Instruction                  = 42
    *         - Next instruction             = 3
    *         - Conditional next instruction = 41
    *         - Interrupt                    = 2
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00006202u32,
        Control: 
        (0x00052007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 1 -> Duty Cycle Update
    *         - Instruction                  = 43
    *         - Next instruction             = 44
    *         - Conditional next instruction = 4
    *         - Interrupt                    = 3
    *         - Pin                          = 10
    */
    hetINSTRUCTION_t {
        Program: 
        0x00058203u32,
        Control: 
        (0x00008007u32 | (0u32 << 22u32) as u32 | (10u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 1 -> Period Update
    *         - Instruction                  = 44
    *         - Next instruction             = 5
    *         - Conditional next instruction = 43
    *         - Interrupt                    = 4
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0000A204u32,
        Control: 
        (0x00056007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 2 -> Duty Cycle Update
    *         - Instruction                  = 45
    *         - Next instruction             = 46
    *         - Conditional next instruction = 6
    *         - Interrupt                    = 5
    *         - Pin                          = 12
    */
    hetINSTRUCTION_t {
        Program: 
        0x0005C205u32,
        Control: 
        (0x0000C007u32 | (0u32 << 22u32) as u32 | (12u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 2 -> Period Update
    *         - Instruction                  = 46
    *         - Next instruction             = 7
    *         - Conditional next instruction = 45
    *         - Interrupt                    = 6
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0000E206u32,
        Control: 
        (0x0005A007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 3 -> Duty Cycle Update
    *         - Instruction                  = 47
    *         - Next instruction             = 48
    *         - Conditional next instruction = 8
    *         - Interrupt                    = 7
    *         - Pin                          = 14
    */
    hetINSTRUCTION_t {
        Program: 
        0x00060207u32,
        Control: 
        (0x00010007u32 | (0u32 << 22u32) as u32 | (14u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 3 -> Period Update
    *         - Instruction                  = 48
    *         - Next instruction             = 9
    *         - Conditional next instruction = 47
    *         - Interrupt                    = 8
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00012208u32,
        Control: 
        (0x0005E007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 4 -> Duty Cycle Update
    *         - Instruction                  = 49
    *         - Next instruction             = 50
    *         - Conditional next instruction = 10
    *         - Interrupt                    = 9
    *         - Pin                          = 16
    */
    hetINSTRUCTION_t {
        Program: 
        0x00064209u32,
        Control: 
        (0x00014007u32 | (0u32 << 22u32) as u32 | (16u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 4 -> Period Update
    *         - Instruction                  = 50
    *         - Next instruction             = 11
    *         - Conditional next instruction = 49
    *         - Interrupt                    = 10
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0001620Au32,
        Control: 
        (0x00062007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 5 -> Duty Cycle Update
    *         - Instruction                  = 51
    *         - Next instruction             = 52
    *         - Conditional next instruction = 12
    *         - Interrupt                    = 11
    *         - Pin                          = 17
    */
    hetINSTRUCTION_t {
        Program: 
        0x0006820Bu32,
        Control: 
        (0x00018007u32 | (0u32 << 22u32) as u32 | (17u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 5 -> Period Update
    *         - Instruction                  = 52
    *         - Next instruction             = 13
    *         - Conditional next instruction = 51
    *         - Interrupt                    = 12
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0001A20Cu32,
        Control: 
        (0x00066007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 6 -> Duty Cycle Update
    *         - Instruction                  = 53
    *         - Next instruction             = 54
    *         - Conditional next instruction = 14
    *         - Interrupt                    = 13
    *         - Pin                          = 18
    */
    hetINSTRUCTION_t {
        Program: 
        0x0006C20Du32,
        Control: 
        (0x0001C007u32 | (0u32 << 22u32) as u32 | (18u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 6 -> Period Update
    *         - Instruction                  = 54
    *         - Next instruction             = 15
    *         - Conditional next instruction = 53
    *         - Interrupt                    = 14
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x0001E20Eu32,
        Control: 
        (0x0006A007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 7 -> Duty Cycle Update
    *         - Instruction                  = 55
    *         - Next instruction             = 56
    *         - Conditional next instruction = 16
    *         - Interrupt                    = 15
    *         - Pin                          = 19
    */
    hetINSTRUCTION_t {
        Program: 
        0x0007020f64u32,
        Control: 
        (0x00020007u32 | (0u32 << 22u32) as u32 | (19u32 << 8u32) as u32 | (3u32 << 3u32) as u32),
        Data: 
        100224u32,
        rsvd1: 
        0x00000000u32
    },
    /* MOV64: PWM 7 -> Period Update
    *         - Instruction                  = 56
    *         - Next instruction             = 17
    *         - Conditional next instruction = 55
    *         - Interrupt                    = 16
    *         - Pin                          = na
    */
    hetINSTRUCTION_t {
        Program: 
        0x00022210u32,
        Control: 
        (0x0006E007u32),
        Data: 
        199936u32,
        rsvd1: 
        0x00000000u32
    },
    /* WCAP: Capture timestamp
    *         - Instruction                  = 57
    *         - Next instruction             = 0
    *         - Conditional next instruction = 0
    *         - Interrupt                    = na
    *         - Pin                          = na
    *         - Reg                          = T
    */
    hetINSTRUCTION_t {
        Program: 
        0x00001600u32,
        Control: 
        (0x00000004u32),
        Data: 
        0x00000000u32,
        rsvd1: 
        0x00000000u32
    },
];



/* @fn fn hetInit(void)
*   @brief Initializes the het Driver
*
*   This function initializes the het 1 module.
*/
/* SourceId : HET_SourceId_001 */
/* DesignId : HET_DesignId_001 */
/* Requirements : HL_SR363 */
pub unsafe fn  hetInit()
{
    /* @b initialize @b HET */

    /* - Set HET pins default output value */
    hetREG1.DOUT.write(0u32);

    /* - Set HET pins direction */
    hetREG1.DIR.write(0u32);

    /* - Set HET pins open drain enable */
    hetREG1.PDR.write(0u32);

    /* - Set HET pins pullup/down enable */
    hetREG1.PULDIS.write(0u32);

    /* - Set HET pins pullup/down select */
    hetREG1.PSL.write(0u32);

    /* - Set HET pins high resolution share */
    hetREG1.HRSH.write(0x00008000u32
                  |  0x00004000u32
                  |  0x00002000u32
                  |  0x00001000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000000u32
                  |  0x00000008u32
                  |  0x00000004u32
                  |  0x00000002u32
                  |  0x00000001u32);

    /* - Set HET pins AND share */
    hetREG1.AND.write(0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32);

    /* - Set HET pins XOR share */
    hetREG1.XOR.write(0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32);

/* USER CODE BEGIN (1) */
/* USER CODE END */

    /* - Setup prescaler values
    *     - Loop resolution prescaler
    *     - High resolution prescaler
    */
    hetREG1.PFR.write(( 6u32 << 8u32) as u32
                | ( 0u32));


   /* - Parity control register
    *     - Enable/Disable Parity check
    */
    hetREG1.PCR.write(0x00000005u32);

    /* - Fill HET RAM with opcodes and Data */
    /*SAFETYMCUSW 94 S MR:11.1,11.2,11.4 <APPROVED> "HET RAM Fill from the table - Allowed as per MISRA rule 11.2" */
    /*SAFETYMCUSW 94 S MR:11.1,11.2,11.4 <APPROVED> "HET RAM Fill from the table - Allowed as per MISRA rule 11.2" */
    /*SAFETYMCUSW 95 S MR:11.1,11.4 <APPROVED> "HET RAM Fill from the table - Allowed as per MISRA rule 11.2" */
    /*SAFETYMCUSW 95 S MR:11.1,11.4 <APPROVED> "HET RAM Fill from the table - Allowed as per MISRA rule 11.2" */
    
    // (void)memcpy((fn *)hetRAM1, (const void *)het1PROGRAM, sizeof(het1PROGRAM));
    // copy_nonoverlapping(hetRAM1 as *const hetRAMBASE_t, het1PROGRAM as *mut hetINSTRUCTION_t,  std::mem::size_of(het1PROGRAM));

    /* - Setup interrupt priority level
    *     - PWM 0 end of duty  level
    *     - PWM 0 end of period level
    *     - PWM 1 end of duty  level
    *     - PWM 1 end of period level
    *     - PWM 2 end of duty  level
    *     - PWM 2 end of period level
    *     - PWM 3 end of duty  level
    *     - PWM 3 end of period level
    *     - PWM 4 end of duty  level
    *     - PWM 4 end of period level
    *     - PWM 5 end of duty  level
    *     - PWM 5 end of period level
    *     - PWM 6 end of duty  level
    *     - PWM 6 end of period level
    *     - PWM 7 end of duty  level
    *     - PWM 7 end of period level

    *     - CCU Edge Detection 0 level
    *     - CCU Edge Detection 1 level
    *     - CCU Edge Detection 2 level
    *     - CCU Edge Detection 3 level
    *     - CCU Edge Detection 4 level
    *     - CCU Edge Detection 5 level
    *     - CCU Edge Detection 6 level
    *     - CCU Edge Detection 7 level
    */
    hetREG1.PRY.write(0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32
                 |  0x00000000u32);

    /* - Enable interrupts
    *     - PWM 0 end of duty
    *     - PWM 0 end of period
    *     - PWM 1 end of duty
    *     - PWM 1 end of period
    *     - PWM 2 end of duty
    *     - PWM 2 end of period
    *     - PWM 3 end of duty
    *     - PWM 3 end of period
    *     - PWM 4 end of duty
    *     - PWM 4 end of period
    *     - PWM 5 end of duty
    *     - PWM 5 end of period
    *     - PWM 6 end of duty
    *     - PWM 6 end of period
    *     - PWM 7 end of duty
    *     - PWM 7 end of period
    *     - CCU Edge Detection 0
    *     - CCU Edge Detection 1
    *     - CCU Edge Detection 2
    *     - CCU Edge Detection 3
    *     - CCU Edge Detection 4
    *     - CCU Edge Detection 5
    *     - CCU Edge Detection 6
    *     - CCU Edge Detection 7
    */
    hetREG1.INTENAC.write(0xFFFFFFFFu32);
    hetREG1.INTENAS.write(0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32
                     |  0x00000000u32);


   /* - Setup control register
    *     - Enable output buffers
    *     - Ignore software breakpoints
    *     - Master or Slave Clock Mode
    *     - Enable HET
    */
    hetREG1.GCR.write( 0x00000001u32 
                   | (0u32 << 24u32) as u32
	               | (1u32 << 16u32) as u32
                   | (0x00020000u32));


}



/* MARKER */

/* @fn fn pwmStart( hetRAM: *mut hetRAMBASE_t, pwm: u32)
*   @brief Start pwm signal
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*
*   Start the given pwm signal
*/
/* SourceId : HET_SourceId_002 */
/* DesignId : HET_DesignId_002 */
/* Requirements : HL_SR364 */
pub unsafe fn pwmStart(hetRAM: *mut hetRAMBASE_t,  pwm: u32)
{

   (*hetRAM).Instruction[((pwm << 1) + 41) as usize].Control  |= 0x00400000u32; //usize cast might not be good
}


/* @fn fn pwmStop( hetRAM: *mut hetRAMBASE_t, pwm: u32)
*   @brief Stop pwm signal
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*
*   Stop the given pwm signal
*/
/* SourceId : HET_SourceId_003 */
/* DesignId : HET_DesignId_003 */
/* Requirements : HL_SR365 */
pub unsafe fn pwmStop(hetRAM: *mut hetRAMBASE_t, pwm: u32)
{
    (*hetRAM).Instruction[((pwm << 1) + 41) as usize].Control  &= !0x00400000u32; //usize cast might not be good
}


/* @fn fn pwmSetDuty(hetRAM: *mut hetRAMBASE_t, pwm: u32, pwm: u32Duty)
*   @brief Set duty cycle
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*   @param[in] pwmDuty duty cycle in %.
*
*   Sets a new duty cycle on the given pwm signal
*/
/* SourceId : HET_SourceId_004 */
/* DesignId : HET_DesignId_004 */
/* Requirements : HL_SR366 */
pub unsafe fn pwmSetDuty(hetRAM: *mut hetRAMBASE_t, pwm: u32,  pwmDuty: u32)
{
    let action: u32;
    let mut pwmPolarity = 0u32;
    let mut pwmPeriod: u32 = (*hetRAM).Instruction[((pwm << 1) + 42) as usize].Data + 128u32; //usize cast might not be good
    pwmPeriod = pwmPeriod >> 7u32;

    if(hetRAM == hetRAM1.ptr)
    {
        pwmPolarity = s_het1pwmPolarity[pwm as usize];
    }
    else
    {
    }
    if (pwmDuty == 0u32)
    {
        action = if pwmPolarity == 3u32  {0u32} else {2u32};
    }
    else if (pwmDuty >= 100u32)
    {
        action = if pwmPolarity == 3u32  {2u32} else {0u32};
    }
    else
    {
        action = pwmPolarity;
    }

    (*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Control = (((*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Control) & (!(0x00000018u32))) | (action << 3u32);
    (*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Data = (((pwmPeriod * pwmDuty) / 100u32) << 7u32) + 128u32;
}


/* @fn fn pwmSetSignal(hetRAM: *mut hetRAMBASE_t, pwm: u32, signal: hetSIGNAL_t)
*   @brief Set period
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*   @param[in] signal signal
               - duty cycle in %.
*              - period period in us.
*
*   Sets a new pwm signal
*/
/* SourceId : HET_SourceId_005 */
/* DesignId : HET_DesignId_005 */
/* Requirements : HL_SR367 */
pub unsafe fn pwmSetSignal(hetRAM: *mut hetRAMBASE_t, pwm: u32, signal: hetSIGNAL_t)
{
    let action: u32;
    let mut pwmPolarity = 0u32;
    let mut pwmPeriod = 0.0f64;

    if(hetRAM == hetRAM1.ptr)
    {
        pwmPeriod = (signal.period * 1000.0f64) / 640.000f64;
        pwmPolarity = s_het1pwmPolarity[pwm as usize];
    }
    else
    {
    }
    if (signal.duty == 0u32)
    {
        action = if (pwmPolarity == 3u32)  {0u32} else {2u32};
    }
    else if (signal.duty >= 100u32)
    {
        action = if (pwmPolarity == 3u32)  {2u32} else  {0u32};
    }
    else
    {
        action = pwmPolarity;
    }

    (*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Control = (((*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Control) & (!(0x00000018u32))) | (action << 3u32);
    (*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Data = (((pwmPeriod as u32 * signal.duty) / 100u32) << 7u32 ) + 128u32;
    (*hetRAM).Instruction[((pwm << 1u32) + 42u32) as usize].Data = ((pwmPeriod as u32) << 7u32) - 128u32;

}


/* @fn fn pwmGetSignal(hetRAM: *mut hetRAMBASE_t, pwm: u32, signal: hetSIGNAL_t)
*   @brief Get duty cycle
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*   @param[in] signal signal
*              - duty cycle in %.
*              - period period in us.
*
*   Gets current signal of the given pwm signal.
*/
/* SourceId : HET_SourceId_006 */
/* DesignId : HET_DesignId_006 */
/* Requirements : HL_SR368 */
pub unsafe fn pwmGetSignal(hetRAM: *mut hetRAMBASE_t, pwm: u32, signal: *mut hetSIGNAL_t)
{
        let pwmDuty   : u32 = ((*hetRAM).Instruction[((pwm << 1u32) + 41u32) as usize].Data - 128u32) >> 7u32;
        let pwmPeriod : u32 = ((*hetRAM).Instruction[((pwm << 1u32) + 42u32) as usize].Data + 128u32) >> 7u32;

    (*signal).duty   = (pwmDuty * 100u32) / pwmPeriod;

    if(hetRAM == hetRAM1.ptr)
    {
        (*signal).period = ((pwmPeriod as f64) * 640.000f64) / 1000.0f64;
    }
    else
    {
        (*signal).period = ((pwmPeriod as f64) * 800.000f64) / 1000.0f64;
    }
}

/* @fn fn pwmEnableNotification(hetREG: *mut hetBASE_t, pwm: u32, notification: u32)
*   @brief Enable pwm notification
*   @param[in] hetREG Pointer to HET Module:
*              - hetREG1: HET1 Module pointer
*              - hetREG2: HET2 Module pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*   @param[in] notification Pwm notification:
*              - pwmEND_OF_DUTY:   Notification on end of duty
*              - pwmEND_OF_PERIOD: Notification on end of end period
*              - pwmEND_OF_BOTH:   Notification on end of both duty and period
*/
/* SourceId : HET_SourceId_007 */
/* DesignId : HET_DesignId_007 */
/* Requirements : HL_SR369 */
pub unsafe fn pwmEnableNotification(hetREG: *mut hetBASE_t, pwm: u32, notification: u32)
{
    (*hetREG).FLG.write(notification << (pwm << 1u32));
    (*hetREG).INTENAS.write(notification << (pwm << 1u32));
}


/* @fn fn pwmDisableNotification(hetREG: *mut hetBASE_t, pwm: u32, notification: u32)
*   @brief Enable pwm notification
*   @param[in] hetREG Pointer to HET Module:
*              - hetREG1: HET1 Module pointer
*              - hetREG2: HET2 Module pointer
*   @param[in] pwm Pwm signal:
*              - pwm0: Pwm 0
*              - pwm1: Pwm 1
*              - pwm2: Pwm 2
*              - pwm3: Pwm 3
*              - pwm4: Pwm 4
*              - pwm5: Pwm 5
*              - pwm6: Pwm 6
*              - pwm7: Pwm 7
*   @param[in] notification Pwm notification:
*              - pwmEND_OF_DUTY:   Notification on end of duty
*              - pwmEND_OF_PERIOD: Notification on end of end period
*              - pwmEND_OF_BOTH:   Notification on end of both duty and period
*/
/* SourceId : HET_SourceId_008 */
/* DesignId : HET_DesignId_008 */
/* Requirements : HL_SR370 */
pub unsafe fn pwmDisableNotification(hetREG: *mut hetBASE_t, pwm: u32, notification: u32)
{
    (*hetREG).INTENAC.write(notification << (pwm << 1u32));
}


/* @fn fn edgeResetCounter(hetRAM: *mut hetRAMBASE_t, edge: u32)
*   @brief Resets edge counter to 0
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] edge Edge signal:
*              - edge0: Edge 0
*              - edge1: Edge 1
*              - edge2: Edge 2
*              - edge3: Edge 3
*              - edge4: Edge 4
*              - edge5: Edge 5
*              - edge6: Edge 6
*              - edge7: Edge 7
*
*   Reset edge counter to 0.
*/
/* SourceId : HET_SourceId_009 */
/* DesignId : HET_DesignId_009 */
/* Requirements : HL_SR372 */
pub unsafe fn edgeResetCounter(hetRAM: *mut hetRAMBASE_t, edge: u32)
{
    (*hetRAM).Instruction[(edge + 17u32) as usize].Data = 0u32;
}


/* @fn edge: u32GetCounter(hetRAM: *mut hetRAMBASE_t, edge: u32)
*   @brief Get current edge counter value
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] edge Edge signal:
*              - edge0: Edge 0
*              - edge1: Edge 1
*              - edge2: Edge 2
*              - edge3: Edge 3
*              - edge4: Edge 4
*              - edge5: Edge 5
*              - edge6: Edge 6
*              - edge7: Edge 7
*
*   Gets current edge counter value.
*/
/* SourceId : HET_SourceId_010 */
/* DesignId : HET_DesignId_010 */
/* Requirements : HL_SR373 */
pub unsafe fn edgeGetCounter(hetRAM: *mut hetRAMBASE_t, edge: u32) -> u32
{
    (*hetRAM).Instruction[(edge + 17u32) as usize].Data >> 7u32
}


/* @fn fn edgeEnableNotification(hetREG: *mut hetBASE_t, edge: u32)
*   @brief Enable edge notification
*   @param[in] hetREG Pointer to HET Module:
*              - hetREG1: HET1 Module pointer
*              - hetREG2: HET2 Module pointer
*   @param[in] edge Edge signal:
*              - edge0: Edge 0
*              - edge1: Edge 1
*              - edge2: Edge 2
*              - edge3: Edge 3
*              - edge4: Edge 4
*              - edge5: Edge 5
*              - edge6: Edge 6
*              - edge7: Edge 7
*/
/* SourceId : HET_SourceId_011 */
/* DesignId : HET_DesignId_011 */
/* Requirements : HL_SR374 */
pub unsafe fn edgeEnableNotification(hetREG: *mut hetBASE_t, edge: u32)
{
    (*hetREG).FLG.write(0x20000u32 << edge);
    (*hetREG).INTENAS.write(0x20000u32 << edge);
}


/* @fn fn edgeDisableNotification(hetREG: *mut hetBASE_t, edge: u32)
*   @brief Enable edge notification
*   @param[in] hetREG Pointer to HET Module:
*              - hetREG1: HET1 Module pointer
*              - hetREG2: HET2 Module pointer
*   @param[in] edge Edge signal:
*              - edge0: Edge 0
*              - edge1: Edge 1
*              - edge2: Edge 2
*              - edge3: Edge 3
*              - edge4: Edge 4
*              - edge5: Edge 5
*              - edge6: Edge 6
*              - edge7: Edge 7
*/
/* SourceId : HET_SourceId_012 */
/* DesignId : HET_DesignId_012 */
/* Requirements : HL_SR375 */
pub unsafe fn edgeDisableNotification(hetREG: *mut hetBASE_t, edge: u32)
{
    (*hetREG).INTENAC.write(0x20000u32 << edge);
}


/* @fn fn capGetSignal(hetRAM: *mut hetRAMBASE_t, cap: u32, signal: hetSIGNAL_t)
*   @brief Get capture signal
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*   @param[in] cap captured signal:
*              - cap0: Captured signal 0
*              - cap1: Captured signal 1
*              - cap2: Captured signal 2
*              - cap3: Captured signal 3
*              - cap4: Captured signal 4
*              - cap5: Captured signal 5
*              - cap6: Captured signal 6
*              - cap7: Captured signal 7
*   @param[in] signal signal
*              - duty cycle in %.
*              - period period in us.
*
*   Gets current signal of the given capture signal.
*/
/* SourceId : HET_SourceId_013 */
/* DesignId : HET_DesignId_013 */
/* Requirements : HL_SR377 */
pub unsafe fn capGetSignal(hetRAM: *mut hetRAMBASE_t, cap: u32, signal: *mut hetSIGNAL_t)
{
        let pwmDuty   : u32 =  ((*hetRAM).Instruction[((cap << 1u32) + 25u32) as usize].Data) >> 7u32;
        let pwmPeriod : u32 =  ((*hetRAM).Instruction[((cap << 1u32) + 26u32) as usize].Data) >> 7u32;

    (*signal).duty   = (pwmDuty * 100u32) / pwmPeriod;

    if( hetRAM == hetRAM1.ptr)
    {
        (*signal).period = ((pwmPeriod as f64) * 640.000f64) / 1000.0f64;
    }
    else
    {
        (*signal).period = ((pwmPeriod as f64) * 800.000f64) / 1000.0f64;
    }
}


/* @fn fn hetResetTimestamp(hetRAMBASE_t *hetRAM)
*   @brief Resets timestamp
*   @param[in] hetRAM Pointer to HET RAM:
*              - hetRAM1: HET1 RAM pointer
*              - hetRAM2: HET2 RAM pointer
*
*   Resets loop count based timestamp.
*/
/* SourceId : HET_SourceId_014 */
/* DesignId : HET_DesignId_014 */
/* Requirements : HL_SR378 */
pub unsafe fn hetResetTimestamp(hetRAM: *mut hetRAMBASE_t)
{
    (*hetRAM).Instruction[0].Data = 0u32;
}


/* @fn uint32 hetGetTimestamp(hetRAMBASE_t *hetRAM)
*   @brief Returns timestamp
*
*   Returns loop count based timestamp.
*/
/* SourceId : HET_SourceId_015 */
/* DesignId : HET_DesignId_015 */
/* Requirements : HL_SR379 */
pub unsafe fn hetGetTimestamp(hetRAM: *mut hetRAMBASE_t) -> u32
{
    (*hetRAM).Instruction[57].Data
}

/* USER CODE BEGIN (4) */
/* USER CODE END */


/* @fn fn het1GetConfigValue(het_config_reg_t *config_reg, config_value_type_t type)
*   @brief Get the initial or current values of the HET1 configuration registers
*
*   @param[in] *config_reg: pointer to the struct to which the initial or current
*                           value of the configuration registers need to be stored
*   @param[in] type:    whether initial or current value of the configuration registers need to be stored
*                       - InitialValue: initial value of the configuration registers will be stored
*                                       in the struct pointed by config_reg
*                       - CurrentValue: initial value of the configuration registers will be stored
*                                       in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type')
*   of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : HET_SourceId_016 */
/* DesignId : HET_DesignId_016 */
/* Requirements : HL_SR379 */
pub unsafe fn het1GetConfigValue(config_reg: *mut het_config_reg_t,  _type: config_value_type_t)
{
    if let config_value_type_t::InitialValue = _type 
    {
        (*config_reg).CONFIG_GCR     = HET1_GCR_CONFIGVALUE;
        (*config_reg).CONFIG_PFR     = HET1_PFR_CONFIGVALUE;
        (*config_reg).CONFIG_INTENAS = HET1_INTENAS_CONFIGVALUE;
        (*config_reg).CONFIG_INTENAC = HET1_INTENAC_CONFIGVALUE;
        (*config_reg).CONFIG_PRY     = HET1_PRY_CONFIGVALUE;
        (*config_reg).CONFIG_AND     = HET1_AND_CONFIGVALUE;
        (*config_reg).CONFIG_HRSH    = HET1_HRSH_CONFIGVALUE;
        (*config_reg).CONFIG_XOR     = HET1_XOR_CONFIGVALUE;
        (*config_reg).CONFIG_DIR     = HET1_DIR_CONFIGVALUE;
        (*config_reg).CONFIG_PDR     = HET1_PDR_CONFIGVALUE;
        (*config_reg).CONFIG_PULDIS  = HET1_PULDIS_CONFIGVALUE;
        (*config_reg).CONFIG_PSL     = HET1_PSL_CONFIGVALUE;
        (*config_reg).CONFIG_PCR     = HET1_PCR_CONFIGVALUE;
    }
    else
    {
    /*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        (*config_reg).CONFIG_GCR     = hetREG1.GCR.read();
        (*config_reg).CONFIG_PFR     = hetREG1.PFR.read();
        (*config_reg).CONFIG_INTENAS = hetREG1.INTENAS.read();
        (*config_reg).CONFIG_INTENAC = hetREG1.INTENAC.read();
        (*config_reg).CONFIG_PRY     = hetREG1.PRY.read();
        (*config_reg).CONFIG_AND     = hetREG1.AND.read();
        (*config_reg).CONFIG_HRSH    = hetREG1.HRSH.read();
        (*config_reg).CONFIG_XOR     = hetREG1.XOR.read();
        (*config_reg).CONFIG_DIR     = hetREG1.DIR.read();
        (*config_reg).CONFIG_PDR     = hetREG1.PDR.read();
        (*config_reg).CONFIG_PULDIS  = hetREG1.PULDIS.read();
        (*config_reg).CONFIG_PSL     = hetREG1.PSL.read();
        (*config_reg).CONFIG_PCR     = hetREG1.PCR.read();
    }
}



