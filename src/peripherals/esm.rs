#![allow(non_snake_case)]
use crate::system::notification::*;
use crate::registers::reg_esm::*;
use crate::registers::sys_common::*;

use volatile_register::RW;

/* @file esm.c 
*   @brief Esm Driver Source File
*   @date 11-Dec-2018
*   @version 04.07.01
*
*   This file contains:
*   - API Functions
*   .
*   which are relevant for the Esm driver.
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com  
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBu32TORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, Bu32T NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICu32LAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBu32TORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, Bu32T NOT 
*  LIMITED TO, PROCu32REMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR Bu32SINESS INTERRUPTION) HOWEVER CAu32SED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* USER CODE BEGIN (0) */
/* USER CODE END */


/* USER CODE BEGIN (1) */
/* USER CODE END */


/* @fn pub fn esmInit(pub fn)
*   @brief Initializes Esm Driver
*
*   This function initializes the Esm driver.
*
*/

/* USER CODE BEGIN (2) */
/* USER CODE END */
/* SourceId : ESM_SourceId_001 */
/* DesignId : ESM_DesignId_001 */
/* Requirements : HL_SR4 */
pub unsafe fn esmInit()
{
/* USER CODE BEGIN (3) */
/* USER CODE END */

    /* - Disable error pin channels */
    esmREG.ESMDEPAPR1.write(0xFFFFFFFFu32);
    esmREG.ESMIEPCR4.write(0xFFFFFFFFu32);

    /* - Disable interrupts */
    esmREG.ESMIECR1.write(0xFFFFFFFFu32);
    esmREG.ESMIECR4.write(0xFFFFFFFFu32);

    /* - Clear error status flags */
    esmREG.ESMSR1.write(0xFFFFFFFFu32);
    esmREG.ESMSR2.write(0xFFFFFFFFu32);
    esmREG.ESMSSR2.write(0xFFFFFFFFu32);
    esmREG.ESMSR3.write(0xFFFFFFFFu32);
    esmREG.ESMSR4.write(0xFFFFFFFFu32);

    /* - Setup LPC preload */
    esmREG.ESMLTCPR.write(16384u32 - 1u32);

    /* - Reset error pin */
    if esmREG.ESMEPSR.read() == 0u32
    {
        esmREG.ESMEKR.write(0x00000005u32);
    }
    else
    {
        esmREG.ESMEKR.write(0x00000000u32);
    }

    /* - Clear interrupt level */
    esmREG.ESMILCR1.write(0xFFFFFFFFu32);
    esmREG.ESMILCR4.write(0xFFFFFFFFu32);

    /* - Set interrupt level */
esmREG.ESMILSR1.write((0u32 << 31u32)
| (0u32 << 30u32)
| (0u32 << 29u32)
| (0u32 << 28u32)
| (0u32 << 27u32)
| (0u32 << 26u32)
| (0u32 << 25u32)
| (0u32 << 24u32)
| (0u32 << 23u32)
| (0u32 << 22u32)
| (0u32 << 21u32)
| (0u32 << 20u32)
| (0u32 << 19u32)
| (0u32 << 18u32)
| (0u32 << 17u32)
| (0u32 << 16u32)
| (0u32 << 15u32)
| (0u32 << 14u32)
| (0u32 << 13u32)
| (0u32 << 12u32)
| (0u32 << 11u32)
| (0u32 << 10u32)
| (0u32 <<  9u32)
| (0u32 <<  8u32)
| (0u32 <<  7u32)
| (0u32 <<  6u32)
| (0u32 <<  5u32)
| (0u32 <<  4u32)
| (0u32 <<  3u32)
| (0u32 <<  2u32)
| (0u32 <<  1u32)
| (0u32 <<  0u32));

esmREG.ESMILSR4.write((0u32 << 31u32)
| (0u32 << 30u32)
| (0u32 << 29u32)
| (0u32 << 28u32)
| (0u32 << 27u32)
| (0u32 << 26u32)
| (0u32 << 25u32)
| (0u32 << 24u32)
| (0u32 << 23u32)
| (0u32 << 22u32)
| (0u32 << 21u32)
| (0u32 << 20u32)
| (0u32 << 19u32)
| (0u32 << 18u32)
| (0u32 << 17u32)
| (0u32 << 16u32)
| (0u32 << 15u32)
| (0u32 << 14u32)
| (0u32 << 13u32)
| (0u32 << 12u32)
| (0u32 << 11u32)
| (0u32 << 10u32)
| (0u32 <<  9u32)
| (0u32 <<  8u32)
| (0u32 <<  7u32)
| (0u32 <<  6u32)
| (0u32 <<  5u32)
| (0u32 <<  4u32)
| (0u32 <<  3u32)
| (0u32 <<  2u32)
| (0u32 <<  1u32)
| (0u32 <<  0u32));

/* - Enable error pin channels */
esmREG.ESMEEPAPR1.write((0u32 << 31u32)
  | (0u32 << 30u32)
  | (0u32 << 29u32)
  | (0u32 << 28u32)
  | (0u32 << 27u32)
  | (0u32 << 26u32)
  | (0u32 << 25u32)
  | (0u32 << 24u32)
  | (0u32 << 23u32)
  | (0u32 << 22u32)
  | (0u32 << 21u32)
  | (0u32 << 20u32)
  | (0u32 << 19u32)
  | (0u32 << 18u32)
  | (0u32 << 17u32)
  | (0u32 << 16u32)
  | (0u32 << 15u32)
  | (0u32 << 14u32)
  | (0u32 << 13u32)
  | (0u32 << 12u32)
  | (0u32 << 11u32)
  | (0u32 << 10u32)
  | (0u32 <<  9u32)
  | (0u32 <<  8u32)
  | (0u32 <<  7u32)
  | (0u32 <<  6u32)
  | (0u32 <<  5u32)
  | (0u32 <<  4u32)
  | (0u32 <<  3u32)
  | (0u32 <<  2u32)
  | (0u32 <<  1u32)
  | (0u32 <<  0u32));

esmREG.ESMIEPSR4.write((0u32 << 31u32)
 | (0u32 << 30u32)
 | (0u32 << 29u32)
 | (0u32 << 28u32)
 | (0u32 << 27u32)
 | (0u32 << 26u32)
 | (0u32 << 25u32)
 | (0u32 << 24u32)
 | (0u32 << 23u32)
 | (0u32 << 22u32)
 | (0u32 << 21u32)
 | (0u32 << 20u32)
 | (0u32 << 19u32)
 | (0u32 << 18u32)
 | (0u32 << 17u32)
 | (0u32 << 16u32)
 | (0u32 << 15u32)
 | (0u32 << 14u32)
 | (0u32 << 13u32)
 | (0u32 << 12u32)
 | (0u32 << 11u32)
 | (0u32 << 10u32)
 | (0u32 <<  9u32)
 | (0u32 <<  8u32)
 | (0u32 <<  7u32)
 | (0u32 <<  6u32)
 | (0u32 <<  5u32)
 | (0u32 <<  4u32)
 | (0u32 <<  3u32)
 | (0u32 <<  2u32)
 | (0u32 <<  1u32)
 | (0u32 <<  0u32));

/* - Enable interrupts */
esmREG.ESMIESR1.write((0u32 << 31u32)
| (0u32 << 30u32)
| (0u32 << 29u32)
| (0u32 << 28u32)
| (0u32 << 27u32)
| (0u32 << 26u32)
| (0u32 << 25u32)
| (0u32 << 24u32)
| (0u32 << 23u32)
| (0u32 << 22u32)
| (0u32 << 21u32)
| (0u32 << 20u32)
| (0u32 << 19u32)
| (0u32 << 18u32)
| (0u32 << 17u32)
| (0u32 << 16u32)
| (0u32 << 15u32)
| (0u32 << 14u32)
| (0u32 << 13u32)
| (0u32 << 12u32)
| (0u32 << 11u32)
| (0u32 << 10u32)
| (0u32 <<  9u32)
| (0u32 <<  8u32)
| (0u32 <<  7u32)
| (0u32 <<  6u32)
| (0u32 <<  5u32)
| (0u32 <<  4u32)
| (0u32 <<  3u32)
| (0u32 <<  2u32)
| (0u32 <<  1u32)
| (0u32 <<  0u32));

esmREG.ESMIESR4.write((0u32 << 31u32)
| (0u32 << 30u32)
| (0u32 << 29u32)
| (0u32 << 28u32)
| (0u32 << 27u32)
| (0u32 << 26u32)
| (0u32 << 25u32)
| (0u32 << 24u32)
| (0u32 << 23u32)
| (0u32 << 22u32)
| (0u32 << 21u32)
| (0u32 << 20u32)
| (0u32 << 19u32)
| (0u32 << 18u32)
| (0u32 << 17u32)
| (0u32 << 16u32)
| (0u32 << 15u32)
| (0u32 << 14u32)
| (0u32 << 13u32)
| (0u32 << 12u32)
| (0u32 << 11u32)
| (0u32 << 10u32)
| (0u32 <<  9u32)
| (0u32 <<  8u32)
| (0u32 <<  7u32)
| (0u32 <<  6u32)
| (0u32 <<  5u32)
| (0u32 <<  4u32)
| (0u32 <<  3u32)
| (0u32 <<  2u32)
| (0u32 <<  1u32)
| (0u32 <<  0u32));

/* USER CODE BEGIN (4) */
/* USER CODE END */
}


/* @fn uint32 esmError(pub fn)
*   @brief Return Error status
*
*   @return The error status
*
*   Returns the error status.
*/
/* SourceId : ESM_SourceId_002 */
/* DesignId : ESM_DesignId_002 */
/* Requirements : HL_SR5 */
pub fn esmError() -> u32
{
    let status: u32;

/* USER CODE BEGIN (5) */
/* USER CODE END */

    status = esmREG.ESMEPSR.read();

/* USER CODE BEGIN (6) */
/* USER CODE END */

    status
}


/* @fn pub fn esmEnableError(channels: u64)
*   @brief Enable Group 1 Channels Error Signals propagation
*
*   @param[in] channels - Channel mask
*
*   Enable Group 1 Channels Error Signals propagation to the error pin.
*/
/* SourceId : ESM_SourceId_003 */
/* DesignId : ESM_DesignId_003 */
/* Requirements : HL_SR6 */
pub unsafe fn esmEnableError(channels: u64)
{
/* USER CODE BEGIN (7) */
/* USER CODE END */

    esmREG.ESMIEPSR4.write(((channels >> 32u32) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMEEPAPR1.write((channels & 0xFFFFFFFFu64) as u32);

/* USER CODE BEGIN (8) */
/* USER CODE END */
}


/* @fn pub fn esmDisableError(channels: u64)
*   @brief Disable Group 1 Channels Error Signals propagation
*
*   @param[in] channels - Channel mask
*
*   Disable Group 1 Channels Error Signals propagation to the error pin.
*/
/* SourceId : ESM_SourceId_004 */
/* DesignId : ESM_DesignId_004 */
/* Requirements : HL_SR7 */
pub unsafe fn esmDisableError(channels: u64)
{
/* USER CODE BEGIN (9) */
/* USER CODE END */

    esmREG.ESMIEPCR4.write(((channels >> 32u32) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMDEPAPR1.write((channels & 0xFFFFFFFFu64) as u32);

/* USER CODE BEGIN (10) */
/* USER CODE END */
}


/* @fn pub fn esmTriggerErrorPinReset(pub fn)
*   @brief Trigger error pin reset and switch back to normal operation
*
*   Trigger error pin reset and switch back to normal operation.
*/
/* SourceId : ESM_SourceId_005 */
/* DesignId : ESM_DesignId_005 */
/* Requirements : HL_SR8 */
pub unsafe fn esmTriggerErrorPinReset()
{
/* USER CODE BEGIN (11) */
/* USER CODE END */

    esmREG.ESMEKR.write(5u32);

/* USER CODE BEGIN (12) */
/* USER CODE END */
}


/* @fn pub fn esmActivateNormalOperation(pub fn)
*   @brief Activate normal operation
*
*   Activates normal operation mode.
*/
/* SourceId : ESM_SourceId_006 */
/* DesignId : ESM_DesignId_006 */
/* Requirements : HL_SR9 */
pub unsafe fn esmActivateNormalOperation()
{
/* USER CODE BEGIN (13) */
/* USER CODE END */

    esmREG.ESMEKR.write(0u32);

/* USER CODE BEGIN (14) */
/* USER CODE END */
}


/* @fn pub fn esmEnableInterrupt(channels: u64)
*   @brief Enable Group 1 Channels Interrupts
*
*   @param[in] channels - Channel mask
*
*   Enable Group 1 Channels Interrupts.
*/
/* SourceId : ESM_SourceId_007 */
/* DesignId : ESM_DesignId_007 */
/* Requirements : HL_SR1 */
pub unsafe fn esmEnableInterrupt(channels: u64)
{
/* USER CODE BEGIN (15) */
/* USER CODE END */

    esmREG.ESMIESR4.write(((channels >> 32u32) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMIESR1.write((channels & 0xFFFFFFFFu64) as u32);

/* USER CODE BEGIN (16) */
/* USER CODE END */
}


/* @fn pub fn esmDisableInterrupt(channels: u64)
*   @brief Disable Group 1 Channels Interrupts
*
*   @param[in] channels - Channel mask
*
*   Disable Group 1 Channels Interrupts.
*/
/* SourceId : ESM_SourceId_008 */
/* DesignId : ESM_DesignId_008 */
/* Requirements : HL_SR11 */
pub unsafe fn esmDisableInterrupt(channels: u64)
{
/* USER CODE BEGIN (17) */
/* USER CODE END */

    esmREG.ESMIECR4.write(((channels >> 32u32) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMIECR1.write((channels & 0xFFFFFFFFu64) as u32);

/* USER CODE BEGIN (18) */
/* USER CODE END */
}


/* @fn pub fn esmSetInterruptLevel(channels: u64, flags: u64)
*   @brief Set Group 1 Channels Interrupt Levels
*
*   @param[in] channels - Channel mask
*   @param[in] flags - Level mask: - 0: Low priority interrupt
*                                  - 1: High priority interrupt
*
*   Set Group 1 Channels Interrupts levels.
*/
/* SourceId : ESM_SourceId_009 */
/* DesignId : ESM_DesignId_009 */
/* Requirements : HL_SR12 */
pub unsafe fn esmSetInterruptLevel(channels: u64, flags: u64)
{
/* USER CODE BEGIN (19) */
/* USER CODE END */

    esmREG.ESMILCR4.write((((channels & (!flags)) >> 32u32) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMILSR4.write((((channels & flags) >> 32u32) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMILCR1.write(((channels & (!flags)) & 0xFFFFFFFFu64) as u32);
    esmREG.ESMILSR1.write(((channels & flags) & 0xFFFFFFFFu64) as u32);

/* USER CODE BEGIN (20) */
/* USER CODE END */
}


/* @fn pub fn esmClearStatus(uint32 group, channels: u64)
*   @brief Clear Group error status
*
*   @param[in] group    - Error group
*   @param[in] channels - Channel mask
*
*   Clear Group error status.
*/
/* SourceId : ESM_SourceId_010 */
/* DesignId : ESM_DesignId_010 */
/* Requirements : HL_SR13 */
pub unsafe fn esmClearStatus(group: u32, channels: u64)
{
/* USER CODE BEGIN (21) */
/* USER CODE END */

    let source_reg: &RW<u32> = match group {
        0u32 => &esmREG.ESMSR1,
        1u32 => &esmREG.ESMSR2,
        2u32 => &esmREG.ESMSR3,
        _ => &esmREG.ESMSR1, // better than segfaulting
    };
    source_reg.write((channels & 0xFFFFFFFFu64) as u32);
	if group == 0u32
	{
	    esmREG.ESMSR4.write(((channels >> 32u32) & 0xFFFFFFFFu64) as u32);
	}

/* USER CODE BEGIN (22) */
/* USER CODE END */
}


/* @fn pub fn esmClearStatusBuffer(channels: u64)
*   @brief Clear Group 2 error status buffer
*
*   @param[in] channels - Channel mask
*
*   Clear Group 2 error status buffer.
*/
/* SourceId : ESM_SourceId_011 */
/* DesignId : ESM_DesignId_011 */
/* Requirements : HL_SR14 */
pub unsafe fn esmClearStatusBuffer(channels: u64)
{
/* USER CODE BEGIN (23) */
/* USER CODE END */

    esmREG.ESMSSR2.write((channels & 0xFFFFFFFFu64) as u32);

/* USER CODE BEGIN (24) */
/* USER CODE END */
}


/* @fn pub fn esmSetCounterPreloadValue(uint32 value)
*   @brief Set counter preload value
*
*   @param[in] value - Counter preload value
*
*   Set counter preload value.
*/
/* SourceId : ESM_SourceId_012 */
/* DesignId : ESM_DesignId_012 */
/* Requirements : HL_SR15 */
pub unsafe fn esmSetCounterPreloadValue(value: u64)
{
/* USER CODE BEGIN (25) */
/* USER CODE END */

    esmREG.ESMLTCPR.write((value & 0xC000u64) as u32);

/* USER CODE BEGIN (26) */
/* USER CODE END */
}


/* @fn uint64 esmGetStatus(uint32 group, channels: u64)
*   @brief Return Error status
*
*   @param[in] group   - Error group
*   @param[in] channels - Error Channels
*
*   @return The channels status of selected group
*
*   Returns the channels status of selected group.
*/
/* SourceId : ESM_SourceId_013 */
/* DesignId : ESM_DesignId_013 */
/* Requirements : HL_SR16 */
pub fn esmGetStatus(group: u32, channels: u64) -> u64
{
    let status: u64;
	let ESM_ESTATUS4: u32;
    let ESM_ESTATUS1: u32;
	if group == 0u32
	{
		ESM_ESTATUS4 = esmREG.ESMSR4.read();
	}
	else
	{
		ESM_ESTATUS4 = 0u32;
	}
	ESM_ESTATUS1 = match group {
        0u32 => esmREG.ESMSR1.read(),
        1u32 => esmREG.ESMSR2.read(),
        3u32 => esmREG.ESMSR3.read(),
        _ => 0u32,
    };

/* USER CODE BEGIN (27) */
/* USER CODE END */
    status = ((((ESM_ESTATUS4 as u64) << 32u32) as u64) | ESM_ESTATUS1 as u64) & channels;

/* USER CODE BEGIN (28) */
/* USER CODE END */

    status
}


/* @fn uint64 esmGetStatusBuffer(channels: u64)
*   @brief Return Group 2 channel x Error status buffer
*
*   @param[in] channels - Error Channels
*
*   @return The channels status
*
*   Returns the group 2 buffered status of selected channels.
*/
/* SourceId : ESM_SourceId_014 */
/* DesignId : ESM_DesignId_014 */
/* Requirements : HL_SR17 */
pub fn esmGetStatusBuffer(channels: u64) -> u64
{
    let status: u64;

/* USER CODE BEGIN (29) */
/* USER CODE END */
    status = (esmREG.ESMSSR2.read() as u64) & channels;

/* USER CODE BEGIN (30) */
/* USER CODE END */

    status
}

/* @fn esmSelfTestFlag_t esmEnterSelfTest(pub fn)
*   @brief Return ESM Self test status
*
*   @return ESM Self test status
*
*   Returns the ESM Self test status.
*/
/* SourceId : ESM_SourceId_015 */
/* DesignId : ESM_DesignId_015 */
/* Requirements : HL_SR19 */
pub fn esmEnterSelfTest() -> esmSelfTestFlag_t
{
    let mut status: esmSelfTestFlag_t;

/* USER CODE BEGIN (31) */
/* USER CODE END */

	let errPinStat: u32 = esmREG.ESMEPSR.read() & 0x1u32;
	let esmKeyReg: u32  = esmREG.ESMEKR.read();
	if (errPinStat == 0x0u32) && (esmKeyReg == 0x0u32)
	{
	  status = esmSelfTestFlag_t::esmSelfTest_NotStarted;
	}
	else
	{
      unsafe {
        esmREG.ESMEKR.write(0xAu32);
      }
	  status = esmSelfTestFlag_t::esmSelfTest_Active;
	  if (esmREG.ESMEPSR.read() & 0x1u32) != 0x0u32
	  {
	    status = esmSelfTestFlag_t::esmSelfTest_Failed;
	  }
	  unsafe {
        esmREG.ESMEKR.write(0x5u32);
      }
	}

/* USER CODE BEGIN (32) */
/* USER CODE END */

	status
}


/* @fn esmSelfTestFlag_t esmSelfTestStatus(pub fn)
*   @brief Return ESM Self test status
*
*   Returns the ESM Self test status.
*/
/* SourceId : ESM_SourceId_016 */
/* DesignId : ESM_DesignId_016 */
/* Requirements : HL_SR18 */
pub fn esmSelfTestStatus() -> esmSelfTestFlag_t 
{
    let status: esmSelfTestFlag_t;

/* USER CODE BEGIN (33) */
/* USER CODE END */

	if (esmREG.ESMEPSR.read() & 0x1u32) == 0x0u32
	{
	  if esmREG.ESMEKR.read() == 0x5u32
	  { 
	    status = esmSelfTestFlag_t::esmSelfTest_Active;
	  }
	  else
	  {
	    status = esmSelfTestFlag_t::esmSelfTest_Failed;
	  }
	}
	else
	{
	    status = esmSelfTestFlag_t::esmSelfTest_Passed;
	}

/* USER CODE BEGIN (34) */
/* USER CODE END */

	status
}

/* @fn pub fn esmGetConfigValue(esm_config_reg_t *config_reg, config_value_type_t type)
*   @brief Get the initial or current values of the configuration registers
*
*	@param[in] *config_reg: pointer to the struct to which the initial or current 
*                           value of the configuration registers need to be stored
*	@param[in] type: 	whether initial or current value of the configuration registers need to be stored
*						- InitialValue: initial value of the configuration registers will be stored 
*                                       in the struct pointed by config_reg
*						- CurrentValue: initial value of the configuration registers will be stored 
*                                       in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type') 
*   of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : ESM_SourceId_017 */
/* DesignId : ESM_DesignId_017 */
/* Requirements : HL_SR20, HL_SR24 */
pub fn esmGetConfigValue(config_reg: &mut esm_config_reg_t , _type: config_value_type_t)
{
	if let config_value_type_t::InitialValue = _type {
		config_reg.CONFIG_EEPAPR1 = ESM_EEPAPR1_CONFIGVALUE;
		config_reg.CONFIG_IESR1 = ESM_IESR1_CONFIGVALUE;
		config_reg.CONFIG_ILSR1 = ESM_ILSR1_CONFIGVALUE;
		config_reg.CONFIG_LTCPR = ESM_LTCPR_CONFIGVALUE;
		config_reg.CONFIG_EKR = ESM_EKR_CONFIGVALUE;  
		config_reg.CONFIG_IEPSR4 = ESM_IEPSR4_CONFIGVALUE;
		config_reg.CONFIG_IESR4 = ESM_IESR4_CONFIGVALUE;
		config_reg.CONFIG_ILSR4 = ESM_ILSR4_CONFIGVALUE;
	}
	else
	{
	/*SAFETYMCu32SW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
		config_reg.CONFIG_EEPAPR1 = esmREG.ESMEEPAPR1.read();
		config_reg.CONFIG_IESR1 = esmREG.ESMIESR1.read();
		config_reg.CONFIG_ILSR1 = esmREG.ESMILSR1.read();
		config_reg.CONFIG_LTCPR = esmREG.ESMLTCPR.read();
		config_reg.CONFIG_EKR = esmREG.ESMEKR.read();  
		config_reg.CONFIG_IEPSR4 = esmREG.ESMIEPSR4.read();
		config_reg.CONFIG_IESR4 = esmREG.ESMIESR4.read();
		config_reg.CONFIG_ILSR4 = esmREG.ESMILSR4.read();
	}
}

/* USER CODE BEGIN (35) */
/* USER CODE END */

/* @fn pub fn esmHighInterrupt(pub fn)
*   @brief High Level Interrupt for ESM
*/

/*Author's Note: UNCOMMENT WHEN RESOLVED This is something that will need to be handled later... 
    CODE_STATE: Change bit-mode of compilation at function level. http://downloads.ti.com/docs/esd/SPNU151/the-code-state-pragma-spnu1517516.html
    INTERRUPT: Handle interrupt with C code. I assume HPI/LPI map to FIQ/IRQ. https://downloads.ti.com/docs/esd/SPRU514Q/the-interrupt-pragma-stdz0556804.html
    WEAK: Weak binding to a symbol. https://downloads.ti.com/docs/esd/SLAu32132/the-weak-pragma-stdz0556591.html
*/
// #pragma CODE_STATE(esmHighInterrupt, 32)
// #pragma INTERRUPT(esmHighInterrupt, FIQ)

/* SourceId : ESM_SourceId_018 */
/* DesignId : ESM_DesignId_018 */
/* Requirements : HL_SR21, HL_SR22 */
pub unsafe fn esmHighInterrupt()
{
    let vec:u32 = esmREG.ESMIOFFHR.read() - 1u32;

/* USER CODE BEGIN (36) */
/* USER CODE END */

    if vec < 32u32
    {
        esmREG.ESMSR1.write(1u32 << vec);
        esmGroup1Notification(vec);
    }
    else if vec < 64u32
    {
        esmREG.ESMSR2.write(1u32 << (vec-32u32));
        esmGroup2Notification(vec-32u32);
    }
    else if vec < 96u32
    {
        esmREG.ESMSR4.write(1u32 << (vec-64u32));
        esmGroup1Notification(vec-32u32);
    }
    else
    {
        esmREG.ESMSR4.write(0xFFFFFFFFu32);
        esmREG.ESMSR2.write(0xFFFFFFFFu32);
        esmREG.ESMSR1.write(0xFFFFFFFFu32);
    }

/* USER CODE BEGIN (37) */
/* USER CODE END */
}
