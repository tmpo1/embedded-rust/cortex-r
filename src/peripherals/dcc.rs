#![allow(non_snake_case)]
use crate::registers::reg_dcc::*;
use raw_pointer::Pointer;
use crate::registers::sys_common::*;

pub unsafe fn dccInit()
{

    /* @b initialize @b DCC1 */
       
    /* DCC1 Clock0 Counter Seed value configuration */
    dccREG1.CNT0SEED   .write( 78408u32);
    
    /* DCC1 Clock0 Valid Counter Seed value configuration */
    dccREG1.VALID0SEED .write( 1584u32);          
    
    /* DCC1 Clock1 Counter Seed value configuration */
    dccREG1.CNT1SEED   .write( 495000u32);

    /* DCC1 Clock1 Source 1 Select */
    dccREG1.CNT1CLKSRC.write(((10u32 << 12u32) |     /* DCC Enable / Disable Key */
                            dcc1clocksource::DCC1_CNT1_PLL1 as u32) as u32);  /* DCC1 Clock Source 1 */
   
    dccREG1.CNT0CLKSRC .write( dcc1clocksource::DCC1_CNT0_OSCIN as u32);  /* DCC1 Clock Source 0 */

    /* DCC1 Global Control register configuration */
    dccREG1.GCTRL.write(0xAu32 |       /* Enable / Disable DCC1 */
                          (0xAu32 << 4u32) as u32 | /* Error Interrupt */
                          (0x5u32 << 8u32) as u32 | /* Single Shot mode */
                          (0xAu32 << 12u32) as u32); 
                                                    /* Done Interrupt */

/* USER CODE BEGIN (3) */
/* USER CODE END */

}

/* @fn fn dccSetCounter0Seed(dcc: Pointer<dccBASE_t> cnt0seed: u32)
*   @brief Set dcc Clock source 0 counter seed value
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] cnt0seed - Clock Source 0 Counter seed value
*
*   This function sets the seed value for Clock source 0 counter.
*
*/
/* SourceId : DCC_SourceId_002 */
/* DesignId : DCC_DesignId_002 */
/* Requirements: HL_SR306 */
pub unsafe fn dccSetCounter0Seed(dcc: Pointer<dccBASE_t>, cnt0seed: u32)
{
/* USER CODE BEGIN (4) */
/* USER CODE END */

    dcc.CNT0SEED     .write( cnt0seed);

/* USER CODE BEGIN (5) */
/* USER CODE END */
}

/* @fn void dccSetTolerance(dcc: Pointer<dccBASE_t> valid0seed: u32)
*   @brief Set dcc Clock source 0 counter seed value
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] valid0seed - Clock Source 0 Counter tolerance value
*
*   This function sets the seed value for Clock source 0 tolerance or 
*    valid counter.
*
*/
/* SourceId : DCC_SourceId_003 */
/* DesignId : DCC_DesignId_003 */
/* Requirements: HL_SR307 */
pub unsafe fn dccSetTolerance(dcc: Pointer<dccBASE_t>, valid0seed: u32)
{
/* USER CODE BEGIN (6) */
/* USER CODE END */

    dcc.VALID0SEED .write( valid0seed);

/* USER CODE BEGIN (7) */
/* USER CODE END */
}

/* @fn void dccSetCounter1Seed(dcc: Pointer<dccBASE_t> cnt1seed: u32)
*   @brief Set dcc Clock source 1 counter seed value
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] cnt1seed - Clock Source 1 Counter seed value
*
*   This function sets the seed value for Clock source 1 counter.
*
*/
/* SourceId : DCC_SourceId_004 */
/* DesignId : DCC_DesignId_004 */
/* Requirements: HL_SR308 */
pub unsafe fn dccSetCounter1Seed(dcc: Pointer<dccBASE_t>, cnt1seed: u32)
{
/* USER CODE BEGIN (8) */
/* USER CODE END */

    dcc.CNT1SEED     .write( cnt1seed);

/* USER CODE BEGIN (9) */
/* USER CODE END */
}

/* @fn void dccSetSeed(dcc, cnt0seed: u32, valid0seed: u32: Pointer<dccBASE_t> cnt1seed: u32)
*   @brief Set dcc Clock source 0 counter seed value
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] cnt0seed        - Clock Source 0 Counter seed value.
*   @param[in] valid0seed    - Clock Source 0 Counter tolerance value.
*   @param[in] cnt1seed        - Clock Source 1 Counter seed value.
*
*   This function sets the seed value for clock source 0, clock source 1 
*    and tolerance counter.
*
*/
/* SourceId : DCC_SourceId_005 */
/* DesignId : DCC_DesignId_005 */
/* Requirements: HL_SR309 */
pub unsafe fn dccSetSeed(dcc: Pointer<dccBASE_t>, cnt0seed: u32, valid0seed: u32, cnt1seed: u32)
{
/* USER CODE BEGIN (10) */
/* USER CODE END */

    dcc.CNT0SEED   .write( cnt0seed);
    dcc.VALID0SEED .write( valid0seed);
    dcc.CNT1SEED   .write( cnt1seed);

/* USER CODE BEGIN (11) */
/* USER CODE END */
}

/* @fn void dccSelectClockSource(dcc: Pointer<dccBASE_t>, cnt0_Clock_Source: u32, cnt1_Clock_Source: u32)
*   @brief Set dcc counter Clock sources
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] cnt0_Clock_Source - Clock source for counter 0.
*   @param[in] cnt1_Clock_Source - Clock source for counter 1.
*
*   This function sets the dcc counter 0 and counter 1 clock sources.
*   DCC must be disabled using dccDisable API before calling this 
*   function.
*/
/* SourceId : DCC_SourceId_006 */
/* DesignId : DCC_DesignId_006 */
/* Requirements: HL_SR310 */
pub unsafe fn dccSelectClockSource(dcc: Pointer<dccBASE_t>, cnt0_Clock_Source: u32, cnt1_Clock_Source: u32)
{
/* USER CODE BEGIN (12) */
/* USER CODE END */

    dcc.CNT1CLKSRC.write((0xAu32 << 12u32) as u32 |     		/* DCC Enable Key */
	                    (cnt1_Clock_Source & 0x0000000Fu32) as u32);	/* Configure Clock source 1 */
    dcc.CNT0CLKSRC  .write( cnt0_Clock_Source);  							/* Configure Clock source 0 */
	
/* USER CODE BEGIN (13) */
/* USER CODE END */
}

/* @fn void dccEnable(dcc: Pointer<dccBASE_t>)
*   @brief Enable dcc module to begin counting
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*
*   This function enables the dcc counters to begin counting.
*
*/
/* SourceId : DCC_SourceId_007 */
/* DesignId : DCC_DesignId_007 */
/* Requirements: HL_SR311 */
pub unsafe fn dccEnable(dcc: Pointer<dccBASE_t>)
{
/* USER CODE BEGIN (14) */
/* USER CODE END */
/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
    dcc.GCTRL .write( (dcc.GCTRL.read() & 0xFFFFFFF0u32) | 0xAu32); 

/* USER CODE BEGIN (15) */
/* USER CODE END */
}

/* @fn void dccDisable(dcc: Pointer<dccBASE_t>)
*   @brief Make selected dcc module to stop counting
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*
*   This function stops the dcc counters from counting.
*
*/
/* SourceId : DCC_SourceId_008 */
/* DesignId : DCC_DesignId_008 */
/* Requirements: HL_SR312 */
pub unsafe fn dccDisable(dcc: Pointer<dccBASE_t>)
{
/* USER CODE BEGIN (16) */
/* USER CODE END */
/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
    dcc.GCTRL .write( (dcc.GCTRL.read() & 0xFFFFFFF0u32) | 0x5u32); 

/* USER CODE BEGIN (17) */
/* USER CODE END */
}

/* @fn uint32 dccGetErrStatus(dcc: Pointer<dccBASE_t>)
*   @brief Get error status from selected dcc module
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*
*   @return The Error status of selected dcc module
*
*   Returns the error status of selected dcc module.
*
*/
/* SourceId : DCC_SourceId_009 */
/* DesignId : DCC_DesignId_009 */
/* Requirements: HL_SR313 */
pub unsafe fn dccGetErrStatus(dcc: Pointer<dccBASE_t>) -> u32
{
/* USER CODE BEGIN (18) */
/* USER CODE END */

    dcc.STAT.read() & 0x00000001u32
}

/* @fn void dccEnableNotification(dcc: Pointer<dccBASE_t> notification: u32)
*   @brief Enable notification of selected DCC module
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] notification Select notification of DCC module:
*              - dccNOTIFICATION_DONE:    DCC DONE notification
*              - dccNOTIFICATION_ERROR: DCC ERROR notification
*
*   This function will enable the selected notification of a DCC module.
*   It is possible to enable multiple notifications masked.
*/
/* SourceId : DCC_SourceId_010 */
/* DesignId : DCC_DesignId_010 */
/* Requirements: HL_SR314 */
pub unsafe fn dccEnableNotification(dcc: Pointer<dccBASE_t>, notification: u32)
{
/* USER CODE BEGIN (19) */
/* USER CODE END */
/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
    dcc.GCTRL .write( (dcc.GCTRL.read() & 0xFFFF0F0Fu32) | notification); 

/* USER CODE BEGIN (20) */
/* USER CODE END */
}

/* @fn void dccDisableNotification(dcc: Pointer<dccBASE_t> notification: u32)
*   @brief Disable notification of selected DCC module
*   @param[in] dcc Pointer to DCC module:
*              - dccREG1: DCC1 module pointer
*              - dccREG2: DCC2 module pointer
*   @param[in] notification Select notification of DCC module:
*              - dccNOTIFICATION_DONE:    DCC DONE notification
*              - dccNOTIFICATION_ERROR: DCC ERROR notification
*
*   This function will enable the selected notification of a DCC module.
*   It is possible to enable multiple notifications masked.
*/
/* SourceId : DCC_SourceId_011 */
/* DesignId : DCC_DesignId_011 */
/* Requirements: HL_SR315 */
pub unsafe fn dccDisableNotification(dcc: Pointer<dccBASE_t>, notification: u32)
{
/* USER CODE BEGIN (21) */
/* USER CODE END */
/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
	dcc.GCTRL .write( (dcc.GCTRL.read() & 0xFFFF0F0Fu32) | ((!notification) & 0x0000F0F0u32));

/* USER CODE BEGIN (22) */
/* USER CODE END */
}

/* @fn void dcc1GetConfigValue(dcc_config_reg_t *config_reg, config_value__type_t _type)
*   @brief Get the initial or current values of the configuration registers
*
*    @param[in] *config_reg: pointer to the struct to which the initial or current value of the configuration registers need to be stored
*    @param[in] _type:     whether initial or current value of the configuration registers need to be stored
*                        - InitialValue: initial value of the configuration registers will be stored in the struct pointed by config_reg
*                        - CurrentValue: initial value of the configuration registers will be stored in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter '_type') of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : DCC_SourceId_012 */
/* DesignId : DCC_DesignId_012 */
/* Requirements: HL_SR318 */
pub fn dcc1GetConfigValue(config_reg: &mut dcc_config_reg_t,  _type: config_value_type_t)
{
    if let config_value_type_t::InitialValue = _type
    {
        config_reg.CONFIG_GCTRL = DCC1_GCTRL_CONFIGVALUE;
        config_reg.CONFIG_CNT0SEED = DCC1_CNT0SEED_CONFIGVALUE;
        config_reg.CONFIG_VALID0SEED = DCC1_VALID0SEED_CONFIGVALUE;
        config_reg.CONFIG_CNT1SEED = DCC1_CNT1SEED_CONFIGVALUE;
        config_reg.CONFIG_CNT1CLKSRC = DCC1_CNT1CLKSRC_CONFIGVALUE;
        config_reg.CONFIG_CNT0CLKSRC = DCC1_CNT0CLKSRC_CONFIGVALUE;
    }
    else
    {
	/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        config_reg.CONFIG_GCTRL = dccREG1.GCTRL.read();
        config_reg.CONFIG_CNT0SEED = dccREG1.CNT0SEED.read();
        config_reg.CONFIG_VALID0SEED = dccREG1.VALID0SEED.read();
        config_reg.CONFIG_CNT1SEED = dccREG1.CNT1SEED.read();
        config_reg.CONFIG_CNT1CLKSRC = dccREG1.CNT1CLKSRC.read();
        config_reg.CONFIG_CNT0CLKSRC = dccREG1.CNT0CLKSRC.read();
    }
}
