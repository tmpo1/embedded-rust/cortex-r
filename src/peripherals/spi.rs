#![allow(non_camel_case_types, dead_code, non_snake_case, non_upper_case_globals)]
use raw_pointer::*;
use crate::registers::reg_spi::*;
use volatile::Volatile;
use crate::registers::sys_common::*;

/* @file spi.c
*   @brief SPI Driver Implementation File
*   @date 11-Dec-2018
*   @version 04.07.01
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com  
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* @struct g_spiPacket
*   @brief globals
*
*/
// static volatile struct g_spiPacket
// {
//     spiDAT1_t g_spiDataFormat;
//     uint32  tx_length;
//     uint32  rx_length;
//     uint16  * txdata_ptr;
//     uint16  * rxdata_ptr;
//     SpiDataStatus_t tx_data_status;
//     SpiDataStatus_t rx_data_status;
// } 

/* Author's Note: this struct needs to be volatile? */
pub struct g_spiPacket {
    g_spiDataFormat: spiDAT1_t,
    tx_length: u32,
    rx_length: u32,
    txdata_ptr: Pointer<u16>,
    rxdata_ptr: Pointer<u16>,
    tx_data_status: SpiDataStatus_t,
    rx_data_status: SpiDataStatus_t,
} 

/* Author's Note: initialize to default values */
static mut g_spiPacket_t: [g_spiPacket; 3] = [
    g_spiPacket{
        g_spiDataFormat: spiDAT1_t {
            CS_HOLD: false,
            WDEL: false,
            DFSEL: SPIDATAFMT_t::SPI_FMT_0,
            CSNR: 0u8,
        },
        tx_length: 0u32,
        rx_length: 0u32,
        txdata_ptr: Pointer::<u16> {
            ptr: 0u32 as *mut u16
        },
        rxdata_ptr: Pointer::<u16> {
            ptr: 0u32 as *mut u16
        },
        tx_data_status: SpiDataStatus_t::SPI_READY,
        rx_data_status: SpiDataStatus_t::SPI_READY,
    }, g_spiPacket{
        g_spiDataFormat: spiDAT1_t {
            CS_HOLD: false,
            WDEL: false,
            DFSEL: SPIDATAFMT_t::SPI_FMT_0,
            CSNR: 0u8,
        },
        tx_length: 0u32,
        rx_length: 0u32,
        txdata_ptr: Pointer::<u16> {
            ptr: 0u32 as *mut u16
        },
        rxdata_ptr: Pointer::<u16> {
            ptr: 0u32 as *mut u16
        },
        tx_data_status: SpiDataStatus_t::SPI_READY,
        rx_data_status: SpiDataStatus_t::SPI_READY,
    }, g_spiPacket{
        g_spiDataFormat: spiDAT1_t {
            CS_HOLD: false,
            WDEL: false,
            DFSEL: SPIDATAFMT_t::SPI_FMT_0,
            CSNR: 0u8,
        },
        tx_length: 0u32,
        rx_length: 0u32,
        txdata_ptr: Pointer::<u16> {
            ptr: 0u32 as *mut u16
        },
        rxdata_ptr: Pointer::<u16> {
            ptr: 0u32 as *mut u16
        },
        tx_data_status: SpiDataStatus_t::SPI_READY,
        rx_data_status: SpiDataStatus_t::SPI_READY,
    },
];

/* @fn fn spiInit(fn)
*   @brief Initializes the SPI Driver
*
*   This function initializes the SPI module.
*/
/* SourceId : SPI_SourceId_001 */
/* DesignId : SPI_DesignId_001 */
/* Requirements : HL_SR126 */
pub unsafe fn spiInit()
{



    /* @b initialize @b SPI2 */

    /* bring SPI out of reset */
    spiREG2.GCR0.write(0u32);
    spiREG2.GCR0.write(1u32);

    /* SPI2 master mode and clock configuration */
    spiREG2.GCR1.write((spiREG2.GCR1.read() & 0xFFFFFFFCu32) | ((1u32 << 1u32)  /* CLOKMOD */
                  | 1u32));  /* MASTER */

    /* SPI2 enable pin configuration */
    spiREG2.INT0.write((spiREG2.INT0.read() & 0xFEFFFFFFu32)| (0u32 << 24u32));  /* ENABLE HIGHZ */

    /* - Delays */
    spiREG2.DELAY.write( (0u32 << 24u32)  /* C2TDELAY */
                   | (0u32 << 16u32)  /* T2CDELAY */
                   | (0u32 << 8u32)   /* T2EDELAY */
                   | (0u32 << 0u32));  /* C2EDELAY */

    /* - Data Format 0 */
    spiREG2.FMT0.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */
    /* - Data Format 1 */
    spiREG2.FMT1.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - Data Format 2 */
    spiREG2.FMT2.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - Data Format 3 */
    spiREG2.FMT3.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - set interrupt levels */
    spiREG2.LVL.write( (0u32 << 9u32)  /* TXINT */
                 | (0u32 << 8u32)  /* RXINT */
                 | (0u32 << 6u32)  /* OVRNINT */
                 | (0u32 << 4u32)  /* BITERR */
                 | (0u32 << 3u32)  /* DESYNC */
                 | (0u32 << 2u32)  /* PARERR */
                 | (0u32 << 1u32) /* TIMEOUT */
                 | (0u32 << 0u32));  /* DLENERR */

    /* - clear any pending interrupts */
    spiREG2.FLG.write(    spiREG2.FLG.read() | 0xFFFFu32);

    /* - enable interrupts */
    spiREG2.INT0.write( (spiREG2.INT0.read() & 0xFFFF0000u32)
                  | (0u32 << 9u32)  /* TXINT */
                  | (0u32 << 8u32)  /* RXINT */
                  | (0u32 << 6u32)  /* OVRNINT */
                  | (0u32 << 4u32)  /* BITERR */
                  | (0u32 << 3u32)  /* DESYNC */
                  | (0u32 << 2u32)  /* PARERR */
                  | (0u32 << 1u32) /* TIMEOUT */
                  | (0u32 << 0u32));  /* DLENERR */

    /* @b initialize @b SPI2 @b Port */

    /* - SPI2 Port output values */
    spiREG2.PC3.write(    (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 9u32)  /* CLK */
                    | (0u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* - SPI2 Port direction */
    spiREG2.PC1 .write(   (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (1u32 << 9u32)  /* CLK */
                    | (1u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* - SPI2 Port open drain enable */
    spiREG2.PC6 .write(   (0u32 << 0u32)  /* SCS[0] */
                    | (0u32 << 1u32)  /* SCS[1] */
                    | (0u32 << 2u32)  /* SCS[2] */
                    | (0u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 9u32)  /* CLK */
                    | (0u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* - SPI2 Port pullup / pulldown selection */
    spiREG2.PC8 .write(   (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (1u32 << 9u32)  /* CLK */
                    | (1u32 << 10u32)  /* SIMO */
                    | (1u32 << 11u32)); /* SOMI */

    /* - SPI2 Port pullup / pulldown enable*/
    spiREG2.PC7 .write(   (0u32 << 0u32)  /* SCS[0] */
                    | (0u32 << 1u32)  /* SCS[1] */
                    | (0u32 << 2u32)  /* SCS[2] */
                    | (0u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 9u32)  /* CLK */
                    | (0u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* SPI2 set all pins to functional */
    spiREG2.PC0 .write(   (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (1u32 << 9u32)  /* CLK */
                    | (1u32 << 10u32)  /* SIMO */
                    | (1u32 << 11u32)); /* SOMI */

    /* - Initialize TX and RX data buffer Status */
    g_spiPacket_t[1].tx_data_status = SpiDataStatus_t::SPI_READY;
    g_spiPacket_t[1].rx_data_status = SpiDataStatus_t::SPI_READY;

    /* - Finally start SPI2 */
    spiREG2.GCR1.write((spiREG2.GCR1.read() & 0xFEFFFFFFu32) | 0x01000000u32);

    /* @b initialize @b SPI3 */

    /* bring SPI out of reset */
    spiREG3.GCR0.write(0u32);
    spiREG3.GCR0.write(1u32);

    /* SPI3 master mode and clock configuration */
    spiREG2.GCR1.write( (spiREG3.GCR1.read() & 0xFFFFFFFCu32) | (1u32 << 1u32)  /* CLOKMOD */
                  | 1u32);  /* MASTER */

    /* SPI3 enable pin configuration */
    spiREG3.INT0.write((spiREG3.INT0.read() & 0xFEFFFFFFu32) | (0u32 << 24u32));  /* ENABLE HIGHZ */

    /* - Delays */
    spiREG2.DELAY.write( (0u32 << 24u32)  /* C2TDELAY */
                   | (0u32 << 16u32)  /* T2CDELAY */
                   | (0u32 << 8u32)   /* T2EDELAY */
                   | (0u32 << 0u32));  /* C2EDELAY */

    /* - Data Format 0 */
    spiREG2.FMT0.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - Data Format 1 */
    spiREG2.FMT1.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - Data Format 2 */
    spiREG2.FMT2.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - Data Format 3 */
    spiREG2.FMT3.write( (0u32 << 24u32)  /* wdelay */
                  | (0u32 << 23u32)  /* parity Polarity */
                  | (0u32 << 22u32)  /* parity enable */
                  | (0u32 << 21u32)  /* wait on enable */
                  | (0u32 << 20u32)  /* shift direction */
                  | (0u32 << 17u32)  /* clock polarity */
                  | (0u32 << 16u32)  /* clock phase */
                  | (99u32 << 8u32) /* baudrate prescale */
                  | (16u32 << 0u32));  /* data word length */

    /* - set interrupt levels */
    spiREG2.LVL.write( (0u32 << 9u32)  /* TXINT */
                 | (0u32 << 8u32)  /* RXINT */
                 | (0u32 << 6u32)  /* OVRNINT */
                 | (0u32 << 4u32)  /* BITERR */
                 | (0u32 << 3u32)  /* DESYNC */
                 | (0u32 << 2u32)  /* PARERR */
                 | (0u32 << 1u32) /* TIMEOUT */
                 | (0u32 << 0u32));  /* DLENERR */

    /* - clear any pending interrupts */
    spiREG3.FLG.write(    spiREG3.FLG.read() | 0xFFFFu32);

    /* - enable interrupts */
    spiREG2.INT0.write( (spiREG3.INT0.read() & 0xFFFF0000u32)
                  | (0u32 << 9u32)  /* TXINT */
                  | (0u32 << 8u32)  /* RXINT */
                  | (0u32 << 6u32)  /* OVRNINT */
                  | (0u32 << 4u32)  /* BITERR */
                  | (0u32 << 3u32)  /* DESYNC */
                  | (0u32 << 2u32)  /* PARERR */
                  | (0u32 << 1u32) /* TIMEOUT */
                  | (0u32 << 0u32));  /* DLENERR */

    /* @b initialize @b SPI3 @b Port */

    /* - SPI3 Port output values */
    spiREG2.PC3.write(    (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 8u32)  /* ENA */
                    | (0u32 << 9u32)  /* CLK */
                    | (0u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* - SPI3 Port direction */
    spiREG2.PC1 .write(   (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 8u32)  /* ENA */
                    | (1u32 << 9u32)  /* CLK */
                    | (1u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* - SPI3 Port open drain enable */
    spiREG2.PC6 .write(   (0u32 << 0u32)  /* SCS[0] */
                    | (0u32 << 1u32)  /* SCS[1] */
                    | (0u32 << 2u32)  /* SCS[2] */
                    | (0u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 8u32)  /* ENA */
                    | (0u32 << 9u32)  /* CLK */
                    | (0u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* - SPI3 Port pullup / pulldown selection */
    spiREG2.PC8 .write(   (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (1u32 << 8u32)  /* ENA */
                    | (1u32 << 9u32)  /* CLK */
                    | (1u32 << 10u32)  /* SIMO */
                    | (1u32 << 11u32)); /* SOMI */

    /* - SPI3 Port pullup / pulldown enable*/
    spiREG2.PC7 .write(   (0u32 << 0u32)  /* SCS[0] */
                    | (0u32 << 1u32)  /* SCS[1] */
                    | (0u32 << 2u32)  /* SCS[2] */
                    | (0u32 << 3u32)  /* SCS[3] */
                    | (0u32 << 8u32)  /* ENA */
                    | (0u32 << 9u32)  /* CLK */
                    | (0u32 << 10u32)  /* SIMO */
                    | (0u32 << 11u32)); /* SOMI */

    /* SPI3 set all pins to functional */
    spiREG2.PC0 .write(   (1u32 << 0u32)  /* SCS[0] */
                    | (1u32 << 1u32)  /* SCS[1] */
                    | (1u32 << 2u32)  /* SCS[2] */
                    | (1u32 << 3u32)  /* SCS[3] */
                    | (1u32 << 8u32)  /* ENA */
                    | (1u32 << 9u32)  /* CLK */
                    | (1u32 << 10u32)  /* SIMO */
                    | (1u32 << 11u32)); /* SOMI */

    /* - Initialize TX and RX data buffer Status */
    g_spiPacket_t[2].tx_data_status = SpiDataStatus_t::SPI_READY;
    g_spiPacket_t[2].rx_data_status = SpiDataStatus_t::SPI_READY;
    /* - Finally start SPI3 */
    spiREG3.GCR1.write((spiREG3.GCR1.read() & 0xFEFFFFFFu32) | 0x01000000u32);

/* USER CODE BEGIN (3) */
/* USER CODE END */
}


/* @fn fn spiSetFunctional(spi: Pointer<spiBASE_t>, port: u32)
*   @brief Change functional behavior of pins at runtime.
*   @param[in] spi   - Spi module base address
*   @param[in] port  - Value to write to PC0 register
*
*   Change the value of the PC0 register at runtime, this allows to
*   dynamically change the functionality of the SPI pins between functional
*   and GIO mode.
*/
/* SourceId : SPI_SourceId_002 */
/* DesignId : SPI_DesignId_002 */
/* Requirements : HL_SR128 */
pub unsafe fn spiSetFunctional(spi: Pointer<spiBASE_t>, port: u32)
{
/* USER CODE BEGIN (4) */
/* USER CODE END */

    spi.PC0.write(port);

/* USER CODE BEGIN (5) */
/* USER CODE END */
}


/* @fn uint32 spiReceiveData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, blocksize: u32, uint16 * destbuff)
*   @brief Receives Data using polling method
*   @param[in] spi           - Spi module base address
*   @param[in] dataconfig_t    - Spi DAT1 register configuration
*   @param[in] blocksize    - number of data
*   @param[in] destbuff        - Pointer to the destination data (16 bit).
*
*   @return flag register value.
*
*   This function transmits blocksize number of data from source buffer using polling method.
*/
/* SourceId : SPI_SourceId_003 */
/* DesignId : SPI_DesignId_007 */
/* Requirements : HL_SR133 */

/* Author's Note: Could destbuff be reference? This whole method felt janky*/
pub fn spiReceiveData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, mut blocksize: u32, mut destbuff: Pointer<u16>) -> u32
{
/* USER CODE BEGIN (6) */
/* USER CODE END */
    let mut Chip_Select_Hold: u32 = if dataconfig_t.CS_HOLD  {0x10000000u32} else {0u32};
    let WDelay: u32 = if dataconfig_t.WDEL  {0x04000000u32} else {0u32};
    let DataFormat: u32 = dataconfig_t.DFSEL;
    let ChipSelect: u8 = dataconfig_t.CSNR;

    while blocksize != 0u32
    {
        if (spi.FLG.read() & 0x000000FFu32) !=0u32
        {
          break;
        }
        if blocksize == 1u32
        {
           Chip_Select_Hold = 0u32;
        }
		
        /*SAFETYMCu32SW 51 S MR:12.3 <APPROVED> "Needs shifting for 32-bit value" */
        unsafe { 
            spi.DAT1.write((DataFormat << 24u32) |
                    ((ChipSelect as u32) << 16u32) as u32 |
                    (WDelay)            |
                    (Chip_Select_Hold)  |
                    (0x00000000u32));
        }
        /*SAFETYMCu32SW 28 D MR:NA <APPROVED> "Hardware status bit read check" */
        while (spi.FLG.read() & 0x00000100u32) != 0x00000100u32
        {
        } /* Wait */
        /*SAFETYMCu32SW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        *destbuff = spi.BUF.read() as u16;
        /*SAFETYMCu32SW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        unsafe {
            destbuff.ptr = destbuff.ptr.add(1);
        }
        blocksize -= 1;
    }

/* USER CODE BEGIN (7) */
/* USER CODE END */

    spi.FLG.read() & 0xFFu32
}


/* @fn uint32 spiGetData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, blocksize: u32, uint16 * destbuff)
*   @brief Receives Data using interrupt method
*   @param[in] spi           - Spi module base address
*   @param[in] dataconfig_t    - Spi DAT1 register configuration
*   @param[in] blocksize    - number of data
*   @param[in] destbuff        - Pointer to the destination data (16 bit).
*
*   @return flag register value.
*
*   This function transmits blocksize number of data from source buffer using interrupt method.
*/
/* SourceId : SPI_SourceId_004 */
/* DesignId : SPI_DesignId_008 */
/* Requirements : HL_SR134 */

/* Author's Note: this method takes control of dataconfig_t which is probably not desirable */
pub unsafe fn spiGetData(spi: Pointer<spiBASE_t>, dataconfig_t: spiDAT1_t, blocksize: u32, destbuff: Pointer<u16>)
{
     let index: usize = if spi.ptr == spiREG1.ptr {0}  else { if spi.ptr == spiREG2.ptr {1} else {2}};

/* USER CODE BEGIN (8) */
/* USER CODE END */

     g_spiPacket_t[index].rx_length = blocksize;
     g_spiPacket_t[index].rxdata_ptr   = destbuff;
     g_spiPacket_t[index].g_spiDataFormat = dataconfig_t;
     g_spiPacket_t[index].rx_data_status =  SpiDataStatus_t::SPI_PENDING;

     spi.INT0.write(spi.INT0.read() | 0x0100u32);

/* USER CODE BEGIN (9) */
/* USER CODE END */
}


/* @fn uint32 spiTransmitData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, blocksize: u32, uint16 * srcbuff)
*   @brief Transmits Data using polling method
*   @param[in] spi           - Spi module base address
*   @param[in] dataconfig_t    - Spi DAT1 register configuration
*   @param[in] blocksize    - number of data
*   @param[in] srcbuff        - Pointer to the source data ( 16 bit).
*
*   @return flag register value.
*
*   This function transmits blocksize number of data from source buffer using polling method.
*/
/* SourceId : SPI_SourceId_005 */
/* DesignId : SPI_DesignId_005 */
/* Requirements : HL_SR131 */
pub unsafe fn spiTransmitData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, mut blocksize: u32, mut srcbuff: Pointer<u16>) -> u32
{
    // volatile uint32 SpiBuf;
    let mut SpiBuf: u32 = 0;
    let mut SpiBuf = Volatile::new(&mut SpiBuf);
    let mut Tx_Data: u32;
    let mut Chip_Select_Hold: u32 = if dataconfig_t.CS_HOLD {0x10000000u32} else {0u32};
    let WDelay: u32 = if dataconfig_t.WDEL {0x04000000u32} else {0u32};
    let DataFormat: u32  = dataconfig_t.DFSEL;
    let ChipSelect:u8 = dataconfig_t.CSNR;

/* USER CODE BEGIN (10) */
/* USER CODE END */
    while blocksize != 0u32
    {
        if (spi.FLG.read() & 0x000000FFu32) !=0u32
        {
           break;
        }

        if blocksize == 1u32
        {
           Chip_Select_Hold = 0u32;
        }
        /*SAFETYMCu32SW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        Tx_Data = *srcbuff as u32;

        spi.DAT1.write((DataFormat << 24u32) |
                     ((ChipSelect as u32) << 16u32) |
                     (WDelay)           |
                     (Chip_Select_Hold) |
                     Tx_Data);
        /*SAFETYMCu32SW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        srcbuff.ptr = srcbuff.ptr.add(1);
        /*SAFETYMCu32SW 28 D MR:NA <APPROVED> "Hardware status bit read check" */
        while (spi.FLG.read() & 0x00000100u32) != 0x00000100u32
        {
        } /* Wait */
        SpiBuf.write(spi.BUF.read());

        blocksize -= 1; 
    }

/* USER CODE BEGIN (11) */
/* USER CODE END */

    spi.FLG.read() & 0xFFu32
}



/* @fn fn spiSendData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, blocksize: u32, uint16 * srcbuff)
*   @brief Transmits Data using interrupt method
*   @param[in] spi           - Spi module base address
*   @param[in] dataconfig_t    - Spi DAT1 register configuration
*   @param[in] blocksize    - number of data
*   @param[in] srcbuff        - Pointer to the source data ( 16 bit).
*
*   @return flag register value.
*
*   This function transmits blocksize number of data from source buffer using interrupt method.
*/
/* SourceId : SPI_SourceId_006 */
/* DesignId : SPI_DesignId_006 */
/* Requirements : HL_SR132 */

/* Author's Note: this method takes control of dataconfig_t which is probably not desirable */
pub unsafe fn spiSendData(spi: Pointer<spiBASE_t>, dataconfig_t: spiDAT1_t, blocksize: u32,  srcbuff: Pointer<u16>)
{
    let index: usize = if spi.ptr == spiREG1.ptr {0usize} else {if spi.ptr == spiREG2.ptr {1usize} else {2usize}};

/* USER CODE BEGIN (12) */
/* USER CODE END */

     g_spiPacket_t[index].tx_length = blocksize;
     g_spiPacket_t[index].txdata_ptr   = srcbuff;
     g_spiPacket_t[index].g_spiDataFormat = dataconfig_t;
     g_spiPacket_t[index].tx_data_status = SpiDataStatus_t::SPI_PENDING;

     spi.INT0.write(     spi.INT0.read() | 0x0200u32);

/* USER CODE BEGIN (13) */
/* USER CODE END */
}


/* @fn uint32 spiTransmitAndReceiveData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, blocksize: u32, uint16 * srcbuff, uint16 * destbuff)
*   @brief Transmits and Receive Data using polling method
*   @param[in] spi           - Spi module base address
*   @param[in] dataconfig_t    - Spi DAT1 register configuration
*   @param[in] blocksize    - number of data
*   @param[in] srcbuff        - Pointer to the source data ( 16 bit).
*   @param[in] destbuff        - Pointer to the destination data ( 16 bit).
*
*   @return flag register value.
*
*   This function transmits and receives blocksize number of data from source buffer using polling method.
*/
/* SourceId : SPI_SourceId_007 */
/* DesignId : SPI_DesignId_009 */
/* Requirements : HL_SR135 */
pub unsafe fn spiTransmitAndReceiveData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, mut blocksize: u32, mut srcbuff: Pointer<u16>, mut destbuff: Pointer<u16>) -> u32
{


//     
//     let mut Chip_Select_Hold: u32 = if dataconfig_t.CS_HOLD {0x10000000u32} else {0u32};
//     let WDelay: u32 = if dataconfig_t.WDEL {0x04000000u32} else {0u32};
//     let DataFormat: u32  = dataconfig_t.DFSEL;
//     let ChipSelect:u8 = dataconfig_t.CSNR;
    let mut Tx_Data: u32;
    let mut Chip_Select_Hold: u32 = if dataconfig_t.CS_HOLD  {0x10000000u32} else {0u32};
    let WDelay: u32 = if dataconfig_t.WDEL {0x04000000u32} else {0u32};
    let DataFormat:u32 = dataconfig_t.DFSEL;
    let ChipSelect:u8 = dataconfig_t.CSNR;

/* USER CODE BEGIN (14) */
/* USER CODE END */
    while blocksize != 0u32
    {
        if (spi.FLG.read() & 0x000000FFu32) !=0u32
        {
           break;
        }

        if blocksize == 1u32
        {
           Chip_Select_Hold = 0u32;
        }
        /*SAFETYMCu32SW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        Tx_Data = *srcbuff as u32;

        spi.DAT1.write((DataFormat << 24u32) |
                   ((ChipSelect as u32) << 16u32) |
                   (WDelay)           |
                   (Chip_Select_Hold) |
                   Tx_Data);
        /*SAFETYMCu32SW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        srcbuff.ptr = srcbuff.ptr.add(1);
        /*SAFETYMCu32SW 28 D MR:NA <APPROVED> "Hardware status bit read check" */
        while (spi.FLG.read() & 0x00000100u32) != 0x00000100u32
        {
        } /* Wait */
        /*SAFETYMCu32SW 45 D MR:21.1 <APPROVED> "Valid non NULL input parameters are only allowed in this driver" */
        *destbuff = spi.BUF.read() as u16;
        /*SAFETYMCu32SW 567 S MR:17.1,17.4 <APPROVED> "Pointer increment needed" */
        destbuff.ptr = destbuff.ptr.add(1);

        blocksize -= 1;
    }

/* USER CODE BEGIN (15) */
/* USER CODE END */

    spi.FLG.read() & 0xFFu32
}


/* USER CODE BEGIN (16) */
/* USER CODE END */

/* @fn fn spiSendAndGetData(spi: Pointer<spiBASE_t>, dataconfig_t: Pointer<spiDAT1_t >, blocksize: u32, uint16 * srcbuff, uint16 * destbuff)
*   @brief Initiate SPI Transmits and receive Data using Interrupt mode.
*   @param[in] spi           - Spi module base address
*   @param[in] dataconfig_t    - Spi DAT1 register configuration
*   @param[in] blocksize    - number of data
*   @param[in] srcbuff        - Pointer to the source data ( 16 bit).
*   @param[in] destbuff        - Pointer to the destination data ( 16 bit).
*
*   Initiate SPI Transmits and receive Data using Interrupt mode..
*/
/* SourceId : SPI_SourceId_008 */
/* DesignId : SPI_DesignId_010 */
/* Requirements : HL_SR136 */

/* Author's Note: this method takes control of dataconfig_t which is probably not desirable */
pub unsafe fn spiSendAndGetData(spi: Pointer<spiBASE_t>, dataconfig_t: spiDAT1_t, blocksize: u32, srcbuff: Pointer<u16>, destbuff: Pointer<u16>)
{

/* USER CODE BEGIN (17) */
/* USER CODE END */

    let index:usize = if spi.ptr == spiREG1.ptr  {0usize} else {if spi.ptr ==spiREG2.ptr {1usize} else {2usize}};

    g_spiPacket_t[index].tx_length       = blocksize;
    g_spiPacket_t[index].rx_length       = blocksize;
    g_spiPacket_t[index].txdata_ptr      = srcbuff;
    g_spiPacket_t[index].rxdata_ptr      = destbuff;
    g_spiPacket_t[index].g_spiDataFormat = dataconfig_t;
    g_spiPacket_t[index].tx_data_status  = SpiDataStatus_t::SPI_PENDING;
    g_spiPacket_t[index].rx_data_status  = SpiDataStatus_t::SPI_PENDING;

    spi.INT0.write(    spi.INT0.read() | 0x0300u32);

/* USER CODE BEGIN (18) */
/* USER CODE END */
}

/* @fn SpiDataStatus_t SpiTxStatus(spi: Pointer<spiBASE_t>)
*   @brief Get the status of the SPI Transmit data block.
*   @param[in] spi           - Spi module base address
*
*   @return Spi Transmit block data status.
*
*   Get the status of the SPI Transmit data block.
*/
/* SourceId : SPI_SourceId_009 */
/* DesignId : SPI_DesignId_013 */
/* Requirements : HL_SR139 */

/* Author's Note: Should reference be returned here? */
pub unsafe fn SpiTxStatus(spi: Pointer<spiBASE_t>) -> &'static SpiDataStatus_t
{

/* USER CODE BEGIN (19) */
/* USER CODE END */

    let index: usize = if spi.ptr == spiREG1.ptr {0usize} else {if spi.ptr == spiREG2.ptr {1usize} else {2usize}};
    &g_spiPacket_t[index].tx_data_status
}

/* USER CODE BEGIN (20) */
/* USER CODE END */

/* @fn SpiDataStatus_t SpiRxStatus(spi: Pointer<spiBASE_t>)
*   @brief Get the status of the SPI Receive data block.
*   @param[in] spi           - Spi module base address
*
*   @return Spi Receive block data status.
*
*   Get the status of the SPI Receive data block.
*/
/* SourceId : SPI_SourceId_010 */
/* DesignId : SPI_DesignId_014 */
/* Requirements : HL_SR140 */

/* Author's Note: Should reference be returned here? */
pub unsafe fn  SpiRxStatus(spi: Pointer<spiBASE_t>) -> &'static SpiDataStatus_t
{

/* USER CODE BEGIN (21) */
/* USER CODE END */

    let index: usize = if spi.ptr == spiREG1.ptr {0usize} else {if spi.ptr == spiREG2.ptr {1usize} else {2usize}};
    &g_spiPacket_t[index].rx_data_status
}

/* USER CODE BEGIN (22) */
/* USER CODE END */

/* @fn fn spiEnableLoopback(spi: Pointer<spiBASE_t>, loopBackType_t Loopbacktype)
*   @brief Enable Loopback mode for self test
*   @param[in] spi        - spi module base address
*   @param[in] Loopbacktype  - Digital or Analog
*
*   This function enables the Loopback mode for self test.
*/
/* SourceId : SPI_SourceId_011 */
/* DesignId : SPI_DesignId_011 */
/* Requirements : HL_SR137 */
pub unsafe fn spiEnableLoopback(spi: Pointer<spiBASE_t>, Loopbacktype: loopBackType_t)
{
/* USER CODE BEGIN (23) */
/* USER CODE END */

    /* Clear Loopback incase enabled already */
    spi.IOLPKTSTCR.write(0u32);

    /* Enable Loopback either in Analog or Digital Mode */
    spi.IOLPKTSTCR.write(0x00000A00u32
                    | ((Loopbacktype as u32) << 1u32));

/* USER CODE BEGIN (24) */
/* USER CODE END */
}

/* USER CODE BEGIN (25) */
/* USER CODE END */

/* @fn fn spiDisableLoopback(spi: Pointer<spiBASE_t>)
*   @brief Enable Loopback mode for self test
*   @param[in] spi        - spi module base address
*
*   This function disable the Loopback mode.
*/
/* SourceId : SPI_SourceId_012 */
/* DesignId : SPI_DesignId_012 */
/* Requirements : HL_SR138 */
pub unsafe fn spiDisableLoopback(spi: Pointer<spiBASE_t>)
{
/* USER CODE BEGIN (26) */
/* USER CODE END */

    /* Disable Loopback Mode */
    spi.IOLPKTSTCR.write(0x00000500u32);

/* USER CODE BEGIN (27) */
/* USER CODE END */
}

/* USER CODE BEGIN (28) */
/* USER CODE END */

/* @fn spiEnableNotification(spi: Pointer<spiBASE_t>, uint32 flags)
*   @brief Enable interrupts
*   @param[in] spi   - spi module base address
*   @param[in] flags - Interrupts to be enabled, can be ored value of:
*/
/* SourceId : SPI_SourceId_013 */
/* DesignId : SPI_DesignId_003 */
/* Requirements : HL_SR129 */
pub unsafe fn spiEnableNotification(spi: Pointer<spiBASE_t>, flags: u32)
{
/* USER CODE BEGIN (29) */
/* USER CODE END */

    spi.INT0.write((spi.INT0.read() & 0xFFFF0000u32) | flags);

/* USER CODE BEGIN (30) */
/* USER CODE END */
}

/* USER CODE BEGIN (31) */
/* USER CODE END */

/* @fn spiDisableNotification(spi: Pointer<spiBASE_t>, uint32 flags)
*   @brief Enable interrupts
*   @param[in] spi   - spi module base address
*   @param[in] flags - Interrupts to be enabled, can be ored value of:
*/
/* SourceId : SPI_SourceId_014 */
/* DesignId : SPI_DesignId_004 */
/* Requirements : HL_SR130 */
pub unsafe fn spiDisableNotification(spi: Pointer<spiBASE_t>, flags: u32)
{
/* USER CODE BEGIN (32) */
/* USER CODE END */

    spi.INT0.write(spi.INT0.read() & (!(flags)));

/* USER CODE BEGIN (33) */
/* USER CODE END */
}


/* @fn fn spi2GetConfigValue(config_reg: spi_config_reg_t, config_value_type_t _type)
*   @brief Get the initial or current values of the configuration registers
*
*    @param[in] *config_reg: pointer to the struct to which the initial or current
*                           value of the configuration registers need to be stored
*    @param[in] _type:     whether initial or current value of the configuration registers need to be stored
*                        - InitialValue: initial value of the configuration registers will be stored
*                                       in the struct pointed by config_reg
*                        - CurrentValue: initial value of the configuration registers will be stored
*                                       in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type')
*   of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : SPI_SourceId_016 */
/* DesignId : SPI_DesignId_015 */
/* Requirements : HL_SR144 */
fn spi2GetConfigValue(config_reg: &mut spi_config_reg_t, _type: config_value_type_t)
{
	if let config_value_type_t::InitialValue = _type {
        config_reg.CONFIG_GCR1  = SPI2_GCR1_CONFIGVALUE;
        config_reg.CONFIG_INT0  = SPI2_INT0_CONFIGVALUE;
        config_reg.CONFIG_LVL   = SPI2_LVL_CONFIGVALUE;
        config_reg.CONFIG_PC0   = SPI2_PC0_CONFIGVALUE;
        config_reg.CONFIG_PC1   = SPI2_PC1_CONFIGVALUE;
        config_reg.CONFIG_PC6   = SPI2_PC6_CONFIGVALUE;
        config_reg.CONFIG_PC7   = SPI2_PC7_CONFIGVALUE;
        config_reg.CONFIG_PC8   = SPI2_PC8_CONFIGVALUE;
        config_reg.CONFIG_DELAY = SPI2_DELAY_CONFIGVALUE;
        config_reg.CONFIG_FMT0  = SPI2_FMT0_CONFIGVALUE;
        config_reg.CONFIG_FMT1  = SPI2_FMT1_CONFIGVALUE;
        config_reg.CONFIG_FMT2  = SPI2_FMT2_CONFIGVALUE;
        config_reg.CONFIG_FMT3  = SPI2_FMT3_CONFIGVALUE;
    }
    else
    {
    /*SAFETYMCu32SW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        config_reg.CONFIG_GCR1  = spiREG2.GCR1.read();
        config_reg.CONFIG_INT0  = spiREG2.INT0.read();
        config_reg.CONFIG_LVL   = spiREG2.LVL.read();
        config_reg.CONFIG_PC0   = spiREG2.PC0.read();
        config_reg.CONFIG_PC1   = spiREG2.PC1.read();
        config_reg.CONFIG_PC6   = spiREG2.PC6.read();
        config_reg.CONFIG_PC7   = spiREG2.PC7.read();
        config_reg.CONFIG_PC8   = spiREG2.PC8.read();
        config_reg.CONFIG_DELAY = spiREG2.DELAY .read();
        config_reg.CONFIG_FMT0  = spiREG2.FMT0.read();
        config_reg.CONFIG_FMT1  = spiREG2.FMT1.read();
        config_reg.CONFIG_FMT2  = spiREG2.FMT2.read();
        config_reg.CONFIG_FMT3  = spiREG2.FMT3.read();
    }
}

/* @fn fn spi3GetConfigValue(config_reg: spi_config_reg_t, config_value_type_t _type)
*   @brief Get the initial or current values of the configuration registers
*
*    @param[in] *config_reg: pointer to the struct to which the initial or current
*                           value of the configuration registers need to be stored
*    @param[in] _type:     whether initial or current value of the configuration registers need to be stored
*                        - InitialValue: initial value of the configuration registers will be stored
*                                       in the struct pointed by config_reg
*                        - CurrentValue: initial value of the configuration registers will be stored
*                                       in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type')
*   of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : SPI_SourceId_017 */
/* DesignId : SPI_DesignId_015 */
/* Requirements : HL_SR144 */
fn spi3GetConfigValue(config_reg: &mut spi_config_reg_t, _type: config_value_type_t)
{
	if let config_value_type_t::InitialValue = _type {
        config_reg.CONFIG_GCR1  = SPI3_GCR1_CONFIGVALUE;
        config_reg.CONFIG_INT0  = SPI3_INT0_CONFIGVALUE;
        config_reg.CONFIG_LVL   = SPI3_LVL_CONFIGVALUE;
        config_reg.CONFIG_PC0   = SPI3_PC0_CONFIGVALUE;
        config_reg.CONFIG_PC1   = SPI3_PC1_CONFIGVALUE;
        config_reg.CONFIG_PC6   = SPI3_PC6_CONFIGVALUE;
        config_reg.CONFIG_PC7   = SPI3_PC7_CONFIGVALUE;
        config_reg.CONFIG_PC8   = SPI3_PC8_CONFIGVALUE;
        config_reg.CONFIG_DELAY = SPI3_DELAY_CONFIGVALUE;
        config_reg.CONFIG_FMT0  = SPI3_FMT0_CONFIGVALUE;
        config_reg.CONFIG_FMT1  = SPI3_FMT1_CONFIGVALUE;
        config_reg.CONFIG_FMT2  = SPI3_FMT2_CONFIGVALUE;
        config_reg.CONFIG_FMT3  = SPI3_FMT3_CONFIGVALUE;
    }
    else
    {
    /*SAFETYMCu32SW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        config_reg.CONFIG_GCR1  = spiREG3.GCR1.read();
        config_reg.CONFIG_INT0  = spiREG3.INT0.read();
        config_reg.CONFIG_LVL   = spiREG3.LVL.read();
        config_reg.CONFIG_PC0   = spiREG3.PC0.read();
        config_reg.CONFIG_PC1   = spiREG3.PC1.read();
        config_reg.CONFIG_PC6   = spiREG3.PC6.read();
        config_reg.CONFIG_PC7   = spiREG3.PC7.read();
        config_reg.CONFIG_PC8   = spiREG3.PC8.read();
        config_reg.CONFIG_DELAY = spiREG3.DELAY .read();
        config_reg.CONFIG_FMT0  = spiREG3.FMT0.read();
        config_reg.CONFIG_FMT1  = spiREG3.FMT1.read();
        config_reg.CONFIG_FMT2  = spiREG3.FMT2.read();
        config_reg.CONFIG_FMT3  = spiREG3.FMT3.read();
    }
}

