#![allow(non_snake_case)]
use crate::registers::reg_gio::*;
use crate::registers::sys_common::*;

use raw_pointer::Pointer;

pub unsafe fn gioInit() {
    /* bring GIO module out of reset */
    gioREG
        
        .GCR0
        .write(1u32);
    gioREG
		
		.ENACLR
		.write(0xFFu32);
    gioREG
		
		.LVLCLR
		.write(0xFFu32);

    //Author's Note: Author decided the orignal method of code below was too verbose. Someone was being paid by the line of code.
    /* - Port A output values */
    gioPORTA
		
		.DOUT
		.write(0u32);     
    
    /* - Port A direction */
    gioPORTA
		
		.DIR
		.write(0u32);
    
    /* - Port A open drain enable */
    gioPORTA
		
		.PDR
		.write(0u32);

    /* - Port A pullup / pulldown selection */
    gioPORTA
		
		.PSL
		.write(0u32);

    /* - Port A pullup / pulldown enable*/
    gioPORTA
		
		.PULDIS
		.write(0u32);


    /* - interrupt polarity */
    gioREG
		
		.POL
		.write(0u32);

    /* - interrupt level */
    gioREG
		
		.LVLSET
		.write(0u32);

    /* - clear all pending interrupts */
    gioREG
		
		.FLG
		.write(0xFFu32);

    /* - enable interrupts */
    gioREG
        
		.ENASET
		.write(0u32);
}

pub unsafe fn gioSetDirection(port: Pointer<gioPORT_t>, dir: u32) {
    port
		.DIR
		.write(dir);
}

pub unsafe fn gioSetBit(port: Pointer<gioPORT_t>, bit: u32, value: u32) {
    if value != 0u32 {
        port
            .DSET
            .write(1u32 << bit);
    }

    else {
        port
            .DCLR
            .write(1u32 << bit);
    }
}

pub unsafe fn gioSetPort(port: Pointer<gioPORT_t>, value: u32) {
    port
		.DOUT
		.write(value);
}

pub unsafe fn gioGetBit(port: Pointer<gioPORT_t>, bit: u32) -> u32 {
    (port.DIN.read() >> bit) & 1u32
}


pub unsafe fn gioGetPort(port: Pointer<gioPORT_t>) ->u32 {
    port.DIN.read()
}


pub unsafe fn gioToggleBit(port: Pointer<gioPORT_t>, bit: u32) {

    if (port.DIN.read() & (1u32 << bit) as u32) != 0u32 {
        port
		.DCLR
		.write(1u32 << bit);
    }
    else {
        port
		.DSET
		.write(1u32 << bit);
    }
}

pub unsafe fn gioEnableNotification(port: Pointer<gioPORT_t>, bit: u32) {

	if port.ptr == gioPORTA.ptr {
		gioREG
		.ENASET
		.write(1u32 << bit);
	}
	else {
		/* Empty */
	}
}



pub unsafe fn gioDisableNotification(port: Pointer<gioPORT_t>, bit: u32) {

	if port.ptr == gioPORTA.ptr {
		gioREG
		.ENACLR
		.write(1u32 << bit);
	}
	else {
		/* Empty */
	}
}

/* Author's Note: Could this function be made safe? */
/*config_reg initially was a pointer in C, attempting to make a reference */
pub fn gioGetConfigValue(config_reg: &mut gio_config_reg_t, _type: config_value_type_t) {
	if let config_value_type_t::InitialValue = _type {
	    config_reg.CONFIG_INTDET    = GIO_INTDET_CONFIGVALUE;
        config_reg.CONFIG_POL       = GIO_POL_CONFIGVALUE;
        config_reg.CONFIG_INTENASET = GIO_INTENASET_CONFIGVALUE;
        config_reg.CONFIG_LVLSET    = GIO_LVLSET_CONFIGVALUE;

	    config_reg.CONFIG_PORTADIR    = GIO_PORTADIR_CONFIGVALUE;
	    config_reg.CONFIG_PORTAPDR    = GIO_PORTAPDR_CONFIGVALUE;
	    config_reg.CONFIG_PORTAPSL    = GIO_PORTAPSL_CONFIGVALUE;
	    config_reg.CONFIG_PORTAPULDIS = GIO_PORTAPULDIS_CONFIGVALUE;

	}
	else {
	/*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        config_reg.CONFIG_INTDET    = gioREG.INTDET.read();
        config_reg.CONFIG_POL       = gioREG.POL.read();
        config_reg.CONFIG_INTENASET = gioREG.ENASET.read();
        config_reg.CONFIG_LVLSET    = gioREG.LVLSET.read();

        config_reg.CONFIG_PORTADIR    = gioPORTA.DIR.read();
        config_reg.CONFIG_PORTAPDR    = gioPORTA.PDR.read();
        config_reg.CONFIG_PORTAPSL    = gioPORTA.PSL.read();
        config_reg.CONFIG_PORTAPULDIS = gioPORTA.PULDIS.read();
	}
}
