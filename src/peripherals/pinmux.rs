#![allow(non_snake_case)]
use crate::registers::reg_pinmux::*;
use crate::registers::sys_common::*;
pub unsafe fn muxInit(){


    /* Enable Pin Muxing */
    kickerReg.KICKER0.write(0x83E70B13u32);
    kickerReg.KICKER1.write(0x95A4F1E0u32);


    pinMuxReg.PINMMR0.write(PINMUX_PIN_1_GIOA_0);
    
    pinMuxReg.PINMMR1.write(PINMUX_PIN_2_GIOA_1 | PINMUX_PIN_5_GIOA_2 | PINMUX_PIN_8_GIOA_3 | PINMUX_PIN_9_GIOA_4);
    
    pinMuxReg.PINMMR2.write(PINMUX_PIN_10_GIOA_5 | PINMUX_PIN_12_GIOA_6 | PINMUX_PIN_18_GIOA_7);
    
    pinMuxReg.PINMMR3.write(PINMUX_PIN_27_MIBSPI1nCS_2 | PINMUX_PIN_36_SPI3CLK | PINMUX_PIN_37_SPI3nENA);
    
    pinMuxReg.PINMMR4.write(PINMUX_PIN_38_SPI3nCS_0 | PINMUX_PIN_39_MIBSPI1nCS_3 | PINMUX_PIN_58_ADEVT);
    
    pinMuxReg.PINMMR5.write(PINMUX_PIN_68_MIBSPI1nENA);
    
    pinMuxReg.PINMMR6.write(PINMUX_PIN_93_MIBSPI1nCS_1);
    
  
    
    /* Disable Pin Muxing */
    kickerReg.KICKER0.write(0x00000000u32);
    kickerReg.KICKER1.write(0x00000000u32);
 
}

/* @fn void pinmuxGetConfigValue(pinmux_config_reg_t *config_reg, config_value_type_t type)
*   @brief Get the initial or current values of the configuration registers
*
*   @param[in] *config_reg: pointer to the struct to which the initial or current 
*                           value of the configuration registers need to be stored
*   @param[in] type:    whether initial or current value of the configuration registers need to be stored
*                       - InitialValue: initial value of the configuration registers will be stored 
*                                       in the struct pointed by config_reg
*                       - CurrentValue: initial value of the configuration registers will be stored 
*                                       in the struct pointed by config_reg
*
*   This function will copy the initial or current value (depending on the parameter 'type') 
*   of the configuration registers to the struct pointed by config_reg
*
*/
/* SourceId : PINMUX_SourceId_002 */
/* DesignId : PINMUX_DesignId_002 */
/* Requirements : HL_SR328 */
pub fn pinmuxGetConfigValue(config_reg: &mut pinmux_config_reg_t, _type: config_value_type_t)
{
    if let config_value_type_t::InitialValue = _type {
	/* Do not pass Initial Value as parameter */
    }
    else
    {
    /*SAFETYMCUSW 134 S MR:12.2 <APPROVED> "LDRA Tool issue" */
        config_reg.CONFIG_PINMMR0  = pinMuxReg.PINMMR0.read();
        config_reg.CONFIG_PINMMR1  = pinMuxReg.PINMMR1.read();
        config_reg.CONFIG_PINMMR2  = pinMuxReg.PINMMR2.read();
        config_reg.CONFIG_PINMMR3  = pinMuxReg.PINMMR3.read();
        config_reg.CONFIG_PINMMR4  = pinMuxReg.PINMMR4.read();
        config_reg.CONFIG_PINMMR5  = pinMuxReg.PINMMR5.read();
        config_reg.CONFIG_PINMMR6  = pinMuxReg.PINMMR6.read();
    }
}
