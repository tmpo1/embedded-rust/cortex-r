#![allow(non_snake_case, non_upper_case_globals, non_camel_case_types)]

use raw_pointer::Pointer;
use volatile_register::*;

/* IOMM Revision and Boot Register */
// #define REVISION_REG		(*(volatile uint32 *)0xFFFFEA00U)
// #define ENDIAN_REG		(*(volatile uint32 *)0xFFFFEA20U)

//Should be dereferenced? 
pub const REVISION_REG: Pointer<RO<u32>> = Pointer::<RO<u32>> {
    ptr: 0xFFFFEA00u32 as *mut RO<u32>
};

pub const ENDIAN_REG: Pointer<RW<u32>> = Pointer::<RW<u32>> {
    ptr: 0xFFFFEA20u32 as *mut RW<u32>
};


/* IOMM Error and Fault Registers */
/* @struct iommErrFault
*   @brief IOMM Error and Fault Register Definition
*
*   This structure is used to access the IOMM Error and Fault registers.
*/
#[repr(C)]
pub struct iommErrFault_t
{
    pub ERR_RAW_STATUS_REG: RW<u32>,          /* Error Raw Status / Set Register */
    pub ERR_ENABLED_STATUS_REG: RW<u32>,      /* Error Enabled Status / Clear Register */
    pub ERR_ENABLE_REG: RW<u32>,              /* Error Signaling Enable Register */
    pub ERR_ENABLE_CLR_REG: RW<u32>,          /* Error Signaling Enable Clear Register */
	    rsvd: RW<u32>,                       /* Reserved */
    pub FAULT_ADDRESS_REG: RW<u32>,           /* Fault Address Register */
    pub FAULT_STATUS_REG: RO<u32>,            /* Fault Status Register */
    pub FAULT_CLEAR_REG: RW<u32>,             /* Fault Clear Register */	
}
/* Pinmux Register Frame Definition */
/* @struct pinMuxKicker
*   @brief Pin Muxing Kicker Register Definition
*
*   This structure is used to access the Pin Muxing Kicker registers.
*/
#[repr(C)]
pub struct pinMuxKICKER_t
{
    pub KICKER0: RW<u32>,       /* kicker 0 register */
    pub KICKER1: RW<u32>,       /* kicker 1 register */
}

/* @struct pinMuxBase
*   @brief PINMUX Register Definition
*
*   This structure is used to access the PINMUX module registers.
*/
/* @typedef pinMuxBASE_t
*   @brief PINMUX Register Frame Type Definition
*
*   This type is used to access the PINMUX Registers.
*/
#[repr(C)]
pub struct pinMuxBASE_t
{
    pub PINMMR0: RW<u32>,		/*< 0xEB10 Pin Mux 0 register*/
    pub PINMMR1: RW<u32>,		/*< 0xEB14 Pin Mux 1 register*/
    pub PINMMR2: RW<u32>,		/*< 0xEB18 Pin Mux 2 register*/
    pub PINMMR3: RW<u32>,		/*< 0xEB1C Pin Mux 3 register*/
    pub PINMMR4: RW<u32>,		/*< 0xEB20 Pin Mux 4 register*/
    pub PINMMR5: RW<u32>,		/*< 0xEB24 Pin Mux 5 register*/
    pub PINMMR6: RW<u32>,		/*< 0xEB28 Pin Mux 6 register*/
    pub PINMMR7: RW<u32>,		/*< 0xEB2C Pin Mux 7 register*/
}

/* @def iommErrFaultReg
*       @brief IOMM Error Fault Register Frame Pointer
*
*               This pointer is used to control IOMM Error and Fault across the device.
*/      
// #define iommErrFaultReg ((iommErrFault_t *) 0xFFFFEAE0u32)
pub const iommErrFaultReg: Pointer<iommErrFault_t> = Pointer::<iommErrFault_t> {
    ptr: 0xFFFFEAE0u32 as *mut iommErrFault_t
};
        
/* @def kickerReg
*	@brief Pin Muxing Kicker Register Frame Pointer
*
*		This pointer is used to enable and disable muxing across the device.
*/	
// #define kickerReg ((pinMuxKICKER_t *) 0xFFFFEA38u32)
pub const kickerReg: Pointer<pinMuxKICKER_t> = Pointer::<pinMuxKICKER_t> {
    ptr: 0xFFFFEA38u32 as *mut pinMuxKICKER_t
};

/* @def pinMuxReg
*	@brief Pin Muxing Control Register Frame Pointer
*
*		This pointer is used to set the muxing registers across the device.
*/	
// #define pinMuxReg ((pinMuxBASE_t *) 0xFFFFEB10u32)
pub const pinMuxReg: Pointer<pinMuxBASE_t> = Pointer::<pinMuxBASE_t> {
    ptr: 0xFFFFEB10u32 as *mut pinMuxBASE_t
};

/* Author's Note: What follows is from pinmux.h */

pub const PINMUX_PIN_1_SHIFT         :u32 = 8u32;
pub const PINMUX_PIN_2_SHIFT         :u32 = 0u32;
pub const PINMUX_PIN_5_SHIFT         :u32 = 8u32;
pub const PINMUX_PIN_8_SHIFT         :u32 = 16u32;
pub const PINMUX_PIN_9_SHIFT         :u32 = 24u32;
pub const PINMUX_PIN_10_SHIFT        :u32 = 0u32;
pub const PINMUX_PIN_12_SHIFT        :u32 = 8u32;
pub const PINMUX_PIN_18_SHIFT        :u32 = 16u32;
pub const PINMUX_PIN_27_SHIFT        :u32 = 0u32;
pub const PINMUX_PIN_36_SHIFT        :u32 = 16u32;
pub const PINMUX_PIN_37_SHIFT        :u32 = 24u32;
pub const PINMUX_PIN_38_SHIFT        :u32 = 0u32;
pub const PINMUX_PIN_39_SHIFT        :u32 = 8u32;
pub const PINMUX_PIN_58_SHIFT        :u32 = 16u32;
pub const PINMUX_PIN_68_SHIFT        :u32 = 8u32;
pub const PINMUX_PIN_93_SHIFT        :u32 = 8u32;

pub const PINMUX_PIN_1_MASK          :u32 = !((0xFFu32 << PINMUX_PIN_1_SHIFT) as u32);
pub const PINMUX_PIN_2_MASK          :u32 = !((0xFFu32 << PINMUX_PIN_2_SHIFT) as u32);
pub const PINMUX_PIN_5_MASK          :u32 = !((0xFFu32 << PINMUX_PIN_5_SHIFT) as u32);
pub const PINMUX_PIN_8_MASK          :u32 = !((0xFFu32 << PINMUX_PIN_8_SHIFT) as u32);
pub const PINMUX_PIN_9_MASK          :u32 = !((0xFFu32 << PINMUX_PIN_9_SHIFT) as u32);
pub const PINMUX_PIN_10_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_10_SHIFT) as u32);
pub const PINMUX_PIN_12_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_12_SHIFT) as u32);
pub const PINMUX_PIN_18_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_18_SHIFT) as u32);
pub const PINMUX_PIN_27_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_27_SHIFT) as u32);
pub const PINMUX_PIN_36_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_36_SHIFT) as u32);
pub const PINMUX_PIN_37_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_37_SHIFT) as u32);
pub const PINMUX_PIN_38_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_38_SHIFT) as u32);
pub const PINMUX_PIN_39_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_39_SHIFT) as u32);
pub const PINMUX_PIN_58_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_58_SHIFT) as u32);
pub const PINMUX_PIN_68_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_68_SHIFT) as u32);
pub const PINMUX_PIN_93_MASK         :u32 = !((0xFFu32 << PINMUX_PIN_93_SHIFT) as u32);



pub const PINMUX_PIN_1_GIOA_0        :u32 = (0x1u32 <<  PINMUX_PIN_1_SHIFT) as u32;
pub const PINMUX_PIN_1_SPI3nCS_3     :u32 = (0x2u32 <<  PINMUX_PIN_1_SHIFT) as u32;

pub const PINMUX_PIN_2_GIOA_1        :u32 = (0x1u32 <<  PINMUX_PIN_2_SHIFT) as u32;
pub const PINMUX_PIN_2_SPI3nCS_2     :u32 = (0x2u32 <<  PINMUX_PIN_2_SHIFT) as u32;

pub const PINMUX_PIN_5_GIOA_2        :u32 = (0x1u32 <<  PINMUX_PIN_5_SHIFT) as u32;
pub const PINMUX_PIN_5_SPI3nCS_1     :u32 = (0x2u32 <<  PINMUX_PIN_5_SHIFT) as u32;

pub const PINMUX_PIN_8_GIOA_3        :u32 = (0x1u32 <<  PINMUX_PIN_8_SHIFT) as u32;
pub const PINMUX_PIN_8_SPI2nCS_3     :u32 = (0x2u32 <<  PINMUX_PIN_8_SHIFT) as u32;

pub const PINMUX_PIN_9_GIOA_4        :u32 = (0x1u32 <<  PINMUX_PIN_9_SHIFT) as u32;
pub const PINMUX_PIN_9_SPI2nCS_2     :u32 = (0x2u32 <<  PINMUX_PIN_9_SHIFT) as u32;

pub const PINMUX_PIN_10_GIOA_5       :u32 = (0x1u32 <<  PINMUX_PIN_10_SHIFT) as u32;
pub const PINMUX_PIN_10_EXTCLKIN     :u32 = (0x2u32 <<  PINMUX_PIN_10_SHIFT) as u32;

pub const PINMUX_PIN_12_GIOA_6       :u32 = (0x1u32 <<  PINMUX_PIN_12_SHIFT) as u32;
pub const PINMUX_PIN_12_SPI2nCS_1    :u32 = (0x2u32 <<  PINMUX_PIN_12_SHIFT) as u32;
pub const PINMUX_PIN_12_HET_31       :u32 = (0x4u32 <<  PINMUX_PIN_12_SHIFT) as u32;

pub const PINMUX_PIN_18_GIOA_7       :u32 = (0x1u32 <<  PINMUX_PIN_18_SHIFT) as u32;
pub const PINMUX_PIN_18_HET_29       :u32 = (0x2u32 <<  PINMUX_PIN_18_SHIFT) as u32;

pub const PINMUX_PIN_27_MIBSPI1nCS_2 :u32 = (0x1u32 <<  PINMUX_PIN_27_SHIFT) as u32;
pub const PINMUX_PIN_27_HET_20       :u32 = (0x2u32 <<  PINMUX_PIN_27_SHIFT) as u32;
pub const PINMUX_PIN_27_HET_19       :u32 = (0x4u32 <<  PINMUX_PIN_27_SHIFT) as u32;

pub const PINMUX_PIN_36_SPI3CLK      :u32 = (0x1u32 <<  PINMUX_PIN_36_SHIFT) as u32;
pub const PINMUX_PIN_36_EQEPA        :u32 = (0x2u32 <<  PINMUX_PIN_36_SHIFT) as u32;

pub const PINMUX_PIN_37_SPI3nENA     :u32 = (0x1u32 <<  PINMUX_PIN_37_SHIFT) as u32;
pub const PINMUX_PIN_37_EQEPB        :u32 = (0x2u32 <<  PINMUX_PIN_37_SHIFT) as u32;

pub const PINMUX_PIN_38_SPI3nCS_0    :u32 = (0x1u32 <<  PINMUX_PIN_38_SHIFT) as u32;
pub const PINMUX_PIN_38_EQEPI        :u32 = (0x2u32 <<  PINMUX_PIN_38_SHIFT) as u32;

pub const PINMUX_PIN_39_MIBSPI1nCS_3 :u32 = (0x1u32 <<  PINMUX_PIN_39_SHIFT) as u32;
pub const PINMUX_PIN_39_HET_26       :u32 = (0x2u32 <<  PINMUX_PIN_39_SHIFT) as u32;

pub const PINMUX_PIN_58_ADEVT        :u32 = (0x1u32 <<  PINMUX_PIN_58_SHIFT) as u32;
pub const PINMUX_PIN_58_HET_28       :u32 = (0x2u32 <<  PINMUX_PIN_58_SHIFT) as u32;

pub const PINMUX_PIN_68_MIBSPI1nENA  :u32 = (0x1u32 <<  PINMUX_PIN_68_SHIFT) as u32;
pub const PINMUX_PIN_68_HET_23       :u32 = (0x2u32 <<  PINMUX_PIN_68_SHIFT) as u32;
pub const PINMUX_PIN_68_HET_30       :u32 = (0x4u32 <<  PINMUX_PIN_68_SHIFT) as u32;

pub const PINMUX_PIN_93_MIBSPI1nCS_1 :u32 = (0x1u32 <<  PINMUX_PIN_93_SHIFT) as u32;
pub const PINMUX_PIN_93_EQEPS        :u32 = (0x2u32 <<  PINMUX_PIN_93_SHIFT) as u32;
pub const PINMUX_PIN_93_HET_17       :u32 = (0x4u32 <<  PINMUX_PIN_93_SHIFT) as u32;

pub struct pinmux_config_reg_t 
{
    pub CONFIG_PINMMR0: u32,
    pub CONFIG_PINMMR1: u32,
    pub CONFIG_PINMMR2: u32,
    pub CONFIG_PINMMR3: u32,
    pub CONFIG_PINMMR4: u32,
    pub CONFIG_PINMMR5: u32,
    pub CONFIG_PINMMR6: u32,
}
