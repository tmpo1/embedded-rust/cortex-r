#![allow(non_snake_case, non_upper_case_globals, non_camel_case_types, dead_code)]
use volatile_register::*;
use raw_pointer::*;
/* @file reg_vim.h
*   @brief VIM Register Layer Header File
*   @date 11-Dec-2018
*   @version 04.07.01
*   
*   This file contains:
*   - Definitions
*   - Types
*   .
*   which are relevant for the System driver.
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* Vim Register Frame Definition */
/* @struct vimBase
*   @brief Vim Register Frame Definition
*
*   This type is used to access the Vim Registers.
*/
/* @typedef vimBASE_t
*   @brief VIM Register Frame Type Definition
*
*   This type is used to access the VIM Registers.
*/

/* Author's Note: Those not pub are reserved/not listed in documentation */
pub struct vimBASE_t
{
    pub      IRQINDEX: RO<u32>,         /* 0x0000       */
    pub      FIQINDEX: RO<u32>,         /* 0x0004       */	
                rsvd1: RW<u32>,          /* 0x0008       */
                rsvd2: RW<u32>,          /* 0x000C       */
    pub      FIRQPR0: RW<u32>,          /* 0x0010       */
    pub      FIRQPR1: RW<u32>,          /* 0x0014       */
    pub      FIRQPR2: RW<u32>,          /* 0x0018       */
    pub         FIRQPR3: RW<u32>,          /* 0x001C       */
    pub      INTREQ0: RW<u32>,          /* 0x0020       */
    pub      INTREQ1: RW<u32>,          /* 0x0024       */
    pub      INTREQ2: RW<u32>,          /* 0x0028       */
             INTREQ3: RW<u32>,          /* 0x002C       */
    pub      REQMASKSET0: RW<u32>,      /* 0x0030       */
    pub      REQMASKSET1: RW<u32>,      /* 0x0034       */
    pub      REQMASKSET2: RW<u32>,      /* 0x0038       */
    pub      REQMASKSET3: RW<u32>,      /* 0x003C       */
    pub      REQMASKCLR0: RW<u32>,      /* 0x0040       */
    pub      REQMASKCLR1: RW<u32>,      /* 0x0044       */
    pub      REQMASKCLR2: RW<u32>,      /* 0x0048       */
    pub      REQMASKCLR3: RW<u32>,      /* 0x004C       */
    pub      WAKEMASKSET0: RW<u32>,     /* 0x0050       */
    pub      WAKEMASKSET1: RW<u32>,     /* 0x0054       */
    pub      WAKEMASKSET2: RW<u32>,     /* 0x0058       */
    pub      WAKEMASKSET3: RW<u32>,     /* 0x005C       */
    pub      WAKEMASKCLR0: RW<u32>,     /* 0x0060       */
    pub      WAKEMASKCLR1: RW<u32>,     /* 0x0064       */
    pub      WAKEMASKCLR2: RW<u32>,     /* 0x0068       */
             WAKEMASKCLR3: RW<u32>,     /* 0x006C       */
    pub      IRQVECREG: RO<u32>,        /* 0x0070       */
    pub      FIQVECREG: RO<u32>,        /* 0x0074       */
    pub      CAPEVT: RW<u32>,           /* 0x0078       */	
                rsvd3: RW<u32>,          /* 0x007C       */
    pub      CHANCTRL: [RW<u32>; 24],    /* 0x0080-0x0DC */
}

pub const vimREG: Pointer<vimBASE_t> = Pointer::<vimBASE_t> {
	ptr: 0xFFFFFE00u32 as *mut vimBASE_t
};

/* Author's Note: The below is from sys_vim.h */

/* VIM Type Definitions */

/* @typedef t_isrFuncPTR
*   @brief ISR Function Pointer Type Definition
*
*   This type is used to access the ISR handler.
*/
// typedef void (*t_isrFuncPTR)(void);
pub type t_isrFuncPTR = fn();

/* @enum systemInterrupt
*   @brief Alias names for clock sources
*
*   This enumeration is used to provide alias names for the clock sources:
*     - IRQ
*     - FIQ
*/
pub enum systemInterrupt_t
{
    SYS_IRQ = 0, /*< Alias for IRQ interrupt */
    SYS_FIQ = 1  /*< Alias for FIQ interrupt */
}

/* USER CODE BEGIN (1) */
/* USER CODE END */


/* VIM General Configuration */

pub const VIM_CHANNELS:u32 = 96u32;

/* USER CODE BEGIN (2) */
/* USER CODE END */

/* Interrupt Handlers */

/* Author's Note: No rust equivalent, not needed externs 
    void esmHighInterrupt(void) -> defined in esm.c (sends esmGroup'X'Notifications where 'X' is number)
    void phantomInterrupt(void) -> defined in sys_phantom.c (empty -> user defined)
*/
// extern void esmHighInterrupt(void);
// extern void phantomInterrupt(void);

/* USER CODE BEGIN (3) */
/* USER CODE END */

pub const VIM_PARFLG:Pointer<RW<u32>> = Pointer::<RW<u32>> {
	ptr:0xFFFFFDECu32 as *mut RW<u32>
};
pub const VIM_PARCTL:Pointer<RW<u32>> = Pointer::<RW<u32>> {
	ptr:0xFFFFFDF0u32 as *mut RW<u32>
};
pub const VIM_ADDERR:Pointer<RW<u32>> = Pointer::<RW<u32>> {
	ptr:0xFFFFFDF4u32 as *mut RW<u32>
};
pub const VIM_FBPARERR:Pointer<RW<u32>> = Pointer::<RW<u32>> {
	ptr:0xFFFFFDF8u32 as *mut RW<u32>
};

pub const VIMRAMPARLOC:Pointer<RW<u32>> = Pointer::<RW<u32>> {
	ptr:0xFFF82400u32 as *mut RW<u32>
};
pub const VIMRAMLOC:Pointer<RW<u32>> = Pointer::<RW<u32>> {
	ptr:0xFFF82000u32 as *mut RW<u32>
};

/* Configuration registers */
pub struct vim_config_reg_t
{
    pub CONFIG_FIRQPR0: u32,
    pub CONFIG_FIRQPR1: u32,
    pub CONFIG_FIRQPR2: u32,
    pub CONFIG_FIRQPR3: u32,
    pub CONFIG_REQMASKSET0: u32,
    pub CONFIG_REQMASKSET1: u32,
    pub CONFIG_REQMASKSET2: u32,
    pub CONFIG_REQMASKSET3: u32,
    pub CONFIG_WAKEMASKSET0: u32,
    pub CONFIG_WAKEMASKSET1: u32,
    pub CONFIG_WAKEMASKSET2: u32,
    pub CONFIG_WAKEMASKSET3: u32,
    pub CONFIG_CAPEVT: u32,
    pub CONFIG_CHANCTRL: [u32; 24],
}

/* Configuration registers initial value */
pub const VIM_FIRQPR0_CONFIGVALUE: u32 = ((systemInterrupt_t::SYS_FIQ as u32) << 0u32 as u32)
                                |((systemInterrupt_t::SYS_FIQ as u32) << 1u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 2u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 3u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 4u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 5u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 6u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 7u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 8u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 9u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 10u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 11u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 12u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 13u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 14u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 15u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 16u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 17u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 18u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 19u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 20u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 21u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 22u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 23u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 24u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 25u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 26u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 27u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 28u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 29u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 30u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 31u32) as u32;
                    
pub const VIM_FIRQPR1_CONFIGVALUE: u32 = ((systemInterrupt_t::SYS_IRQ as u32) << 0u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 1u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 2u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 3u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 4u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 5u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 6u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 7u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 8u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 9u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 10u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 11u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 12u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 13u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 14u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 15u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 16u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 17u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 18u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 19u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 20u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 21u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 22u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 23u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 24u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 25u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 26u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 27u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 28u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 29u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 30u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 31u32) as u32;

pub const VIM_FIRQPR2_CONFIGVALUE: u32 = ((systemInterrupt_t::SYS_IRQ as u32) << 0u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 1u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 2u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 3u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 4u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 5u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 6u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 7u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 8u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 9u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 10u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 11u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 12u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 13u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 14u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 15u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 16u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 17u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 18u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 19u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 20u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 21u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 22u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 23u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 24u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 25u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 26u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 27u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 28u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 29u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 30u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 31u32) as u32;
                    
pub const VIM_FIRQPR3_CONFIGVALUE: u32 = ((systemInterrupt_t::SYS_IRQ as u32) << 0u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 1u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 2u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 3u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 4u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 5u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 6u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 7u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 8u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 9u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 10u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 11u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 12u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 13u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 14u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 15u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 16u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 17u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 18u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 19u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 20u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 21u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 22u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 23u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 24u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 25u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 26u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 27u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 28u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 29u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 30u32 as u32)
                                |((systemInterrupt_t::SYS_IRQ as u32) << 31u32) as u32;
                    
pub const VIM_REQMASKSET0_CONFIGVALUE: u32 = ((1u32 << 0u32) as u32)
                                    |((1u32 << 1u32) as u32)
                                    |((0u32 << 2u32) as u32)
                                    |((0u32 << 3u32) as u32)
                                    |((0u32 << 4u32) as u32)
                                    |((0u32 << 5u32) as u32)
                                    |((0u32 << 6u32) as u32)
                                    |((0u32 << 7u32) as u32)
                                    |((0u32 << 8u32) as u32)
                                    |((0u32 << 9u32) as u32)
                                    |((0u32 << 10u32) as u32)
                                    |((0u32 << 11u32) as u32)
                                    |((0u32 << 12u32) as u32)
                                    |((0u32 << 13u32) as u32)
                                    |((0u32 << 14u32) as u32)
                                    |((0u32 << 15u32) as u32)
                                    |((0u32 << 16u32) as u32)
                                    |((0u32 << 17u32) as u32)
                                    |((0u32 << 18u32) as u32)
                                    |((0u32 << 19u32) as u32)
                                    |((0u32 << 20u32) as u32)
                                    |((0u32 << 21u32) as u32)
                                    |((0u32 << 22u32) as u32)
                                    |((0u32 << 23u32) as u32)
                                    |((0u32 << 24u32) as u32)
                                    |((0u32 << 25u32) as u32)
                                    |((0u32 << 26u32) as u32)
                                    |((0u32 << 27u32) as u32)
                                    |((0u32 << 28u32) as u32)
                                    |((0u32 << 29u32) as u32)
                                    |((0u32 << 30u32) as u32)
                                    |((0u32 << 31u32)) as u32;
                        
pub const VIM_REQMASKSET1_CONFIGVALUE: u32 = ((0u32 << 0u32) as u32)
                                    |((0u32 << 1u32) as u32)
                                    |((0u32 << 2u32) as u32)
                                    |((0u32 << 3u32) as u32)
                                    |((0u32 << 4u32) as u32)
                                    |((0u32 << 5u32) as u32)
                                    |((0u32 << 6u32) as u32)
                                    |((0u32 << 7u32) as u32)
                                    |((0u32 << 8u32) as u32)
                                    |((0u32 << 9u32) as u32)
                                    |((0u32 << 10u32) as u32)
                                    |((0u32 << 11u32) as u32)
                                    |((0u32 << 12u32) as u32)
                                    |((0u32 << 13u32) as u32)
                                    |((0u32 << 14u32) as u32)
                                    |((0u32 << 15u32) as u32)
                                    |((0u32 << 16u32) as u32)
                                    |((0u32 << 17u32) as u32)
                                    |((0u32 << 18u32) as u32)
                                    |((0u32 << 19u32) as u32)
                                    |((0u32 << 20u32) as u32)
                                    |((0u32 << 21u32) as u32)
                                    |((0u32 << 22u32) as u32)
                                    |((0u32 << 23u32) as u32)
                                    |((0u32 << 24u32) as u32)
                                    |((0u32 << 25u32) as u32)
                                    |((0u32 << 26u32) as u32)
                                    |((0u32 << 27u32) as u32)
                                    |((0u32 << 28u32) as u32)
                                    |((0u32 << 29u32) as u32)
                                    |((0u32 << 30u32) as u32)
                                    |((0u32 << 31u32)) as u32;
                        
pub const VIM_REQMASKSET2_CONFIGVALUE: u32 = ((0u32 << 0u32) as u32)
                                    |((0u32 << 1u32) as u32)
                                    |((0u32 << 2u32) as u32)
                                    |((0u32 << 3u32) as u32)
                                    |((0u32 << 4u32) as u32)
                                    |((0u32 << 5u32) as u32)
                                    |((0u32 << 6u32) as u32)
                                    |((0u32 << 7u32) as u32)
                                    |((0u32 << 8u32) as u32)
                                    |((0u32 << 9u32) as u32)
                                    |((0u32 << 10u32) as u32)
                                    |((0u32 << 11u32) as u32)
                                    |((0u32 << 12u32) as u32)
                                    |((0u32 << 13u32) as u32)
                                    |((0u32 << 14u32) as u32)
                                    |((0u32 << 15u32) as u32)
                                    |((0u32 << 16u32) as u32)
                                    |((0u32 << 17u32) as u32)
                                    |((0u32 << 18u32) as u32)
                                    |((0u32 << 19u32) as u32)
                                    |((0u32 << 20u32) as u32)
                                    |((0u32 << 21u32) as u32)
                                    |((0u32 << 22u32) as u32)
                                    |((0u32 << 23u32) as u32)
                                    |((0u32 << 24u32) as u32)
                                    |((0u32 << 25u32) as u32)
                                    |((0u32 << 26u32) as u32)
                                    |((0u32 << 27u32) as u32)
                                    |((0u32 << 28u32) as u32)
                                    |((0u32 << 29u32) as u32)
                                    |((0u32 << 30u32) as u32)
                                    |((0u32 << 31u32)) as u32;
                        
pub const VIM_REQMASKSET3_CONFIGVALUE: u32 = ((0u32 << 0u32) as u32)
                                    |((0u32 << 1u32) as u32)
                                    |((0u32 << 2u32) as u32)
                                    |((0u32 << 3u32) as u32)
                                    |((0u32 << 4u32) as u32)
                                    |((0u32 << 5u32) as u32)
                                    |((0u32 << 6u32) as u32)
                                    |((0u32 << 7u32) as u32)
                                    |((0u32 << 8u32) as u32)
                                    |((0u32 << 9u32) as u32)
                                    |((0u32 << 10u32) as u32)
                                    |((0u32 << 11u32) as u32)
                                    |((0u32 << 12u32) as u32)
                                    |((0u32 << 13u32) as u32)
                                    |((0u32 << 14u32) as u32)
                                    |((0u32 << 15u32) as u32)
                                    |((0u32 << 16u32) as u32)
                                    |((0u32 << 17u32) as u32)
                                    |((0u32 << 18u32) as u32)
                                    |((0u32 << 19u32) as u32)
                                    |((0u32 << 20u32) as u32)
                                    |((0u32 << 21u32) as u32)
                                    |((0u32 << 22u32) as u32)
                                    |((0u32 << 23u32) as u32)
                                    |((0u32 << 24u32) as u32)
                                    |((0u32 << 25u32) as u32)
                                    |((0u32 << 26u32) as u32)
                                    |((0u32 << 27u32) as u32)
                                    |((0u32 << 28u32) as u32)
                                    |((0u32 << 29u32) as u32)
                                    |((0u32 << 30u32) as u32)
                                    |((0u32 << 31u32)) as u32;
                        
pub const VIM_WAKEMASKSET0_CONFIGVALUE   : u32 = 0xFFFFFFFFu32;
pub const VIM_WAKEMASKSET1_CONFIGVALUE   : u32 = 0xFFFFFFFFu32;
pub const VIM_WAKEMASKSET2_CONFIGVALUE   : u32 = 0xFFFFFFFFu32;
pub const VIM_WAKEMASKSET3_CONFIGVALUE   : u32 = 0u32;
pub const VIM_CAPEVT_CONFIGVALUE         : u32 =  ((0u32 << 0u32) as u32) | ((0u32 << 16u32) as u32);

pub const VIM_CHANCTRL0_CONFIGVALUE      : u32 = 0x00010203u32;
pub const VIM_CHANCTRL1_CONFIGVALUE      : u32 = 0x04050607u32;
pub const VIM_CHANCTRL2_CONFIGVALUE      : u32 = 0x08090A0Bu32;
pub const VIM_CHANCTRL3_CONFIGVALUE      : u32 = 0x0C0D0E0Fu32;
pub const VIM_CHANCTRL4_CONFIGVALUE      : u32 = 0x10111213u32;
pub const VIM_CHANCTRL5_CONFIGVALUE      : u32 = 0x14151617u32;
pub const VIM_CHANCTRL6_CONFIGVALUE      : u32 = 0x18191A1Bu32;
pub const VIM_CHANCTRL7_CONFIGVALUE      : u32 = 0x1C1D1E1Fu32;
pub const VIM_CHANCTRL8_CONFIGVALUE      : u32 = 0x20212223u32;
pub const VIM_CHANCTRL9_CONFIGVALUE      : u32 = 0x24252627u32;
pub const VIM_CHANCTRL10_CONFIGVALUE     : u32 = 0x28292A2Bu32;
pub const VIM_CHANCTRL11_CONFIGVALUE     : u32 = 0x2C2D2E2Fu32;
pub const VIM_CHANCTRL12_CONFIGVALUE     : u32 = 0x30313233u32;
pub const VIM_CHANCTRL13_CONFIGVALUE     : u32 = 0x34353637u32;
pub const VIM_CHANCTRL14_CONFIGVALUE     : u32 = 0x38393A3Bu32;
pub const VIM_CHANCTRL15_CONFIGVALUE     : u32 = 0x3C3D3E3Fu32;
pub const VIM_CHANCTRL16_CONFIGVALUE     : u32 = 0x40414243u32;
pub const VIM_CHANCTRL17_CONFIGVALUE     : u32 = 0x44454647u32;
pub const VIM_CHANCTRL18_CONFIGVALUE     : u32 = 0x48494A4Bu32;
pub const VIM_CHANCTRL19_CONFIGVALUE     : u32 = 0x4C4D4E4Fu32;
pub const VIM_CHANCTRL20_CONFIGVALUE     : u32 = 0x50515253u32;
pub const VIM_CHANCTRL21_CONFIGVALUE     : u32 = 0x54555657u32;
pub const VIM_CHANCTRL22_CONFIGVALUE     : u32 = 0x58595A5Bu32;
pub const VIM_CHANCTRL23_CONFIGVALUE     : u32 = 0x5C5D5E5Fu32;