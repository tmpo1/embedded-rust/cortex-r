#![allow(non_camel_case_types, dead_code, non_snake_case, non_upper_case_globals, )]

use volatile_register::*;
use raw_pointer::Pointer;

/* Dcc Register Frame Definition */
/* @struct dccBase
*   @brief DCC Base Register Definition
*
*   This structure is used to access the DCC module registers.
*/
/* @typedef dccBASE_t
*   @brief DCC Register Frame Type Definition
*
*   This type is used to access the DCC Registers.
*/

#[repr(C)]
pub struct dccBASE_t
{
    pub GCTRL: RW<u32>,      	    /*< 0x0000: DCC Control Register		*/
    pub REV: RO<u32>,      		/*< 0x0004: DCC Revision Id Register 	*/
    pub CNT0SEED: RW<u32>,    	/*< 0x0008: DCC Counter0 Seed Register	*/
    pub VALID0SEED: RW<u32>,      /*< 0x000C: DCC Valid0 Seed Register 	*/
    pub CNT1SEED: RW<u32>,		/*< 0x0010: DCC Counter1 Seed Register 	*/
    pub STAT: RW<u32>, 			/*< 0x0014: DCC Status Register 		*/
    pub CNT0: RO<u32>,    		/*< 0x0018: DCC Counter0 Value Register 	*/
    pub VALID0: RO<u32>,    	    /*< 0x001C: DCC Valid0 Value Register 	*/
    pub CNT1: RW<u32>,      	    /*< 0x0020: DCC Counter1 Value Register	*/
    pub CNT1CLKSRC: RW<u32>,   	/*< 0x0024: DCC Counter1 Clock Source Selection Register 	*/
    pub CNT0CLKSRC: RW<u32>,   	/*< 0x0028: DCC Counter0 Clock Source Selection Register 	*/
}

/* @def dccREG1
*   @brief DCC1 Register Frame Pointer
*
*   This pointer is used by the DCC driver to access the dcc2 module registers.
*/
// #define dccREG1   ((dccBASE_t *)0xFFFFEC00U)
pub const dccREG1: Pointer<dccBASE_t> = Pointer::<dccBASE_t> {
    ptr: 0xFFFFEC00u32 as *mut dccBASE_t
};

/* Author's Note: What follows below is from dcc.h */


/* DCC General Definitions */

/* @def dcc1CNT0_CLKSRC_HFLPO
*   @brief Alias name for DCC1 Counter 0 Clock Source HFLPO
*
*   This is an alias name for the Clock Source HFLPO for DCC1 Counter 0.
*
*   @note This value should be used for API argument @a cnt0_Clock_Source
*/
pub const dcc1CNT0_CLKSRC_HFLPO: u32 =  0x00000005u32;

/* @def dcc1CNT0_CLKSRC_TCK
*   @brief Alias name for DCC1 Counter 0 Clock Source TCK
*
*   This is an alias name for the Clock Source TCK for DCC1 Counter 0.
*
*   @note This value should be used for API argument @a cnt0_Clock_Source
*/
pub const dcc1CNT0_CLKSRC_TCK: u32 =  0x0000000Au32; 

/* @def dcc1CNT0_CLKSRC_OSCIN
*   @brief Alias name for DCC1 Counter 0 Clock Source OSCIN
*
*   This is an alias name for the Clock Source OSCIN for DCC1 Counter 0.
*
*   @note This value should be used for API argument @a cnt0_Clock_Source
*/
pub const dcc1CNT0_CLKSRC_OSCIN: u32 =  0x0000000Fu32; 

/* @def dcc1CNT1_CLKSRC_PLL1
*   @brief Alias name for DCC1 Counter 1 Clock Source PLL1
*
*   This is an alias name for the Clock Source PLL for DCC1 Counter 1.
*
*   @note This value should be used for API argument @a cnt1_Clock_Source
*/
pub const dcc1CNT1_CLKSRC_PLL1: u32 =  0x0000A000u32; 

/* @def dcc1CNT1_CLKSRC_LFLPO
*   @brief Alias name for DCC1 Counter 1 Clock Source LFLPO
*
*   This is an alias name for the Clock Source LFLPO for DCC1 Counter 1.
*
*   @note This value should be used for API argument @a cnt1_Clock_Source
*/
pub const dcc1CNT1_CLKSRC_LFLPO: u32 =  0x0000A002u32; 

/* @def dcc1CNT1_CLKSRC_HFLPO
*   @brief Alias name for DCC1 Counter 1 Clock Source HFLPO
*
*   This is an alias name for the Clock Source HFLPO for DCC1 Counter 1.
*
*   @note This value should be used for API argument @a cnt1_Clock_Source
*/
pub const dcc1CNT1_CLKSRC_HFLPO: u32 =  0x0000A003u32; 

/* @def dcc1CNT1_CLKSRC_EXTCLKIN1
*   @brief Alias name for DCC1 Counter 1 Clock Source EXTCLKIN1
*
*   This is an alias name for the Clock Source EXTCLKIN1 for DCC1 Counter 1.
*
*   @note This value should be used for API argument @a cnt1_Clock_Source
*/
pub const dcc1CNT1_CLKSRC_EXTCLKIN1: u32 =  0x0000A005u32; 

/* @def dcc1CNT1_CLKSRC_VCLK
*   @brief Alias name for DCC1 Counter 1 Clock Source VCLK
*
*   This is an alias name for the Clock Source VCLK for DCC1 Counter 1.
*
*   @note This value should be used for API argument @a cnt1_Clock_Source
*/
pub const dcc1CNT1_CLKSRC_VCLK: u32 =  0x0000A008u32; 

/* @def dcc1CNT1_CLKSRC_N2HET1_31
*   @brief Alias name for DCC1 Counter 1 Clock Source N2HET1_31
*
*   This is an alias name for the Clock Source N2HET1_31 for DCC1 Counter 1.
*
*   @note This value should be used for API argument @a cnt1_Clock_Source
*/
pub const dcc1CNT1_CLKSRC_N2HET1_31: u32 =  0x0000500Fu32; 

/* @def dccNOTIFICATION_DONE
*   @brief Alias name for DCC Done notification
*
*   This is an alias name for the DCC Done notification.
*
*   @note This value should be used for API argument @a notification
*/
pub const dccNOTIFICATION_DONE: u32 =  0x0000A000u32; 

/* @def dccNOTIFICATION_ERROR
*   @brief Alias name for DCC Error notification
*
*   This is an alias name for the DCC Error notification.
*
*   @note This value should be used for API argument @a notification
*/
pub const dccNOTIFICATION_ERROR: u32 =  0x000000A0u32; 

/* @enum dcc1clocksource
*   @brief Alias names for dcc clock sources
*
*   This enumeration is used to provide alias names for the clock sources:
*/
pub struct dcc1clocksource;

impl dcc1clocksource
{
    pub const DCC1_CNT0_HF_LPO:u32    = 0x5;    /*< Alias for DCC1 CNT 0 CLOCK SOURCE 0*/
    pub const DCC1_CNT0_TCK:u32       = 0xA;    /*< Alias for DCC1 CNT 0 CLOCK SOURCE 1*/
    pub const DCC1_CNT0_OSCIN:u32     = 0xF;    /*< Alias for DCC1 CNT 0 CLOCK SOURCE 2*/

    pub const DCC1_CNT1_PLL1:u32      = 0x0;    /*< Alias for DCC1 CNT 1 CLOCK SOURCE 0*/
    pub const DCC1_CNT1_LF_LPO:u32    = 0x2;    /*< Alias for DCC1 CNT 1 CLOCK SOURCE 2*/
    pub const DCC1_CNT1_HF_LPO:u32    = 0x3;    /*< Alias for DCC1 CNT 1 CLOCK SOURCE 3*/
    pub const DCC1_CNT1_EXTCLKIN1:u32 = 0x5;    /*< Alias for DCC1 CNT 1 CLOCK SOURCE 4*/
    pub const DCC1_CNT1_VCLK:u32      = 0x8;    /*< Alias for DCC1 CNT 1 CLOCK SOURCE 8*/
    pub const DCC1_CNT1_N2HET1_31:u32 = 0xA;    /*< Alias for DCC1 CNT 1 CLOCK SOURCE 9*/
}


/* Configuration registers */
#[repr(C)]
pub struct dcc_config_reg_t
{
    pub CONFIG_GCTRL: u32, 
    pub CONFIG_CNT0SEED: u32, 
    pub CONFIG_VALID0SEED: u32, 
    pub CONFIG_CNT1SEED: u32, 
    pub CONFIG_CNT1CLKSRC: u32, 
    pub CONFIG_CNT0CLKSRC: u32, 
} 

/* Configuration registers initial value */
pub const DCC1_GCTRL_CONFIGVALUE:u32 =  0xAu32 
                                     | (0xAu32 << 4u32) as u32
                                     | (0x5u32 << 8u32) as u32
                                     | (0xAu32 << 12u32) as u32;
                                     
pub const DCC1_CNT0SEED_CONFIGVALUE:u32 =  78408u32;
pub const DCC1_VALID0SEED_CONFIGVALUE:u32 =  1584u32;
pub const DCC1_CNT1SEED_CONFIGVALUE:u32 =  495000u32;
pub const DCC1_CNT1CLKSRC_CONFIGVALUE:u32 =   ((10u32 << 12u32) | dcc1clocksource::DCC1_CNT1_PLL1 as u32) as u32;
/*SAFETYMCUSW 79 S MR:19.4 <APPROVED> "Values come from GUI drop down option" */
pub const DCC1_CNT0CLKSRC_CONFIGVALUE :u32 =  dcc1clocksource::DCC1_CNT0_OSCIN as u32;
