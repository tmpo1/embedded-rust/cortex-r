#![allow(non_camel_case_types, dead_code, non_snake_case, non_upper_case_globals, unused_parens)]
use volatile_register::*;
use super::reg_gio::gioPORT_t;

use raw_pointer::Pointer;

#[repr(C)]
pub struct hetBASE_t
{
    pub GCR: RW<u32>,     /*< 0x0000: Global control register              */
    pub PFR: RW<u32>,     /*< 0x0004: Prescale factor register             */
    pub ADDR: RW<u32>,    /*< 0x0008: Current address register             */
    pub OFF1: RW<u32>,    /*< 0x000C: Interrupt offset register 1          */
    pub OFF2: RW<u32>,    /*< 0x0010: Interrupt offset register 2          */
    pub INTENAS: RW<u32>, /*< 0x0014: Interrupt enable set register        */
    pub INTENAC: RW<u32>, /*< 0x0018: Interrupt enable clear register      */
    pub EXC1: RW<u32>,    /*< 0x001C: Exception control register 1          */
    pub EXC2: RW<u32>,    /*< 0x0020: Exception control register 2          */
    pub PRY: RW<u32>,     /*< 0x0024: Interrupt priority register          */
    pub FLG: RW<u32>,     /*< 0x0028: Interrupt flag register              */
    pub AND: RW<u32>,     /*< 0x002C: AND share control register         */
       rsvd1: RW<u32>, /*< 0x0030: Reserved                             */
    pub HRSH: RW<u32>,    /*< 0x0034: High resolution share register        */
    pub XOR: RW<u32>,     /*< 0x0038: XOR share register                   */
    pub REQENS: RW<u32>,  /*< 0x003C: Request enable set register          */
    pub REQENC: RW<u32>,  /*< 0x0040: Request enable clear register        */
    pub REQDS: RW<u32>,   /*< 0x0044: Request destination select register  */
       rsvd2: RW<u32>, /*< 0x0048: Reserved                             */
    pub DIR: RW<u32>,     /*< 0x004C: Direction register                   */
    pub DIN: RW<u32>,     /*< 0x0050: Data input register                  */
    pub DOUT: RW<u32>,    /*< 0x0054: Data output register                 */
    pub DSET: RW<u32>,    /*< 0x0058: Data output set register             */
    pub DCLR: RW<u32>,    /*< 0x005C: Data output clear register           */
    pub PDR: RW<u32>,     /*< 0x0060: Open drain register                  */
    pub PULDIS: RW<u32>,  /*< 0x0064: Pull disable register                */
    pub PSL: RW<u32>,     /*< 0x0068: Pull select register                 */
       rsvd3: RW<u32>, /*< 0x006C: Reserved                             */
       rsvd4: RW<u32>, /*< 0x0070: Reserved                             */
    pub PCR: RW<u32>,   /*< 0x0074: Parity control register              */
    pub PAR: RW<u32>,     /*< 0x0078: Parity address register              */
    pub PPR: RW<u32>,     /*< 0x007C: Parity pin select register           */
    pub SFPRLD: RW<u32>,  /*< 0x0080: Suppression filter preload register  */
    pub SFENA: RW<u32>,   /*< 0x0084: Suppression filter enable register   */
       rsvd5: RW<u32>, /*< 0x0088: Reserved                             */
    pub LBPSEL: RW<u32>,  /*< 0x008C: Loop back pair select register       */
    pub LBPDIR: RW<u32>,  /*< 0x0090: Loop back pair direction register    */
    PINDIS: RW<u32>,  /*< 0x0094: Pin disable register                 */
}


/* Author's Note: Not register, so cannot use volatile register. How to make volatile? */
#[repr(C)]
pub struct hetINSTRUCTION_t
{
    pub Program: u32,
    pub Control: u32,
    pub Data: u32,
    pub rsvd1: u32,
} 


#[repr(C)]
pub struct hetRAMBASE_t
{
    pub Instruction: [hetINSTRUCTION_t; 160],
} 



// #define hetREG1 ((hetBASE_t *)0xFFF7B800U)
pub static mut hetREG1: Pointer<hetBASE_t> = Pointer::<hetBASE_t> {
    ptr: 0xFFF7B800u32 as *mut hetBASE_t
};


// #define hetPORT1 ((gioPORT_t *)0xFFF7B84CU)
pub static mut hetPORT1: Pointer<gioPORT_t> = Pointer::<gioPORT_t> {
    ptr: 0xFFF7B84Cu32 as *mut gioPORT_t
};


// #define hetRAM1 ((hetRAMBASE_t *)0xFF460000U)
pub static mut hetRAM1: Pointer<hetRAMBASE_t> = Pointer::<hetRAMBASE_t> {
    ptr: 0xFF460000u32 as *mut hetRAMBASE_t
};

// #define NHET1RAMPARLOC	(*(volatile uint32 *)0xFF462000U)
pub static mut NHET1RAMPARLOC: Pointer<RW<u32>> = Pointer::<RW<u32>> {
    ptr: 0xFF462000u32 as *mut RW<u32>
};

// #define NHET1RAMLOC		(*(volatile uint32 *)0xFF460000U)
pub static mut NHET1RAMLOC: Pointer<RW<u32>> = Pointer::<RW<u32>> {
    ptr: 0xFF460000u32 as *mut RW<u32>
};

/* Author's Note: The following is from het.h */
pub const  pwm0  : u32 = 0u32;

/* @def pwm1
*   @brief Pwm signal 1
*
*   Alias for pwm signal 1
*/
pub const  pwm1  : u32 = 1u32;

/* @def pwm2
*   @brief Pwm signal 2
*
*   Alias for pwm signal 2
*/
pub const  pwm2  : u32 = 2u32;

/* @def pwm3
*   @brief Pwm signal 3
*
*   Alias for pwm signal 3
*/
pub const  pwm3  : u32 = 3u32;

/* @def pwm4
*   @brief Pwm signal 4
*
*   Alias for pwm signal 4
*/
pub const  pwm4  : u32 = 4u32;

/* @def pwm5
*   @brief Pwm signal 5
*
*   Alias for pwm signal 5
*/
pub const  pwm5  : u32 = 5u32;

/* @def pwm6
*   @brief Pwm signal 6
*
*   Alias for pwm signal 6
*/
pub const  pwm6  : u32 = 6u32;

/* @def pwm7
*   @brief Pwm signal 7
*
*   Alias for pwm signal 7
*/
pub const  pwm7  : u32 = 7u32;


/* @def edge0
*   @brief Edge signal 0
*
*   Alias for edge signal 0
*/
pub const  edge0  : u32 = 0u32;

/* @def edge1
*   @brief Edge signal 1
*
*   Alias for edge signal 1
*/
pub const  edge1  : u32 = 1u32;

/* @def edge2
*   @brief Edge signal 2
*
*   Alias for edge signal 2
*/
pub const  edge2  : u32 = 2u32;

/* @def edge3
*   @brief Edge signal 3
*
*   Alias for edge signal 3
*/
pub const  edge3  : u32 = 3u32;

/* @def edge4
*   @brief Edge signal 4
*
*   Alias for edge signal 4
*/
pub const  edge4  : u32 = 4u32;

/* @def edge5
*   @brief Edge signal 5
*
*   Alias for edge signal 5
*/
pub const  edge5  : u32 = 5u32;

/* @def edge6
*   @brief Edge signal 6
*
*   Alias for edge signal 6
*/
pub const  edge6  : u32 = 6u32;

/* @def edge7
*   @brief Edge signal 7
*
*   Alias for edge signal 7
*/
pub const  edge7  : u32 = 7u32;


/* @def cap0
*   @brief Capture signal 0
*
*   Alias for capture signal 0
*/
pub const  cap0  : u32 = 0u32;

/* @def cap1
*   @brief Capture signal 1
*
*   Alias for capture signal 1
*/
pub const  cap1  : u32 = 1u32;

/* @def cap2
*   @brief Capture signal 2
*
*   Alias for capture signal 2
*/
pub const  cap2  : u32 = 2u32;

/* @def cap3
*   @brief Capture signal 3
*
*   Alias for capture signal 3
*/
pub const  cap3  : u32 = 3u32;

/* @def cap4
*   @brief Capture signal 4
*
*   Alias for capture signal 4
*/
pub const  cap4  : u32 = 4u32;

/* @def cap5
*   @brief Capture signal 5
*
*   Alias for capture signal 5
*/
pub const  cap5  : u32 = 5u32;

/* @def cap6
*   @brief Capture signal 6
*
*   Alias for capture signal 6
*/
pub const  cap6  : u32 = 6u32;

/* @def cap7
*   @brief Capture signal 7
*
*   Alias for capture signal 7
*/
pub const  cap7  : u32 = 7u32;

/* @def pwmEND_OF_DUTY
*   @brief Pwm end of duty
*
*   Alias for pwm end of duty notification
*/
pub const  pwmEND_OF_DUTY  : u32 = 2u32;

/* @def pwmEND_OF_PERIOD
*   @brief Pwm end of period
*
*   Alias for pwm end of period notification
*/
pub const  pwmEND_OF_PERIOD  : u32 = 4u32;

/* @def pwmEND_OF_BOTH
*   @brief Pwm end of duty and period
*
*   Alias for pwm end of duty and period notification
*/
pub const  pwmEND_OF_BOTH  : u32 = 6u32;


/* USER CODE BEGIN (1) */
/* USER CODE END */

/* @struct hetBase
*   @brief HET Register Definition
*
*   This structure is used to access the HET module registers.
*/
/* @typedef hetBASE_t
*   @brief HET Register Frame Type Definition
*
*   This type is used to access the HET Registers.
*/

pub enum hetPinSelect
{
   PIN_HET_0 = 0,
   PIN_HET_1 = 1,
   PIN_HET_2 = 2,
   PIN_HET_3 = 3,
   PIN_HET_4 = 4,
   PIN_HET_5 = 5,
   PIN_HET_6 = 6,
   PIN_HET_7 = 7,
   PIN_HET_8 = 8,
   PIN_HET_9 = 9,
   PIN_HET_10 = 10,
   PIN_HET_11 = 11,
   PIN_HET_12 = 12,
   PIN_HET_13 = 13,
   PIN_HET_14 = 14,
   PIN_HET_15 = 15,
   PIN_HET_16 = 16,
   PIN_HET_17 = 17,
   PIN_HET_18 = 18,
   PIN_HET_19 = 19,
   PIN_HET_20 = 20,
   PIN_HET_21 = 21,
   PIN_HET_22 = 22,
   PIN_HET_23 = 23,
   PIN_HET_24 = 24,
   PIN_HET_25 = 25,
   PIN_HET_26 = 26,
   PIN_HET_27 = 27,
   PIN_HET_28 = 28,
   PIN_HET_29 = 29,
   PIN_HET_30 = 30,
   PIN_HET_31 = 31
}


/* @struct hetSignal
*   @brief HET Signal Definition
*
*   This structure is used to define a pwm signal.
*/
/* @typedef hetSIGNAL_t
*   @brief HET Signal Type Definition
*
*   This type is used to access HET Signal Information.
*/
#[repr(C)]
pub struct  hetSIGNAL_t
{
	pub duty: u32,   /*< Duty cycle in % of the period  */
	pub period: f64, /*< Period in us                   */
}

/* Configuration registers */
#[repr(C)]
pub struct het_config_reg_t
{
   pub CONFIG_GCR: u32,
   pub CONFIG_PFR: u32,
   pub CONFIG_INTENAS: u32,
   pub CONFIG_INTENAC: u32,
   pub CONFIG_PRY: u32,
   pub CONFIG_AND: u32,
   pub CONFIG_HRSH: u32,
   pub CONFIG_XOR: u32,
   pub CONFIG_DIR: u32,
   pub CONFIG_PDR: u32,	
	pub CONFIG_PULDIS: u32,
	pub CONFIG_PSL: u32,	
	pub CONFIG_PCR: u32,	
}

/* Configuration registers initial value for HET1*/
pub const HET1_DIR_CONFIGVALUE : u32 =     (0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);

pub const HET1_PDR_CONFIGVALUE : u32 =    (0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);

pub const HET1_PULDIS_CONFIGVALUE : u32 = (0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);							

pub const HET1_PSL_CONFIGVALUE : u32 =   (0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
   | 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);							

pub const HET1_HRSH_CONFIGVALUE : u32 =   (0x00008000u32 
| 0x00004000u32 
| 0x00002000u32 
| 0x00001000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000008u32 
| 0x00000004u32 
| 0x00000002u32 
| 0x00000001u32);

pub const HET1_AND_CONFIGVALUE  : u32 =   (0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);		

pub const HET1_XOR_CONFIGVALUE  : u32 =   (0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);						

pub const HET1_PFR_CONFIGVALUE  : u32 =  ((6u32 << 8u32) | 0u32);


pub const HET1_PRY_CONFIGVALUE  : u32 =   (0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);

pub const HET1_INTENAC_CONFIGVALUE: u32 = (0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);

pub const HET1_INTENAS_CONFIGVALUE: u32 = (0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32 
| 0x00000000u32);

pub const HET1_PCR_CONFIGVALUE     : u32 = (0x00000005u32);
pub const  HET1_GCR_CONFIGVALUE : u32 =      0x00030001u32;
