#![allow(non_snake_case, non_upper_case_globals)]

use volatile_register::RW;
use raw_pointer::Pointer;

#[repr(C)]
pub struct STCBase {
    pub STCGCR0: RW<u32>,
    pub STCGCR1: RW<u32>,
    pub STCTPR: RW<u32>,
    pub STC_CADDR: RW<u32>,
    pub STCCICR: RW<u32>,
    pub STCGSTAT: RW<u32>,
    pub STCFSTAT: RW<u32>,
    pub CPU1_CURMISR3: RW<u32>,
    pub CPU1_CURMISR2: RW<u32>,
    pub CPU1_CURMISR1: RW<u32>,
    pub CPU1_CURMISR0: RW<u32>,
    pub CPU2_CURMISR3: RW<u32>,
    pub CPU2_CURMISR2: RW<u32>,
    pub CPU2_CURMISR1: RW<u32>,
    pub CPU2_CURMISR0: RW<u32>,
    pub STCSCSCR: RW<u32>,
}

pub const stcREG: Pointer<STCBase> = Pointer::<STCBase> {
    ptr: 0xFFFFE600u32 as *mut STCBase
};
