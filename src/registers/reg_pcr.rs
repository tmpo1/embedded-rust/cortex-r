#![allow(non_snake_case, dead_code)]

use volatile_register::RW;
use raw_pointer::Pointer;

#[repr(C)]
pub struct PCRBase {
    pub PMPROTSET0: RW<u32>,
    pub PMPROTSET1: RW<u32>,
    rsvd1: [u32; 2],
    pub PMPROTCLR0: RW<u32>,
    pub PMPROTCLR1: RW<u32>,
    rsvd2: [u32; 2],
    pub PPROTSET0: RW<u32>,
    pub PPROTSET1: RW<u32>,
    pub PPROTSET2: RW<u32>,
    pub PPROTSET3: RW<u32>,
    rsvd3: [u32; 4],
    pub PPROTCLR0: RW<u32>,
    pub PPROTCLR1: RW<u32>,
    pub PPROTCLR2: RW<u32>,
    pub PPROTCLR3: RW<u32>,
    rsvd4: [u32; 4],
    pub PCSPWRDWNSET0: RW<u32>,
    pub PCSPWRDWNSET1: RW<u32>,
    rsvd5: [u32; 2],
    pub PCSPWRDWNCLR0: RW<u32>,
    pub PCSPWRDWNCLR1: RW<u32>,
    rsvd6: [u32; 2],
    pub PSPWRDWNSET0: RW<u32>,
    pub PSPWRDWNSET1: RW<u32>,
    pub PSPWRDWNSET2: RW<u32>,
    pub PSPWRDWNSET3: RW<u32>,
    rsvd7: [u32; 4],
    pub PSPWRDWNCLR0: RW<u32>,
    pub PSPWRDWNCLR1: RW<u32>,
    pub PSPWRDWNCLR2: RW<u32>,
    pub PSPWRDWNCLR3: RW<u32>,
}

pub const PCRREG: Pointer<PCRBase> = Pointer::<PCRBase> {
    ptr: 0xFFFFE000u32 as *mut PCRBase
};
