#![allow(non_camel_case_types, non_snake_case, non_upper_case_globals)]

use raw_pointer::*;
use volatile_register::*;

/* Crc Register Frame Definition */
/* @struct crcBase
*   @brief CRC Register Frame Definition
*
*   This type is used to access the CRC Registers.
*/
/* @typedef crcBASE_t
*   @brief CRC Register Frame Type Definition
*
*   This type is used to access the CRC Registers.
*/
#[repr(C)]
pub struct crcBASE_t
{
    pub      CTRL0: RW<u32>,                /*< 0x0000: Global Control Register 0 >**/
    pub      rvd1: RW<u32>,                 /*< 0x0004: reserved >**/
    pub      CTRL1: RW<u32>,                /*< 0x0008: Global Control Register 1 >**/
    pub      rvd2: RW<u32>,                 /*< 0x000C: reserved >**/    
    pub      CTRL2: RW<u32>,                /*< 0x0010: Global Control Register 2 >**/
    pub      rvd3: [RW<u32>; 19],             /*< 0x0014-0x0060: reserved >**/        
    pub      PSA_SIGREGL1: RW<u32>,         /*< 0x0060: Channel 1 PSA signature low register >**/
    pub      PSA_SIGREGH1: RW<u32>,         /*< 0x0064: Channel 1 PSA signature high register >**/
    pub      rvd4: [RW<u32>; 4],              /*< 0x0068-0x0078: reserved >**/        
    pub      RAW_DATAREGL1: RW<u32>,        /*< 0x0078: Channel 1 Raw Data Low Register >**/
    pub      RAW_DATAREGH1: RW<u32>,        /*< 0x007C: Channel 1 Raw Data High Register >**/
    pub      rvd5: [RW<u32>; 8],              /*< 0x0080-0x00A0: reserved >**/
    pub      PSA_SIGREGL2: RW<u32>,         /*< 0x00A0: Channel 2 PSA signature low register >**/
    pub      PSA_SIGREGH2: RW<u32>,         /*< 0x00A4: Channel 2 PSA signature high register >**/
    pub      rvd6: [RW<u32>; 4],              /*< 0x00A8-0x00B8: reserved >**/    
	pub      RAW_DATAREGL2: RW<u32>,        /*< 0x00B8: Channel 2 Raw Data Low Register >**/
    pub      RAW_DATAREGH2: RW<u32>,        /*< 0x00BC: Channel 2 Raw Data High Register >**/
}
 
/* @def crcREG
*   @brief CRC Register Frame Pointer
*
*   This pointer is used by the CRC driver to access the CRC registers.
*/
// #define crcREG ((crcBASE_t *)0xFE000000U)
pub const crcREG: Pointer<crcBASE_t> = Pointer::<crcBASE_t> {
    ptr: 0xFE000000u32 as *mut crcBASE_t
};

/* Author's Note: what follows is from crc.h */


/* CRC General Definitions */

/* @def CRCLEVEL_ACTIVE
*   @brief Alias name for CRC error operation level active 
*/
pub const CRCLEVEL_ACTIVE: u32 = 0x00u32;


/* @def CRC_AUTO
*   @brief Alias name for CRC auto mode
*/
pub const CRC_AUTO: u32 = 0x00000001u32;


/* @def CRC_SEMI_CPU
*   @brief Alias name for semi cpu mode setting
*/
pub const CRC_SEMI_CPU: u32 = 0x00000002u32;


/* @def CRC_FULL_CPU
*   @brief Alias name for CRC cpu full mode
*/
pub const CRC_FULL_CPU: u32 = 0x00000003u32;


/* @def CRC_CH4_TO
*   @brief Alias name for channel4 time out interrupt flag
*/
pub const CRC_CH4_TO: u32 = 0x10000000u32;

/* @def CRC_CH4_UR
*   @brief Alias name for channel4 underrun interrupt flag
*/
pub const CRC_CH4_UR: u32 = 0x08000000u32;

/* @def CRC_CH4_OR
*   @brief Alias name for channel4 overrun interrupt flag
*/
pub const CRC_CH4_OR: u32 = 0x04000000u32;

/* @def CRC_CH4_FAIL
*   @brief Alias name for channel4 crc fail interrupt flag
*/
pub const CRC_CH4_FAIL: u32 = 0x02000000u32;

/* @def CRC_CH4_CC
*   @brief Alias name for channel4 compression complete interrupt flag
*/
pub const CRC_CH4_CC: u32 = 0x01000000u32;

/* @def CRC_CH3_TO
*   @brief Alias name for channel3 time out interrupt flag
*/
pub const CRC_CH3_TO: u32 = 0x00100000u32;

/* @def CRC_CH3_UR
*   @brief Alias name for channel3 underrun interrupt flag
*/
pub const CRC_CH3_UR: u32 = 0x00080000u32;

/* @def CRC_CH3_OR
*   @brief Alias name for channel3 overrun interrupt flag
*/
pub const CRC_CH3_OR: u32 = 0x00040000u32;

/* @def CRC_CH3_FAIL
*   @brief Alias name for channel3 crc fail interrupt flag
*/
pub const CRC_CH3_FAIL: u32 = 0x00020000u32;

/* @def CRC_CH3_CC
*   @brief Alias name for channel3 compression complete interrupt flag
*/
pub const CRC_CH3_CC: u32 = 0x00010000u32;

/* @def CRC_CH2_TO
*   @brief Alias name for channel2 time out interrupt flag
*/
pub const CRC_CH2_TO: u32 = 0x00001000u32;

/* @def CRC_CH2_UR
*   @brief Alias name for channel2 underrun interrupt flag
*/
pub const CRC_CH2_UR: u32 = 0x00000800u32;

/* @def CRC_CH2_OR
*   @brief Alias name for channel2 overrun interrupt flag  
*/
pub const CRC_CH2_OR: u32 = 0x00000400u32;

/* @def CRC_CH2_FAIL
*   @brief Alias name for channel2 crc fail interrupt flag 
*/
pub const CRC_CH2_FAIL: u32 = 0x00000200u32;

/* @def CRC_CH2_CC
*   @brief Alias name for channel2 compression complete interrupt flag  
*/
pub const CRC_CH2_CC: u32 = 0x00000100u32;

/* @def CRC_CH1_TO
*   @brief Alias name for channel1 time out interrupt flag 
*/
pub const CRC_CH1_TO: u32 = 0x00000010u32;

/* @def CRC_CH1_UR
*   @brief Alias name for channel1 underrun interrupt flag
*/
pub const CRC_CH1_UR: u32 = 0x00000008u32;


/* @def CRC_CH1_OR
*   @brief Alias name for channel1 overrun interrupt flag  
*/
pub const CRC_CH1_OR: u32 = 0x00000004u32;

/* @def CRC_CH1_FAIL
*   @brief Alias name for channel1 crc fail interrupt flag  
*/
pub const CRC_CH1_FAIL: u32 = 0x00000002u32;

/* @def CRC_CH1_CC
*   @brief Alias name for channel1 compression complete interrupt flag    
*/
pub const CRC_CH1_CC: u32 = 0x00000001u32;

/* @def CRC_CH1
*   @brief Alias name for channel1 
*/
pub const CRC_CH1   : u32 = 0x00000000u32;

/* @def CRC_CH1
*   @brief Alias name for channel2 
*/
pub const CRC_CH2   : u32 = 0x00000001u32;

/* @def CRC_CH3
*   @brief Alias name for channel3 
*/
pub const CRC_CH3   : u32 = 0x00000002u32;

/* @def CRC_CH4
*   @brief Alias name for channel4 
*/
pub const CRC_CH4   : u32 = 0x00000003u32;


/* @struct crcModConfig
*   @brief CRC mode specific parameters 
*
*   This type is used to pass crc mode specific parameters 
*/
/* @typedef crcModConfig_t
*   @brief CRC Data Type Definition
*/
pub struct crcModConfig_t
{
    pub mode: u32,                  /*< Mode of operation        */
    pub crc_channel: u32,           /*< CRC channel-0,1       */
    pub src_data_pat: Pointer<u64>,     /*< Pattern data */
    pub data_length: u32,           /*< Pattern data length.Number of 64 bit size word*/
}

/* @struct crcConfig
*   @brief CRC configuration for different modes
*
*   This type is used to pass crc configuration
*/
/* @typedef crcConfig_t
*   @brief CRC Data Type Definition
*/
pub struct crcConfig_t
{
    pub crc_channel: u32,       /*< CRC channel-0,1         */
    pub mode: u32,              /*< Mode of operation        */
    pub pcount: u32,            /*< Pattern count*/
    pub scount: u32,            /*< Sector count  */
    pub wdg_preload: u32,       /*< Watchdog period */
    pub block_preload: u32,     /*< Block period*/

}

/* USER CODE BEGIN (1) */
/* USER CODE END */

pub struct crc_config_reg_t
{
    pub CONFIG_CTRL0: u32,
	pub CONFIG_CTRL1: u32,
	pub CONFIG_CTRL2: u32,
}
		
pub const CRC_CTRL0_CONFIGVALUE:u32 = 			0x00000000u32;
pub const CRC_CTRL1_CONFIGVALUE:u32 =			0x00000000u32;
pub const CRC_CTRL2_CONFIGVALUE:u32 = 			(0u32 << 4u32) | (CRC_FULL_CPU as u32)  | ((CRC_FULL_CPU as u32) << 8u32);