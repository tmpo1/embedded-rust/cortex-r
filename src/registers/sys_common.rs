#![allow(non_camel_case_types)]

// Author's Note: The source C file for this had a bunch of wierd typedefs that might be relevant later.
pub enum config_value_type_t
{
	InitialValue,
	CurrentValue
}

pub enum loopBackType_t 
{
    Digital_Lbk = 0, 
    Analog_Lbk = 1,
}