#![allow(non_snake_case, non_upper_case_globals)]
use volatile_register::*;
use raw_pointer::*;
/** @file reg_efc.h
*   @brief EFC Register Layer Header File
*   @date 11-Dec-2018
*   @version 04.07.01
*   
*   This file contains:
*   - Definitions
*   - Types
*   .
*   which are relevant for the System driver.
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/* Efc Register Frame Definition */
/** @struct efcBase
*   @brief Efc Register Frame Definition
*
*   This type is used to access the Efc Registers.
*/
/** @typedef efcBASE_t
*   @brief Efc Register Frame Type Definition
*
*   This type is used to access the Efc Registers.
*/
#[repr(C)]
pub struct efcBASE_t
{
        INSTRUCTION: RW<u32>,               /* 0x0  INSTRUCTION AN DUMPWORD REGISTER    */
        ADDRESS: RW<u32>,                   /* 0x4  ADDRESS REGISTER                    */
        DATA_UPPER: RW<u32>,                /* 0x8  DATA UPPER REGISTER                 */
        DATA_LOWER: RW<u32>,                /* 0xc  DATA LOWER REGISTER                 */
        SYSTEM_CONFIG: RW<u32>,             /* 0x10 SYSTEM CONFIG REGISTER              */
        SYSTEM_STATUS: RW<u32>,             /* 0x14 SYSTEM STATUS REGISTER              */
        ACCUMULATOR: RW<u32>,               /* 0x18 ACCUMULATOR REGISTER                */
    pub BOUNDARY: RW<u32>,                  /* 0x1C BOUNDARY REGISTER                   */
        KEY_FLAG: RW<u32>,                  /* 0x20 KEY FLAG REGISTER                   */
        KEY: RW<u32>,                       /* 0x24 KEY REGISTER                        */
        rsvd1: RW<u32>,                     /* 0x28 RESERVED                            */
    pub PINS: RO<u32>,                      /* 0x2C PINS REGISTER                       */
        CRA: RW<u32>,                       /* 0x30 CRA                                 */
        READ: RW<u32>,                      /* 0x34 READ REGISTER                       */
        PROGRAMME: RW<u32>,                 /* 0x38 PROGRAMME REGISTER                  */
    pub ERROR: RW<u32>,                     /* 0x3C ERROR STATUS REGISTER               */
        SINGLE_BIT: RW<u32>,                /* 0x40 SINGLE BIT ERROR                    */
        TWO_BIT_ERROR: RW<u32>,             /* 0x44 DOUBLE BIT ERROR                    */
    pub SELF_TEST_CYCLES: RW<u32>,          /* 0x48 SELF TEST CYCLEX                    */
    pub SELF_TEST_SIGN: RW<u32>,            /* 0x4C SELF TEST SIGNATURE                 */
}

// #define efcREG   ((efcBASE_t *)0xFFF8C000U)
pub const efcREG: Pointer<efcBASE_t> = Pointer::<efcBASE_t> {
    ptr: 0xFFF8C000u32 as *mut efcBASE_t
};