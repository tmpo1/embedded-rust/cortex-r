#![allow(non_snake_case, non_camel_case_types, non_upper_case_globals, dead_code)]

use volatile_register::*;
use raw_pointer::Pointer;

#[repr(C)]
pub struct gioBASE_t
{
    pub GCR0: RW<u32>,          /*< 0x0000: Global Control Register */
        rsvd: RW<u32>,          /*< 0x0004: Reserved*/
    pub INTDET: RW<u32>,        /*< 0x0008: Interrupt Detect Register*/
    pub POL: RW<u32>,           /*< 0x000C: Interrupt Polarity Register */
    pub ENASET: RW<u32>,        /*< 0x0010: Interrupt Enable Set Register */
    pub ENACLR: RW<u32>,        /*< 0x0014: Interrupt Enable Clear Register */
    pub LVLSET: RW<u32>,        /*< 0x0018: Interrupt Priority Set Register */
    pub LVLCLR: RW<u32>,        /*< 0x001C: Interrupt Priority Clear Register */
    pub FLG: RW<u32>,           /*< 0x0020: Interrupt Flag Register */
    pub OFF1: RW<u32>,          /*< 0x0024: Interrupt Offset A Register */
    pub OFF2: RW<u32>,          /*< 0x0028: Interrupt Offset B Register */
    pub EMU1: RW<u32>,          /*< 0x002C: Emulation 1 Register */
	pub EMU2: RW<u32>,          /*< 0x0030: Emulation 2 Register */
}

#[repr(C)]
pub struct gioPORT_t
{
    pub DIR: RW<u32>,    /*< 0x0000: Data Direction Register */
    pub DIN: RW<u32>,    /*< 0x0004: Data Input Register */
    pub DOUT: RW<u32>,   /*< 0x0008: Data Output Register */
    pub DSET: RW<u32>,   /*< 0x000C: Data Output Set Register */
    pub DCLR: RW<u32>,   /*< 0x0010: Data Output Clear Register */
    pub PDR: RW<u32>,    /*< 0x0014: Open Drain Register */
    pub PULDIS: RW<u32>, /*< 0x0018: Pullup Disable Register */
    pub PSL: RW<u32>,    /*< 0x001C: Pull Up/Down Selection Register */
}

// unsafe impl Sync for gioBASE_t { }

/* #define gioREG   ((gioBASE_t *)0xFFF7BC00U) */
// pub static mut gioREG: *mut gioBASE_t = 0xFFF7BC00u32 as *mut gioBASE_t;

/* #define gioPORTA ((gioPORT_t *)0xFFF7BC34U) */
// pub static mut gioPORTA: *mut gioPORT_t = 0xFFF7BC34u32 as *mut gioPORT_t;

/* Author's Note: What follows below is ported gio.h */
pub const gioREG: Pointer<gioBASE_t> = Pointer::<gioBASE_t> {
    ptr: 0xFFF7BC00u32 as *mut gioBASE_t
};

pub const gioPORTA: Pointer<gioPORT_t> = Pointer::<gioPORT_t> {
    ptr: 0xFFF7BC34u32 as *mut gioPORT_t
};


#[repr(C)]
pub struct gio_config_reg_t
{
     pub CONFIG_INTDET: u32,
     pub CONFIG_POL: u32,
     pub CONFIG_INTENASET: u32,
     pub CONFIG_LVLSET: u32,
	
	 pub CONFIG_PORTADIR: u32,
	 pub CONFIG_PORTAPDR: u32,
	 pub CONFIG_PORTAPSL: u32,
	 pub CONFIG_PORTAPULDIS: u32,

}

pub static GIO_INTDET_CONFIGVALUE: u32          = 0u32;
pub static GIO_POL_CONFIGVALUE: u32	            = 0u32;
pub static GIO_INTENASET_CONFIGVALUE: u32       = 0u32;
pub static GIO_LVLSET_CONFIGVALUE: u32          = 0u32;           
pub static GIO_PORTADIR_CONFIGVALUE: u32        = 0u32;
pub static GIO_PORTAPDR_CONFIGVALUE: u32	    = 0u32;
pub static GIO_PORTAPSL_CONFIGVALUE: u32	    = 0u32;
pub static GIO_PORTAPULDIS_CONFIGVALUE: u32     = 0u32;
