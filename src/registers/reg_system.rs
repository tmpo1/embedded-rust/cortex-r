#![allow(non_snake_case, non_camel_case_types, dead_code)]

use volatile_register::RW;
use raw_pointer::Pointer;

#[repr(C)]
pub struct PrimaryControlFrame {
    pub SYSPC1: RW<u32>,
    pub SYSPC2: RW<u32>,
    pub SYSPC3: RW<u32>,
    pub SYSPC4: RW<u32>,
    pub SYSPC5: RW<u32>,
    pub SYSPC6: RW<u32>,
    pub SYSPC7: RW<u32>,
    pub SYSPC8: RW<u32>,
    pub SYSPC9: RW<u32>,
    rsvd1: [u32; 3],
    pub CSDIS: RW<u32>,
    pub CSDISSET: RW<u32>,
    pub CSDISCLR: RW<u32>,
    pub CDDIS: RW<u32>,
    pub CDDISSET: RW<u32>,
    pub CDDISCLR: RW<u32>,
    pub GHVSRC: RW<u32>,
    pub VCLKASRC: RW<u32>,
    pub RCLKSRC: RW<u32>,
    pub CSVSTAT: RW<u32>,
    pub MSTGCR: RW<u32>,
    pub MINITGCR: RW<u32>,
    pub MSINENA: RW<u32>,
    pub MSTFAIL: RW<u32>,
    pub MSTCGSTAT: RW<u32>,
    pub MINISTAT: RW<u32>,
    pub PLLCTL1: RW<u32>,
    pub PLLCTL2: RW<u32>,
    pub SYSPC10: RW<u32>,
    pub DIEIDL: RW<u32>,
    pub DIEIDH: RW<u32>,
    rsvd2: u32,
    pub LPOMONCTL: RW<u32>,
    pub CLKTEST: RW<u32>,
    pub DFTCTRLREG: RW<u32>,
    pub DFTCTRLREG2: RW<u32>,
    rsvd3: [u32; 2],
    pub GPREG1: RW<u32>,
    rsvd4: u32,
    pub IMPFASTS: RW<u32>,
    pub IMPFTADD: RW<u32>,
    pub SSIR1: RW<u32>,
    pub SSIR2: RW<u32>,
    pub SSIR3: RW<u32>,
    pub SSIR4: RW<u32>,
    pub RAMGCR: RW<u32>,
    pub BMMCR1: RW<u32>,
    rsvd5: u32,
    pub CPURSTCR: RW<u32>,
    pub CLKCNTL: RW<u32>,
    pub ECPCNTL: RW<u32>,
    rsvd6: u32,
    pub DEVCR1: RW<u32>,
    pub SYSECR: RW<u32>,
    pub SYSESR: RW<u32>,
    pub SYSTASR: RW<u32>,
    pub GLBSTAT: RW<u32>,
    pub DEVID: RW<u32>,
    pub SSIVEC: RW<u32>,
    pub SSIF: RW<u32>,
}

#[repr(C)]
pub struct SecondaryControlFrame {
    rsvd1: [u32; 2],
    pub STCCLKDIV: RW<u32>,
    rsvd2: [u32; 25],
    pub CLKSLIP: RW<u32>,
    rsvd3: [u32; 30],
    pub EFC_CTLREG: RW<u32>,
    pub DIEIDL_REG0: RW<u32>,
    pub DIEIDH_REG1: RW<u32>,
    pub DIEIDL_REG2: RW<u32>,
    pub DIEIDH_REG3: RW<u32>,
}

pub const SYSFRAME1: Pointer<PrimaryControlFrame> = Pointer::<PrimaryControlFrame> {
    ptr: 0xFFFFFF00u32 as *mut PrimaryControlFrame
};
pub const SYSFRAME2: Pointer<SecondaryControlFrame> = Pointer::<SecondaryControlFrame> {
    ptr: 0xFFFFE100u32 as *mut SecondaryControlFrame
};

pub const LPO_TRIM_VALUE: Pointer<u32> = Pointer::<u32> {
    ptr: 0xF00801B4u32 as *mut u32
};
