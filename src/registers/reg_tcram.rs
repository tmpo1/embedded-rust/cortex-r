#![allow(non_camel_case_types, dead_code, non_snake_case, non_upper_case_globals)]
use volatile_register::*;
use raw_pointer::*;
/** @file reg_tcram.h
*   @brief TCRAM Register Layer Header File
*   @date 11-Dec-2018
*   @version 04.07.01
*   
*   This file contains:
*   - Definitions
*   - Types
*   .
*   which are relevant for the System driver.
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com 
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

pub struct tcramBASE_t
{
    pub RAMCTRL: RW<u32>,		    /* 0x0000 */
    pub RAMTHRESHOLD: RW<u32>,      /* 0x0004 */
    pub RAMOCCUR: RW<u32>,			/* 0x0008 */
    pub RAMINTCTRL: RW<u32>,		/* 0x000C */
    pub RAMERRSTATUS: RW<u32>,		/* 0x0010 */
    pub RAMSERRADDR: RO<u32>,		/* 0x0014 */
       rsvd1: u32,			        /* 0x0018 */
    pub RAMUERRADDR: RO<u32>,		/* 0x001C */
       rsvd2: [u32; 4],		        /* 0x0020 */
    pub RAMTEST: RW<u32>,			/* 0x0030 */
       rsvd3: u32,			        /* 0x0034 */
    pub RAMADDRDECVECT: RW<u32>,	/* 0x0038 */
    pub RAMPERADDR: RW<u32>,        /* 0x003C */
}

pub const tcram1REG: Pointer<tcramBASE_t> = Pointer::<tcramBASE_t> {
	ptr: 0xFFFFF800u32 as *mut tcramBASE_t
};
pub const tcram2REG: Pointer<tcramBASE_t> = Pointer::<tcramBASE_t> {
	ptr: 0xFFFFF900u32 as *mut tcramBASE_t
};

