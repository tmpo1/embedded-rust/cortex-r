#![allow(non_snake_case, non_upper_case_globals, non_camel_case_types, dead_code)]
use crate::registers::reg_gio::*;
use raw_pointer::*;
use volatile_register::*;

/* @file reg_spi.h
*   @brief SPI Register Layer Header File
*   @date 11-Dec-2018
*   @version 04.07.01
*   
*   This file contains:
*   - Definitions
*   - Types
*   - Interface Prototypes
*   .
*   which are relevant for the SPI driver.
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com  
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/





/* Spi Register Frame Definition */
/* @struct spiBase
*   @brief SPI Register Definition
*
*   This structure is used to access the SPI module registers.
*/
/* @typedef spiBASE_t
*   @brief SPI Register Frame Type Definition
*
*   This type is used to access the SPI Registers.
*/
#[repr(C)]
pub struct spiBASE_t
{
    pub  GCR0: RW<u32>,                 /*< 0x0000: Global Control 0 */
    pub  GCR1: RW<u32>,                 /*< 0x0004: Global Control 1 */	
    pub  INT0: RW<u32>,                 /*< 0x0008: Interrupt Register */	
    pub  LVL: RW<u32>,                  /*< 0x000C: Interrupt Level */
    pub  FLG: RW<u32>,                  /*< 0x0010: Interrupt flags */		
    pub  PC0: RW<u32>,                  /*< 0x0014: Function Pin Enable */
    pub  PC1: RW<u32>,                  /*< 0x0018: Pin Direction */
    pub  PC2: RW<u32>,                  /*< 0x001C: Pin Input Latch */
    pub  PC3: RW<u32>,                  /*< 0x0020: Pin Output Latch */
    pub  PC4: RW<u32>,                  /*< 0x0024: Output Pin Set */
    pub  PC5: RW<u32>,                  /*< 0x0028: Output Pin Clr */
    pub  PC6: RW<u32>,                  /*< 0x002C: Open Drain Output Enable */
    pub  PC7: RW<u32>,                  /*< 0x0030: Pullup/Pulldown Disable */
    pub  PC8: RW<u32>,                  /*< 0x0034: Pullup/Pulldown Selection */
    pub  DAT0: RW<u32>,                 /*< 0x0038: Transmit Data */
    pub  DAT1: RW<u32>,                 /*< 0x003C: Transmit Data with Format and Chip Select */
    pub  BUF: RW<u32>,                  /*< 0x0040: Receive Buffer */
    pub  EMU: RO<u32>,                  /*< 0x0044: Emulation Receive Buffer */
    pub  DELAY: RW<u32>,                /*< 0x0048: Delays */
    pub  DEF: RW<u32>,                  /*< 0x004C: Default Chip Select */
    pub  FMT0: RW<u32>,                 /*< 0x0050: Data Format 0 */
    pub  FMT1: RW<u32>,                 /*< 0x0054: Data Format 1 */
    pub  FMT2: RW<u32>,                 /*< 0x0058: Data Format 2 */
    pub  FMT3: RW<u32>,                 /*< 0x005C: Data Format 3 */
    pub  INTVECT0: RO<u32>,             /*< 0x0060: Interrupt Vector 0 */
    pub  INTVECT1: RO<u32>,             /*< 0x0064: Interrupt Vector 1 */
    
    /* Author's Note: There are a fair bit of registers here, I think this is reserved due to some congfiguration option? */
    pub  RESERVED: [RW<u32>; 51],        /*< 0x0068 to 0x0130: Reserved */	
    pub  IOLPKTSTCR: RW<u32>,           /*< 0x0134: IO loopback */
} 


/* @def spiREG1
*   @brief SPI1 (MIBSPI - Compatibility Mode) Register Frame Pointer
*
*   This pointer is used by the SPI driver to access the spi module registers.
*/
pub const spiREG1: Pointer<spiBASE_t> = Pointer::<spiBASE_t> {
	ptr: 0xFFF7F400u32 as *mut spiBASE_t
};


/* @def spiPORT1
*   @brief SPI1 (MIBSPI - Compatibility Mode) GIO Port Register Pointer
*
*   Pointer used by the GIO driver to access I/O PORT of SPI1
*   (use the GIO drivers to access the port pins).
*/
/* Author's Note: This is an interesting move, if you look at offset 0x0018 in spiBASE_t, you see it align with gioBASE_t*/
pub const spiPORT1: Pointer<gioPORT_t> = Pointer::<gioPORT_t> {
	ptr: 0xFFF7F418u32 as *mut gioPORT_t
};

/* @def spiREG2
*   @brief SPI2 Register Frame Pointer
*
*   This pointer is used by the SPI driver to access the spi module registers.
*/
pub const spiREG2: Pointer<spiBASE_t> = Pointer::<spiBASE_t> {
	ptr: 0xFFF7F600u32 as *mut spiBASE_t
};


/* @def spiPORT2
*   @brief SPI2 GIO Port Register Pointer
*
*   Pointer used by the GIO driver to access I/O PORT of SPI2
*   (use the GIO drivers to access the port pins).
*/
pub const spiPORT2: Pointer<gioPORT_t> = Pointer::<gioPORT_t> {
	ptr: 0xFFF7F618u32 as *mut gioPORT_t
};


/* @def spiREG3
*   @brief SPI3 Register Frame Pointer
*
*   This pointer is used by the SPI driver to access the spi module registers.
*/
pub const spiREG3: Pointer<spiBASE_t> = Pointer::<spiBASE_t> {
	ptr: 0xFFF7F800u32 as *mut spiBASE_t
};


/* @def spiPORT3
*   @brief SPI3 GIO Port Register Pointer
*
*   Pointer used by the GIO driver to access I/O PORT of SPI3
*   (use the GIO drivers to access the port pins).
*/
pub const spiPORT3: Pointer<gioPORT_t> = Pointer::<gioPORT_t> {
	ptr: 0xFFF7F818u32 as *mut gioPORT_t
};

/* Author's Note: The below is from spi.h */


/** @pub enum chipSelect
*   @brief Transfer Group Chip Select
*/
pub enum spiChipSelect
{
    SPI_CS_NONE = 0xFF,
    SPI_CS_0    = 0xFE,
    SPI_CS_1    = 0xFD,
    SPI_CS_2    = 0xFB,
    SPI_CS_3    = 0xF7,
    SPI_CS_4    = 0xEF,
    SPI_CS_5    = 0xDF,
    SPI_CS_6    = 0xBF,
    SPI_CS_7    = 0x7F
}

/** @pub enum spiPinSelect
*   @brief spi Pin Select
*/
pub enum spiPinSelect
{
    SPI_PIN_CS0    = 0,
    SPI_PIN_CS1    = 1,
    SPI_PIN_CS2    = 2,
    SPI_PIN_CS3    = 3,
    SPI_PIN_CS4    = 4,
    SPI_PIN_CS5    = 5,
    SPI_PIN_CS6    = 6,
    SPI_PIN_CS7    = 7,
    SPI_PIN_ENA    = 8,
    SPI_PIN_CLK    = 9,
    SPI_PIN_SIMO   = 10,
    SPI_PIN_SOMI   = 11,
    SPI_PIN_SIMO_1 = 17,
    SPI_PIN_SIMO_2 = 18,
    SPI_PIN_SIMO_3 = 19,
    SPI_PIN_SIMO_4 = 20,
    SPI_PIN_SIMO_5 = 21,
    SPI_PIN_SIMO_6 = 22,
    SPI_PIN_SIMO_7 = 23,
    SPI_PIN_SOMI_1 = 25,
    SPI_PIN_SOMI_2 = 26,
    SPI_PIN_SOMI_3 = 27,
    SPI_PIN_SOMI_4 = 28,
    SPI_PIN_SOMI_5 = 29,
    SPI_PIN_SOMI_6 = 30,
    SPI_PIN_SOMI_7 = 31
}

/** @pub enum dataformat
*   @brief SPI dataformat register select
*/
pub struct SPIDATAFMT_t;

impl SPIDATAFMT_t {
    pub const SPI_FMT_0: u32 = 0;
    pub const SPI_FMT_1: u32 = 1;
    pub const SPI_FMT_2: u32 = 2;
    pub const SPI_FMT_3: u32 = 3;
}

    

/** @struct spiDAT1RegConfig
*   @brief SPI data register configuration
*/
pub struct spiDAT1_t
{
    pub CS_HOLD: bool,
    pub WDEL: bool,
    pub DFSEL: u32, /*Author's Note: This was changed to u32 to allow shifting and the SPIDATAFMT_t enum was changed to struct/impl */
    pub CSNR: u8,
}

/** @enum SpiTxRxDataStatus
*   @brief SPI Data Status
*/
pub enum SpiDataStatus_t
{
    SPI_READY = 0,
    SPI_PENDING = 1,
    SPI_COMPLETED = 2
}


pub struct spi_config_reg_t
{
    pub CONFIG_GCR1: u32,
    pub CONFIG_INT0: u32,
    pub CONFIG_LVL: u32,
    pub CONFIG_PC0: u32,
    pub CONFIG_PC1: u32,
    pub CONFIG_PC6: u32,
    pub CONFIG_PC7: u32,
    pub CONFIG_PC8: u32,
    pub CONFIG_DELAY: u32,
    pub CONFIG_FMT0: u32,
    pub CONFIG_FMT1: u32,
    pub CONFIG_FMT2: u32,
    pub CONFIG_FMT3: u32,
}



pub const SPI2_GCR1_CONFIGVALUE     :u32 = 0x01000000u32 | (1u32 << 1u32) | 1u32;

pub const SPI2_INT0_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 8u32) | (0u32 << 6u32) | (0u32 << 4u32) | (0u32 << 3u32) | (0u32 << 2u32) | (0u32 << 1u32) | (0u32 << 0u32);
pub const SPI2_LVL_CONFIGVALUE      :u32 = (0u32 << 9u32) | (0u32 << 8u32) | (0u32 << 6u32) | (0u32 << 4u32) | (0u32 << 3u32) | (0u32 << 2u32) | (0u32 << 1u32) | (0u32 << 0u32);

pub const SPI2_PC0_CONFIGVALUE      :u32 = (1u32 << 0u32) | (1u32 << 1u32) | (1u32 << 2u32) | (1u32 << 3u32) | (1u32 << 9u32) | (1u32 << 10u32) | (1u32 << 16u32) | (1u32 << 11u32) | (1u32 << 24u32);
pub const SPI2_PC1_CONFIGVALUE      :u32 = (1u32 << 0u32) | (1u32 << 1u32) | (1u32 << 2u32) | (1u32 << 3u32) | (1u32 << 9u32) | (1u32 << 10u32) | (1u32 << 16u32) | (0u32 << 11u32) | (0u32 << 24u32);
pub const SPI2_PC6_CONFIGVALUE      :u32 = (0u32 << 0u32) | (0u32 << 1u32) | (0u32 << 2u32) | (0u32 << 3u32) | (0u32 << 9u32) | (0u32 << 10u32) | (0u32 << 16u32) | (0u32 << 11u32) | (0u32 << 24u32);
pub const SPI2_PC7_CONFIGVALUE      :u32 = (0u32 << 0u32) | (0u32 << 1u32) | (0u32 << 2u32) | (0u32 << 3u32) | (0u32 << 9u32) | (0u32 << 10u32) | (0u32 << 16u32) | (0u32 << 11u32) | (0u32 << 24u32);
pub const SPI2_PC8_CONFIGVALUE      :u32 = (1u32 << 0u32) | (1u32 << 1u32) | (1u32 << 2u32) | (1u32 << 3u32) | (1u32 << 9u32) | (1u32 << 10u32) | (1u32 << 16u32) | (1u32 << 11u32) | (1u32 << 24u32);

pub const SPI2_DELAY_CONFIGVALUE    :u32 = (0u32 << 24u32) | (0u32 << 16u32) | (0u32 << 8u32) | (0u32 << 0u32);

pub const SPI2_FMT0_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
pub const SPI2_FMT1_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
pub const SPI2_FMT2_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
pub const SPI2_FMT3_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);


pub const SPI3_GCR1_CONFIGVALUE     :u32 = 0x01000000u32 | (1u32 << 1u32) | 1u32;

pub const SPI3_INT0_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 8u32) | (0u32 << 6u32) | (0u32 << 4u32) | (0u32 << 3u32) | (0u32 << 2u32) | (0u32 << 1u32) | (0u32 << 0u32);
pub const SPI3_LVL_CONFIGVALUE      :u32 = (0u32 << 9u32) | (0u32 << 8u32) | (0u32 << 6u32) | (0u32 << 4u32) | (0u32 << 3u32) | (0u32 << 2u32) | (0u32 << 1u32) | (0u32 << 0u32);

pub const SPI3_PC0_CONFIGVALUE      :u32 = (1u32 << 0u32) | (1u32 << 1u32) | (1u32 << 2u32) | (1u32 << 3u32) | (1u32 << 8u32) | (1u32 << 9u32) | (1u32 << 10u32) | (1u32 << 16u32) | (1u32 << 11u32) | (1u32 << 24u32);
pub const SPI3_PC1_CONFIGVALUE      :u32 = (1u32 << 0u32) | (1u32 << 1u32) | (1u32 << 2u32) | (1u32 << 3u32) | (0u32 << 8u32) | (1u32 << 9u32) | (1u32 << 10u32) | (1u32 << 16u32) | (0u32 << 11u32) | (0u32 << 24u32);
pub const SPI3_PC6_CONFIGVALUE      :u32 = (0u32 << 0u32) | (0u32 << 1u32) | (0u32 << 2u32) | (0u32 << 3u32) | (0u32 << 8u32) | (0u32 << 9u32) | (0u32 << 10u32) | (0u32 << 16u32) | (0u32 << 11u32) | (0u32 << 24u32);
pub const SPI3_PC7_CONFIGVALUE      :u32 = (0u32 << 0u32) | (0u32 << 1u32) | (0u32 << 2u32) | (0u32 << 3u32) | (0u32 << 8u32) | (0u32 << 9u32) | (0u32 << 10u32) | (0u32 << 10u32) | (0u32 << 11u32) | (0u32 << 24u32);
pub const SPI3_PC8_CONFIGVALUE      :u32 = (1u32 << 0u32) | (1u32 << 1u32) | (1u32 << 2u32) | (1u32 << 3u32) | (1u32 << 8u32) | (1u32 << 9u32) | (1u32 << 10u32) | (1u32 << 16u32) | (1u32 << 11u32) | (1u32 << 24u32);

pub const SPI3_DELAY_CONFIGVALUE    :u32 = (0u32 << 24u32) | (0u32 << 16u32) | (0u32 << 8u32) | (0u32 << 0u32);

pub const SPI3_FMT0_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
pub const SPI3_FMT1_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
pub const SPI3_FMT2_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
pub const SPI3_FMT3_CONFIGVALUE     :u32 = (0u32 << 24u32) | (0u32 << 23u32) | (0u32 << 22u32) | (0u32 << 21u32) | (0u32 << 20u32) | (0u32 << 17u32) | (0u32 << 16u32) | (99u32 << 8u32) | (16u32 << 0u32);
