#![allow(non_snake_case, non_upper_case_globals, non_camel_case_types, dead_code)]

use volatile_register::{RO, RW};
use raw_pointer::Pointer;

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com  
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#[repr(C)]
pub struct ESMBase {
    pub ESMEEPAPR1: RW<u32>,
    pub ESMDEPAPR1: RW<u32>,
    pub ESMIESR1: RW<u32>,
    pub ESMIECR1: RW<u32>,
    pub ESMILSR1: RW<u32>,
    pub ESMILCR1: RW<u32>,
    pub ESMSR1: RW<u32>,
    pub ESMSR2: RW<u32>,
    pub ESMSR3: RW<u32>,
    pub ESMEPSR: RO<u32>,
    pub ESMIOFFHR: RO<u32>,
    pub ESMIOFFLR: RO<u32>,
    pub ESMLTCR: RO<u32>,
    pub ESMLTCPR: RW<u32>,  /* only bits 14 and 15 are configurable */
    pub ESMEKR: RW<u32>,
    pub ESMSSR2: RW<u32>,
    pub ESMIEPSR4: RW<u32>,
    pub ESMIEPCR4: RW<u32>,
    pub ESMIESR4: RW<u32>,
    pub ESMIECR4: RW<u32>,
    pub ESMILSR4: RW<u32>,
    pub ESMILCR4: RW<u32>,
    pub ESMSR4: RW<u32>,
}

pub const esmREG: Pointer<ESMBase> = Pointer::<ESMBase> {
    ptr: 0xFFFFF500u32 as *mut ESMBase
};

/* Author's Note: What follows is ported from esm.h */


/* ESM General Definitions */

/** @def esmGROUP1
*   @brief Alias name for ESM group 1
*
*   This is an alias name for the ESM group 1.
*
*   @note This value should be used for API argument @a group
*/
pub const esmGROUP1: u32 = 0u32;

/** @def esmGROUP2
*   @brief Alias name for ESM group 2
*
*   This is an alias name for the ESM group 2.
*
*   @note This value should be used for API argument @a group
*/
pub const esmGROUP2: u32 = 1u32;

/** @def esmGROUP3
*   @brief Alias name for ESM group 3
*
*   This is an alias name for the ESM group 3.
*
*   @note This value should be used for API argument @a group
*/
pub const esmGROUP3: u32 = 2u32;

/** @def esmCHANNEL0
*   @brief Alias name for ESM group x channel 0
*
*   This is an alias name for the ESM group x channel 0.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL0: u64 = 0x0000000000000001u64;

/** @def esmCHANNEL1
*   @brief Alias name for ESM group x channel 1
*
*   This is an alias name for the ESM group x channel 1.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL1: u64 = 0x0000000000000002u64;

/** @def esmCHANNEL2
*   @brief Alias name for ESM group x channel 2
*
*   This is an alias name for the ESM group x channel 2.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL2: u64 = 0x0000000000000004u64;

/** @def esmCHANNEL3
*   @brief Alias name for ESM group x channel 3
*
*   This is an alias name for the ESM group x channel 3.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL3: u64 = 0x0000000000000008u64;

/** @def esmCHANNEL4
*   @brief Alias name for ESM group x channel 4
*
*   This is an alias name for the ESM group x channel 4.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL4: u64 = 0x0000000000000010u64;

/** @def esmCHANNEL5
*   @brief Alias name for ESM group x channel 5
*
*   This is an alias name for the ESM group x channel 5.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL5: u64 = 0x0000000000000020u64;

/** @def esmCHANNEL6
*   @brief Alias name for ESM group x channel 6
*
*   This is an alias name for the ESM group x channel 6.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL6: u64 = 0x0000000000000040u64;

/** @def esmCHANNEL7
*   @brief Alias name for ESM group x channel 7
*
*   This is an alias name for the ESM group x channel 7.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL7: u64 = 0x0000000000000080u64;

/** @def esmCHANNEL8
*   @brief Alias name for ESM group x channel 8
*
*   This is an alias name for the ESM group x channel 8.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL8: u64 = 0x0000000000000100u64;

/** @def esmCHANNEL9
*   @brief Alias name for ESM group x channel 9
*
*   This is an alias name for the ESM group x channel 9.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL9: u64 = 0x0000000000000200u64;

/** @def esmCHANNEL10
*   @brief Alias name for ESM group x channel 10
*
*   This is an alias name for the ESM group x channel 10.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL10: u64 = 0x0000000000000400u64;

/** @def esmCHANNEL11
*   @brief Alias name for ESM group x channel 11
*
*   This is an alias name for the ESM group x channel 11.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL11: u64 = 0x0000000000000800u64;

/** @def esmCHANNEL12
*   @brief Alias name for ESM group x channel 12
*
*   This is an alias name for the ESM group x channel 12.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL12: u64 = 0x0000000000001000u64;

/** @def esmCHANNEL13
*   @brief Alias name for ESM group x channel 13
*
*   This is an alias name for the ESM group x channel 13.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL13: u64 = 0x0000000000002000u64;

/** @def esmCHANNEL14
*   @brief Alias name for ESM group x channel 14
*
*   This is an alias name for the ESM group x channel 14.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL14: u64 = 0x0000000000004000u64;

/** @def esmCHANNEL15
*   @brief Alias name for ESM group x channel 15
*
*   This is an alias name for the ESM group x channel 15.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL15: u64 = 0x0000000000008000u64;

/** @def esmCHANNEL16
*   @brief Alias name for ESM group x channel 16
*
*   This is an alias name for the ESM group x channel 16.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL16: u64 = 0x0000000000010000u64;

/** @def esmCHANNEL17
*   @brief Alias name for ESM group x channel 17
*
*   This is an alias name for the ESM group x channel 17.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL17: u64 = 0x0000000000020000u64;

/** @def esmCHANNEL18
*   @brief Alias name for ESM group x channel 18
*
*   This is an alias name for the ESM group x channel 18.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL18: u64 = 0x0000000000040000u64;

/** @def esmCHANNEL19
*   @brief Alias name for ESM group x channel 19
*
*   This is an alias name for the ESM group x channel 19.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL19: u64 = 0x0000000000080000u64;

/** @def esmCHANNEL20
*   @brief Alias name for ESM group x channel 20
*
*   This is an alias name for the ESM group x channel 20.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL20: u64 = 0x0000000000100000u64;

/** @def esmCHANNEL21
*   @brief Alias name for ESM group x channel 21
*
*   This is an alias name for the ESM group x channel 21.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL21: u64 = 0x0000000000200000u64;

/** @def esmCHANNEL22
*   @brief Alias name for ESM group x channel 22
*
*   This is an alias name for the ESM group x channel 22.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL22: u64 = 0x0000000000400000u64;

/** @def esmCHANNEL23
*   @brief Alias name for ESM group x channel 23
*
*   This is an alias name for the ESM group x channel 23.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL23: u64 = 0x0000000000800000u64;

/** @def esmCHANNEL24
*   @brief Alias name for ESM group x channel 24
*
*   This is an alias name for the ESM group x channel 24.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL24: u64 = 0x0000000001000000u64;

/** @def esmCHANNEL25
*   @brief Alias name for ESM group x channel 25
*
*   This is an alias name for the ESM group x channel 25.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL25: u64 = 0x0000000002000000u64;

/** @def esmCHANNEL26
*   @brief Alias name for ESM group x channel 26
*
*   This is an alias name for the ESM group x channel 26.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL26: u64 = 0x0000000004000000u64;

/** @def esmCHANNEL27
*   @brief Alias name for ESM group x channel 27
*
*   This is an alias name for the ESM group x channel 27.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL27: u64 = 0x0000000008000000u64;

/** @def esmCHANNEL28
*   @brief Alias name for ESM group x channel 28
*
*   This is an alias name for the ESM group x channel 28.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL28: u64 = 0x0000000010000000u64;

/** @def esmCHANNEL29
*   @brief Alias name for ESM group x channel 29
*
*   This is an alias name for the ESM group x channel 29.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL29: u64 = 0x0000000020000000u64;

/** @def esmCHANNEL30
*   @brief Alias name for ESM group x channel 30
*
*   This is an alias name for the ESM group x channel 30.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL30: u64 = 0x0000000040000000u64;

/** @def esmCHANNEL31
*   @brief Alias name for ESM group x channel 31
*
*   This is an alias name for the ESM group x channel 31.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL31: u64 = 0x0000000080000000u64;

/** @def esmCHANNEL32
*   @brief Alias name for ESM group x channel 32
*
*   This is an alias name for the ESM group x channel 32.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL32: u64 = 0x0000000100000000u64;

/** @def esmCHANNEL33
*   @brief Alias name for ESM group x channel 33
*
*   This is an alias name for the ESM group x channel 33.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL33: u64 = 0x0000000200000000u64;

/** @def esmCHANNEL34
*   @brief Alias name for ESM group x channel 34
*
*   This is an alias name for the ESM group x channel 34.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL34: u64 = 0x0000000400000000u64;

/** @def esmCHANNEL35
*   @brief Alias name for ESM group x channel 35
*
*   This is an alias name for the ESM group x channel 35.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL35: u64 = 0x0000000800000000u64;

/** @def esmCHANNEL36
*   @brief Alias name for ESM group x channel 36
*
*   This is an alias name for the ESM group x channel 36.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL36: u64 = 0x0000001000000000u64;

/** @def esmCHANNEL37
*   @brief Alias name for ESM group x channel 37
*
*   This is an alias name for the ESM group x channel 37.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL37: u64 = 0x0000002000000000u64;

/** @def esmCHANNEL38
*   @brief Alias name for ESM group x channel 38
*
*   This is an alias name for the ESM group x channel 38.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL38: u64 = 0x0000004000000000u64;

/** @def esmCHANNEL39
*   @brief Alias name for ESM group x channel 39
*
*   This is an alias name for the ESM group x channel 39.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL39: u64 = 0x0000008000000000u64;

/** @def esmCHANNEL40
*   @brief Alias name for ESM group x channel 40
*
*   This is an alias name for the ESM group x channel 40.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL40: u64 = 0x0000010000000000u64;

/** @def esmCHANNEL41
*   @brief Alias name for ESM group x channel 41
*
*   This is an alias name for the ESM group x channel 41.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL41: u64 = 0x0000020000000000u64;

/** @def esmCHANNEL42
*   @brief Alias name for ESM group x channel 42
*
*   This is an alias name for the ESM group x channel 42.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL42: u64 = 0x0000040000000000u64;

/** @def esmCHANNEL43
*   @brief Alias name for ESM group x channel 43
*
*   This is an alias name for the ESM group x channel 43.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL43: u64 = 0x0000080000000000u64;

/** @def esmCHANNEL44
*   @brief Alias name for ESM group x channel 44
*
*   This is an alias name for the ESM group x channel 44.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL44: u64 = 0x0000100000000000u64;

/** @def esmCHANNEL45
*   @brief Alias name for ESM group x channel 45
*
*   This is an alias name for the ESM group x channel 45.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL45: u64 = 0x0000200000000000u64;

/** @def esmCHANNEL46
*   @brief Alias name for ESM group x channel 46
*
*   This is an alias name for the ESM group x channel 46.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL46: u64 = 0x0000400000000000u64;

/** @def esmCHANNEL47
*   @brief Alias name for ESM group x channel 47
*
*   This is an alias name for the ESM group x channel 47.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL47: u64 = 0x0000800000000000u64;

/** @def esmCHANNEL48
*   @brief Alias name for ESM group x channel 48
*
*   This is an alias name for the ESM group x channel 48.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL48: u64 = 0x0001000000000000u64;

/** @def esmCHANNEL49
*   @brief Alias name for ESM group x channel 49
*
*   This is an alias name for the ESM group x channel 49.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL49: u64 = 0x0002000000000000u64;

/** @def esmCHANNEL50
*   @brief Alias name for ESM group x channel 50
*
*   This is an alias name for the ESM group x channel 50.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL50: u64 = 0x0004000000000000u64;

/** @def esmCHANNEL51
*   @brief Alias name for ESM group x channel 51
*
*   This is an alias name for the ESM group x channel 51.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL51: u64 = 0x0008000000000000u64;

/** @def esmCHANNEL52
*   @brief Alias name for ESM group x channel 52
*
*   This is an alias name for the ESM group x channel 52.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL52: u64 = 0x0010000000000000u64;

/** @def esmCHANNEL53
*   @brief Alias name for ESM group x channel 53
*
*   This is an alias name for the ESM group x channel 53.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL53: u64 = 0x0020000000000000u64;

/** @def esmCHANNEL54
*   @brief Alias name for ESM group x channel 54
*
*   This is an alias name for the ESM group x channel 54.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL54: u64 = 0x0040000000000000u64;

/** @def esmCHANNEL55
*   @brief Alias name for ESM group x channel 55
*
*   This is an alias name for the ESM group x channel 55.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL55: u64 = 0x0080000000000000u64;

/** @def esmCHANNEL56
*   @brief Alias name for ESM group x channel 56
*
*   This is an alias name for the ESM group x channel 56.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL56: u64 = 0x0100000000000000u64;

/** @def esmCHANNEL57
*   @brief Alias name for ESM group x channel 57
*
*   This is an alias name for the ESM group x channel 57.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL57: u64 = 0x0200000000000000u64;

/** @def esmCHANNEL58
*   @brief Alias name for ESM group x channel 58
*
*   This is an alias name for the ESM group x channel 58.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL58: u64 = 0x0400000000000000u64;

/** @def esmCHANNEL59
*   @brief Alias name for ESM group x channel 59
*
*   This is an alias name for the ESM group x channel 59.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL59: u64 = 0x0800000000000000u64;

/** @def esmCHANNEL60
*   @brief Alias name for ESM group x channel 60
*
*   This is an alias name for the ESM group x channel 60.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL60: u64 = 0x1000000000000000u64;

/** @def esmCHANNEL61
*   @brief Alias name for ESM group x channel 61
*
*   This is an alias name for the ESM group x channel 61.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL61: u64 = 0x2000000000000000u64;

/** @def esmCHANNEL62
*   @brief Alias name for ESM group x channel 62
*
*   This is an alias name for the ESM group x channel 62.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL62: u64 = 0x4000000000000000u64;

/** @def esmCHANNEL63
*   @brief Alias name for ESM group x channel 63
*
*   This is an alias name for the ESM group x channel 63.
*
*   @note This value should be used for API argument @a channel
*/
pub const esmCHANNEL63: u64 = 0x8000000000000000u64;

/** @typedef esmSelfTestFlag_t
*   @brief ESM Self-Test Status Type Definition
*
*   This type is used to represent ESM Self-Test Status.
*/
pub enum esmSelfTestFlag_t 
{
    esmSelfTest_Passed = 0,
	esmSelfTest_Active = 1,
	esmSelfTest_NotStarted = 2,
    esmSelfTest_Failed = 3
}

pub struct esm_config_reg_t
{
    pub CONFIG_EEPAPR1: u32,
    pub CONFIG_IESR1: u32,
    pub CONFIG_ILSR1: u32,
    pub CONFIG_LTCPR: u32,
	pub CONFIG_EKR: u32,  
    pub CONFIG_IEPSR4: u32,
    pub CONFIG_IESR4: u32,
    pub CONFIG_ILSR4: u32,
}

/* Configuration registers initial value */
pub const ESM_EEPAPR1_CONFIGVALUE: u32 =	(0u32 << 31u32)
                               | (0u32 << 30u32)
                               | (0u32 << 29u32)
                               | (0u32 << 28u32)
                               | (0u32 << 27u32)
                               | (0u32 << 26u32)
                               | (0u32 << 25u32)
                               | (0u32 << 24u32)
                               | (0u32 << 23u32)
                               | (0u32 << 22u32)
                               | (0u32 << 21u32)
                               | (0u32 << 20u32)
                               | (0u32 << 19u32)
                               | (0u32 << 18u32)
                               | (0u32 << 17u32)
                               | (0u32 << 16u32)
                               | (0u32 << 15u32)
                               | (0u32 << 14u32)
                               | (0u32 << 13u32)
                               | (0u32 << 12u32)
                               | (0u32 << 11u32)
                               | (0u32 << 10u32)
                               | (0u32 <<  9u32)
                               | (0u32 <<  8u32)
                               | (0u32 <<  7u32)
                               | (0u32 <<  6u32)
                               | (0u32 <<  5u32)
                               | (0u32 <<  4u32)
                               | (0u32 <<  3u32)
                               | (0u32 <<  2u32)
                               | (0u32 <<  1u32)
                               | (0u32 <<  0u32);
pub const ESM_IESR1_CONFIGVALUE: u32 =	(0u32 << 31u32)
                               | (0u32 << 30u32)
                               | (0u32 << 29u32)
                               | (0u32 << 28u32)
                               | (0u32 << 27u32)
                               | (0u32 << 26u32)
                               | (0u32 << 25u32)
                               | (0u32 << 24u32)
                               | (0u32 << 23u32)
                               | (0u32 << 22u32)
                               | (0u32 << 21u32)
                               | (0u32 << 20u32)
                               | (0u32 << 19u32)
                               | (0u32 << 18u32)
                               | (0u32 << 17u32)
                               | (0u32 << 16u32)
                               | (0u32 << 15u32)
                               | (0u32 << 14u32)
                               | (0u32 << 13u32)
                               | (0u32 << 12u32)
                               | (0u32 << 11u32)
                               | (0u32 << 10u32)
                               | (0u32 <<  9u32)
                               | (0u32 <<  8u32)
                               | (0u32 <<  7u32)
                               | (0u32 <<  6u32)
                               | (0u32 <<  5u32)
                               | (0u32 <<  4u32)
                               | (0u32 <<  3u32)
                               | (0u32 <<  2u32)
                               | (0u32 <<  1u32)
                               | (0u32 <<  0u32);
pub const ESM_ILSR1_CONFIGVALUE: u32 =   (0u32 << 31u32)
                               | (0u32 << 30u32)
                               | (0u32 << 29u32)
                               | (0u32 << 28u32)
                               | (0u32 << 27u32)
                               | (0u32 << 26u32)
                               | (0u32 << 25u32)
                               | (0u32 << 24u32)
                               | (0u32 << 23u32)
                               | (0u32 << 22u32)
                               | (0u32 << 21u32)
                               | (0u32 << 20u32)
                               | (0u32 << 19u32)
                               | (0u32 << 18u32)
                               | (0u32 << 17u32)
                               | (0u32 << 16u32)
                               | (0u32 << 15u32)
                               | (0u32 << 14u32)
                               | (0u32 << 13u32)
                               | (0u32 << 12u32)
                               | (0u32 << 11u32)
                               | (0u32 << 10u32)
                               | (0u32 <<  9u32)
                               | (0u32 <<  8u32)
                               | (0u32 <<  7u32)
                               | (0u32 <<  6u32)
                               | (0u32 <<  5u32)
                               | (0u32 <<  4u32)
                               | (0u32 <<  3u32)
                               | (0u32 <<  2u32)
                               | (0u32 <<  1u32)
                               | (0u32 <<  0u32);
pub const ESM_LTCPR_CONFIGVALUE: u32 = 16384u32 - 1u32;
pub const ESM_EKR_CONFIGVALUE     : u32 = 0u32;
pub const ESM_IEPSR4_CONFIGVALUE: u32 = (0u32 << 31u32)
                              | (0u32 << 30u32)
                              | (0u32 << 29u32)
                              | (0u32 << 28u32)
                              | (0u32 << 27u32)
                              | (0u32 << 26u32)
                              | (0u32 << 25u32)
                              | (0u32 << 24u32)
                              | (0u32 << 23u32)
                              | (0u32 << 22u32)
                              | (0u32 << 21u32)
                              | (0u32 << 20u32)
                              | (0u32 << 19u32)
                              | (0u32 << 18u32)
                              | (0u32 << 17u32)
                              | (0u32 << 16u32)
                              | (0u32 << 15u32)
                              | (0u32 << 14u32)
                              | (0u32 << 13u32)
                              | (0u32 << 12u32)
                              | (0u32 << 11u32)
                              | (0u32 << 10u32)
                              | (0u32 <<  9u32)
                              | (0u32 <<  8u32)
                              | (0u32 <<  7u32)
                              | (0u32 <<  6u32)
                              | (0u32 <<  5u32)
                              | (0u32 <<  4u32)
                              | (0u32 <<  3u32)
                              | (0u32 <<  2u32)
                              | (0u32 <<  1u32)
                              | (0u32 <<  0u32);
pub const ESM_IESR4_CONFIGVALUE: u32 = (0u32 << 31u32)
                             | (0u32 << 30u32)
                             | (0u32 << 29u32)
                             | (0u32 << 28u32)
                             | (0u32 << 27u32)
                             | (0u32 << 26u32)
                             | (0u32 << 25u32)
                             | (0u32 << 24u32)
                             | (0u32 << 23u32)
                             | (0u32 << 22u32)
                             | (0u32 << 21u32)
                             | (0u32 << 20u32)
                             | (0u32 << 19u32)
                             | (0u32 << 18u32)
                             | (0u32 << 17u32)
                             | (0u32 << 16u32)
                             | (0u32 << 15u32)
                             | (0u32 << 14u32)
                             | (0u32 << 13u32)
                             | (0u32 << 12u32)
                             | (0u32 << 11u32)
                             | (0u32 << 10u32)
                             | (0u32 <<  9u32)
                             | (0u32 <<  8u32)
                             | (0u32 <<  7u32)
                             | (0u32 <<  6u32)
                             | (0u32 <<  5u32)
                             | (0u32 <<  4u32)
                             | (0u32 <<  3u32)
                             | (0u32 <<  2u32)
                             | (0u32 <<  1u32)
                             | (0u32 <<  0u32);
pub const ESM_ILSR4_CONFIGVALUE: u32 = (0u32 << 31u32)
                             | (0u32 << 30u32)
                             | (0u32 << 29u32)
                             | (0u32 << 28u32)
                             | (0u32 << 27u32)
                             | (0u32 << 26u32)
                             | (0u32 << 25u32)
                             | (0u32 << 24u32)
                             | (0u32 << 23u32)
                             | (0u32 << 22u32)
                             | (0u32 << 21u32)
                             | (0u32 << 20u32)
                             | (0u32 << 19u32)
                             | (0u32 << 18u32)
                             | (0u32 << 17u32)
                             | (0u32 << 16u32)
                             | (0u32 << 15u32)
                             | (0u32 << 14u32)
                             | (0u32 << 13u32)
                             | (0u32 << 12u32)
                             | (0u32 << 11u32)
                             | (0u32 << 10u32)
                             | (0u32 <<  9u32)
                             | (0u32 <<  8u32)
                             | (0u32 <<  7u32)
                             | (0u32 <<  6u32)
                             | (0u32 <<  5u32)
                             | (0u32 <<  4u32)
                             | (0u32 <<  3u32)
                             | (0u32 <<  2u32)
                             | (0u32 <<  1u32)
                             | (0u32 <<  0u32);
