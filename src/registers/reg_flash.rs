#![allow(non_snake_case, non_camel_case_types, non_upper_case_globals, dead_code)]

use volatile_register::{RW, RO};
use raw_pointer::Pointer;

pub enum FlashPowerMode {
    SLEEP = 0,
    STANDBY = 1,
    ACTIVE = 3
}

#[repr(C)]
pub struct FlashBase {
    pub FRDCNTL: RW<u32>,
    rsvd1: u32,
    pub FEDACTRL1: RW<u32>,
    pub FEDACTRL2: RW<u32>,
    pub FCOR_ERR_CNT: RW<u32>,
    pub FCOR_ERR_ADD: RW<u32>,
    pub FEDACSTATUS: RW<u32>,
    pub FUNC_ERR_ADD: RW<u32>,
    pub FEDACSDIS: RW<u32>,
    pub FPRIM_ADD_TAG: RW<u32>,
    pub FDUP_ADD_TAG: RW<u32>,
    pub FBPROT: RW<u32>,
    pub FBSE: RW<u32>,
    pub FBBUSY: RW<u32>,
    pub FBAC: RW<u32>,
    pub FBFALLBACK: RW<u32>,
    pub FBPRDY: RW<u32>,
    pub FPAC1: RW<u32>,
    pub FPAC2: RW<u32>,
    pub FMAC: RW<u32>,
    pub FMSTAT: RW<u32>,
    pub FEMU_DMSW: RW<u32>,
    pub FEMU_DLSW: RW<u32>,
    pub FEMU_ECC: RW<u32>,
    pub FEMU_ADDR: RW<u32>,
    pub FDIAGCTRL: RW<u32>,
    pub FRAW_DATAH: RW<u32>,
    pub FRAW_DATAL: RW<u32>,
    pub FRAW_ECC: RW<u32>,
    pub FPAR_OVR: RW<u32>,
    rsvd2: [u32; 16],
    pub FEDACSDIS2: RW<u32>,
    rsvd3: [u32; 15],
    rsvd4: [u32; 13],
    rsvd5: [u32; 85],
    pub FSM_WR_ENA: RW<u32>,
    rsvd6: [u32; 6],
    pub FSM_SECTOR: RW<u32>,
    rsvd7: [u32; 4],
    pub EEPROM_CONFIG: RW<u32>,
    rsvd8: [u32; 19],
    pub EE_CTRL1: RW<u32>,
    pub EE_CTRL2: RW<u32>,
    pub EE_COR_ERR_CNT: RW<u32>,
    pub EE_COR_ERR_ADD: RW<u32>,
    pub EE_COR_ERR_POS: RW<u32>,
    pub EE_STATUS: RW<u32>,
    pub EE_UNC_ERR_ADD: RW<u32>,
    rsvd9: [u32; 56],
    pub FCFG_BANK: RW<u32>,
}

pub const flashREG: Pointer<FlashBase> = Pointer::<FlashBase> {
    ptr: 0xFFF87000u32 as *mut FlashBase
};

pub const singleBitErr: Pointer<RO<u32>> = Pointer::<RO<u32>> {
    ptr: 0xF00803F0u32 as *mut RO<u32>
};
pub const doubleBitErr: Pointer<RO<u32>> = Pointer::<RO<u32>> {
    ptr: 0xF00803F8u32 as *mut RO<u32>
};
