#![no_std]
#![no_main]

use cortex_r::peripherals::{gio};
use cortex_r::registers::{reg_gio::*};
use core::panic::PanicInfo;

#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
    loop {}
}

#[no_mangle]
pub extern "C" fn _start() -> ! {
    unsafe { gio::gioSetBit(gioPORTA, 2, 1); }
    loop {}
}
